/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mock_up_logger.c
 * @author Brendan Bruner
 * @date May 22, 2015
 */

#include "dependency_injection.h"

char *mas_tbl = "mmt0.txt";
char nm_spc = 'a';

#define nm_spc1 'b'
#define nm_spc2 'c'
#define nm_spc3 'd'
#define nm_spc4 'e'
#define nm_spc5 'f'
#define nm_spc6 'g'
char switch_nm_spc[] = {nm_spc1, nm_spc2, nm_spc3, nm_spc4, nm_spc5, nm_spc6};

#define mas_tbl1 "mmt1.txt"
#define mas_tbl2 "mmt2.txt"
#define mas_tbl3 "mmt3.txt"
#define mas_tbl4 "mmt4.txt"
#define mas_tbl5 "mmt5.txt"
#define mas_tbl6 "mmt6.txt"
char *switch_mas_tbl[] = {mas_tbl1, mas_tbl2, mas_tbl3, mas_tbl4, mas_tbl5, mas_tbl6};



void mock_up_logger_reset( void )
{
	filesystem_t *fs;
	fs = kitp->fs;

	if( fs->file_exists( fs, mas_tbl ) == FS_OK ) fs->delete( fs, mas_tbl );
	if( fs->file_exists( fs, mas_tbl1 ) == FS_OK ) fs->delete( fs, mas_tbl1 );
	if( fs->file_exists( fs, mas_tbl2 ) == FS_OK ) fs->delete( fs, mas_tbl2 );
	if( fs->file_exists( fs, mas_tbl3 ) == FS_OK ) fs->delete( fs, mas_tbl3 );
	if( fs->file_exists( fs, mas_tbl4 ) == FS_OK ) fs->delete( fs, mas_tbl4 );
	if( fs->file_exists( fs, mas_tbl5 ) == FS_OK ) fs->delete( fs, mas_tbl5 );
	if( fs->file_exists( fs, mas_tbl6 ) == FS_OK ) fs->delete( fs, mas_tbl6 );
}


void initialize_mock_up_logger( logger_t *logger )
{
	initialize_logger( logger, kitp->fs, mas_tbl, nm_spc, 15 );
}

void initialize_mock_up_logger_custom( logger_t *logger, uint8_t ns_switch )
{
	char nm_spc = switch_nm_spc[ ns_switch ];
	char *master = switch_mas_tbl[ ns_switch ];

	initialize_logger( logger, kitp->fs, master, nm_spc, 15 );
}
