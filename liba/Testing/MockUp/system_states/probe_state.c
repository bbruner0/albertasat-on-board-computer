/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file probe_state
 * @author Brendan Bruner
 * @date Jan 30, 2015
 */

#include <states/state_relay.h>
#include <states/state.h>
#include <telecommands/telecommand.h>

#include <dependency_injection.h>


/************************************************************************/
/* Entry / Exit Methods													*/
/************************************************************************/
static void enter_probe_state( state_t *state, driver_toolkit_t *drivers )
{
	++((state_probe_t *) state)->_counter_->enters;
}

void exit_probe_state( state_t *state, driver_toolkit_t *drivers )
{
	++((state_probe_t *) state)->_counter_->exits;
}

/************************************************************************/
/* Next State Method													*/
/************************************************************************/
state_t *next_state_from_probe( 	state_t *state,
										state_relay_t *relay )
{
	++((state_probe_t *) state)->_counter_->nexts;
	return state;
}

/************************************************************************/
/* Initialization Method												*/
/************************************************************************/
void initialize_probe_state( state_probe_t *self, probe_counter_t *counter, filesystem_t *fs )
{	
	state_config_t config;
	state_t *state = (state_t *) self;

	config._uses_dfgm = STATE_ENABLES_EXECUTION;
	config._uses_mnlp = STATE_ENABLES_EXECUTION;
	config._uses_transmit = STATE_ENABLES_EXECUTION;
	config._uses_response = STATE_ENABLES_EXECUTION;
	config._uses_hk = STATE_ENABLES_EXECUTION;

	initialize_state( state, "pst_l.txt", fs, &config );

	counter->enters = 0;
	counter->exits = 0;
	counter->nexts = 0;

	self->_counter_ = counter;
	state->_id_ = 			STATE_PROBE_ID;
	state->enter_state = &enter_probe_state;
	state->exit_state = 	&exit_probe_state;
	state->next_state = &next_state_from_probe;
}
