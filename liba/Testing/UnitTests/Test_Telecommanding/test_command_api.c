/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_command_api.c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include <telecommands/telecommand.h>
#include <telecommands/self_alteration/telecommand_blink.h>
#include <portable_types.h>
#include <string.h>

#include <test_suites.h>
#include <dependency_injection.h>

#define TEST_ID		10131
#define TEST_TYPE	45
#define TELECOMMAND_ARG_STRING "derp mcderp"

driver_toolkit_t *kit;
telecommand_counter_t counter;
int count;

TEST_SETUP( )
{
	count = 0;
	initialize_command_counter( &counter, &count, kit );
}
TEST_TEARDOWN( )
{
	((telecommand_t *) &counter)->destroy( (telecommand_t *) &counter );
}

TEST( non_virtual_execute )
{
	telecommand_execute( (telecommand_t *) &counter );
	ASSERT( "command didn't executed", count == 1 );
}

TEST( command_status )
{
	telecommand_status_t status;

	status = telecommand_status( (telecommand_t *) &counter );
	ASSERT( "initial status is %d, not \"CMND_PENDING\"", status == CMND_PENDING, status );

	telecommand_execute( (telecommand_t *) &counter );
	status = telecommand_status( (telecommand_t *) &counter );
	ASSERT( "executed status is %d, not \"CMND_EXECUTED\"", status == CMND_EXECUTED, status );
}

TEST( command_id )
{
	telecommand_set_id( (telecommand_t *) &counter, TEST_ID );
	ASSERT( "command id is %d, but should be %d", telecommand_get_id( (telecommand_t *) &counter ) == TEST_ID,
												  telecommand_get_id( (telecommand_t *) &counter ), TEST_ID );
}

TEST( command_type )
{
	((telecommand_t *) &counter)->_type_ = TEST_TYPE;
	ASSERT( "command type is %d, but should be %d", telecommand_get_type( (telecommand_t *) &counter ) == TEST_TYPE,
													telecommand_get_type( (telecommand_t *) &counter ), TEST_TYPE);
}

TEST( argument )
{
	char* argument = TELECOMMAND_ARG_STRING;

	telecommand_argument( (telecommand_t*) &counter, argument, strlen( argument ) );
	ASSERT( "incorrect argument", 0 == strncmp( ((telecommand_t*) &counter)->_argument, argument, TELECOMMAND_MAX_ARGUMENT_LENGTH ) );
}

TEST_SUITE( command_api )
{
	kit = kitp;
	ADD_TEST( non_virtual_execute );
	ADD_TEST( command_status );
	ADD_TEST( command_id );
	ADD_TEST( command_type );
	ADD_TEST( argument );
}
