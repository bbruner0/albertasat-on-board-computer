/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_script_daemon.c
 * @author Brendan Bruner
 * @date Nov 4, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <telecommands/telecommand_prototype_manager.h>
#include <script_daemon.h>
#include <string.h>

#define COUNT_KEY "tsd_cmnd"
#define SCRIPT_STRING "COMMAND( \"tsd_cmnd\" );"

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( daemon_runs )
{
	telecommand_prototype_manager_t*	prototypes;
	telecommand_counter_t				count_command;
	ground_station_t					gs;
	script_daemon_t						daemon;
	char*								key = COUNT_KEY;
	char*								script = SCRIPT_STRING;
	int									count;
	bool_t								err;


	/* Register the count command with the prototype manager. */
	prototypes = get_telecommand_prototype_manager( );
	initialize_command_counter( &count_command, &count, kitp );
	err = prototypes->register_prototype( prototypes, key, (telecommand_t*) &count_command );
	if( !err )
	{
		ABORT_TEST( "failed to register command prototype" );
	}


	/* Pretend to be ground station, and write telecommand. */
	initialize_mock_up_ground_station_dynamic_sizes( &gs );
	gs.write( &gs, (uint8_t*) script, strlen( script ), TELECOMMAND_PORT, BLOCK_FOREVER );

	/* Initialize daemon, it should start up, see the script and run it. */
	err = initialize_script_daemon( &daemon, &gs );
	if( err == false )
	{
		/* Error creating daemon. */
		((telecommand_t*) &count_command)->destroy( (telecommand_t*) &count_command );
	}

	/* Give daemon CPU time. */
	task_delay( 300 );
	/* Assert the daemon ran. */
	ASSERT( "Daemon did not execute command", count == 1 );

	((telecommand_t*) &count_command)->destroy( (telecommand_t*) &count_command );
	daemon.destroy( &daemon );
}

TEST_SUITE( script_daemon )
{
	ADD_TEST( daemon_runs );
}
