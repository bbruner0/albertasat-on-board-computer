/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_interpreter.c
 * @author Brendan Bruner
 * @date Oct 13, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <telecommands/telecommand_prototype_manager.h>
#include <parser/interpreter/telecommand_expression.h>
#include <parser/parser.h>
#include <string.h>

#define COUNT_KEY "count"
static telecommand_counter_t count_command;
#define ARG_EXP_STRING "derp_string"
#define COUNT_KEY2 "count two"
#define SCRIPT_STRING "COMMAND( \"count two\", \"derp_string\" );";

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( prototype_manager )
{
	telecommand_prototype_manager_t* prototypes;
	char *keys[] = { "command A", "command B" };
	telecommand_t* elements[] = { (telecommand_t*) 0x05, (telecommand_t*) 0x554 };
	telecommand_t* retrieved;

	prototypes = get_telecommand_prototype_manager( );
	prototypes->register_prototype( prototypes, keys[0], elements[0] );
	prototypes->register_prototype( prototypes, keys[1], elements[1] );

	retrieved = prototypes->get_prototype( prototypes, keys[0] );
	ASSERT( "command A prototype incorrect", retrieved == elements[0] );

	retrieved = prototypes->get_prototype( prototypes, keys[1] );
	ASSERT( "command A prototype incorrect", retrieved == elements[1] );
}

TEST( telecommand_expression )
{
	telecommand_prototype_manager_t*	prototypes;
	char*								key = COUNT_KEY;
	char*								arg = ARG_EXP_STRING;
	int									count;
	bool_t								err;
	telecommand_expression_t			*expression;


	/* Register the count command with the prototype manager. */
	prototypes = get_telecommand_prototype_manager( );
	initialize_command_counter( &count_command, &count, kitp );
	err = prototypes->register_prototype( prototypes, key, (telecommand_t*) &count_command );
	if( !err )
	{
		ABORT_TEST( "failed to register command prototype" );
	}

	/* Initialize the telecommand expression. */
	expression = (telecommand_expression_t*) OBCMalloc( sizeof(telecommand_expression_t) );
	if( expression == NULL )
	{
		ABORT_TEST( "Failed to allocate expression" );
	}
	err = initialize_telecommand_expression( expression, key, strlen( key ), arg, strlen( arg ) );
	if( err == false )
	{
		OBCFree( expression );
		ABORT_TEST( "Failed to initialize expression" );
	}

	/* Interpret the expression and assert the command gets executed. */
	ASSERT( "command should not have executed yet", count == 0 );
	((script_expression_t*) expression)->interpret( (script_expression_t*) expression );
	ASSERT( "command did not execute", count == 1 );
	ASSERT( "command has wrong argument", 0 == strcmp( arg, expression->_.telecommand->_argument) );
	((script_expression_t*) expression)->destroy( (script_expression_t*) expression );
}

TEST( parser )
{

	parser_t 							parser;
	char const* 						script = SCRIPT_STRING;
	script_expression_t* 				expression;
	telecommand_prototype_manager_t*	prototypes;
	char*								key = COUNT_KEY2;
	int									count;
	bool_t								err;


	/* Register the count command with the prototype manager. */
	prototypes = get_telecommand_prototype_manager( );
	initialize_command_counter( &count_command, &count, kitp );
	err = prototypes->register_prototype( prototypes, key, (telecommand_t*) &count_command );
	if( !err )
	{
		ABORT_TEST( "failed to register command prototype" );
	}

	initialize_parser( &parser );
	expression = parser_parse_string( &parser, script, strlen( script ) );
	ASSERT( "Failed to parse script", expression != NULL );
	if( expression != NULL )
	{
		ASSERT( "command should not have executed", count == 0 );
		expression->interpret( expression );
		ASSERT( "command should have executed", count == 1 );
		expression->destroy( expression );
	}
}


TEST_SUITE( interpreter )
{
	ADD_TEST( prototype_manager );
	ADD_TEST( telecommand_expression );
	ADD_TEST( parser );
}
