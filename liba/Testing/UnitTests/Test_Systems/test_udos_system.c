/*
 *
 * Copyright (C) 2018  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */

/* test_telemetry_stream.c
 *
 *  Created on: Apr 3, 2018
 *      Author: Brendan Bruner
 */
#include <unit.h>
#include <string.h>
#include <logger/telemetry_stream.h>
#include <teledyne/teledyne.h>
#include <inttypes.h>

#define TEST_UDOS_RESUME "COMMAND(\"udos\", \"start\");"
#define TEST_UDOS_RESUME_LEN strlen(TEST_UDOS_RESUME)

#define TEST_UDOS_STOP "COMMAND(\"udos\", \"stop\");"
#define TEST_UDOS_STOP_LEN strlen(TEST_UDOS_STOP)

#define TEST_UDOS_LOG_TIMEOUT (450000)

#define TEST_UDOS_RECORDED_PACKET_COUNT 2

#define TEST_UDOS_CHAN0_MAX 2650
#define TEST_UDOS_CHAN0_MIN 2620
#define TEST_UDOS_CHAN1_MAX 3170
#define TEST_UDOS_CHAN1_MIN 3140

static semaphore_t constraint_notification;

static int test_udos_stream_constraint( struct telemetry_stream_t* self, file_t* file )
{
	uint32_t size;
	fs_error_t ferr;

	UNIT_PRINT("applying test constraints..");
	post_semaphore(constraint_notification);

	size = file->size(file, &ferr);
	if( ferr == FS_OK && size >= self->max_bucket_size ) {
		return 1;
	}
	return -1;
}

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( telecommanding )
{
	/* Test that telecommands can be used to start/stop the
	 * udos application.
	 */
	uint32_t app_start_time, app_end_time;

	/* Start the udos application.
	 */
	ground_station_nanomind_insert(kitp->gs, TEST_UDOS_RESUME, TEST_UDOS_RESUME_LEN);
	peek_semaphore(kitp->teledyne->power_ctrl, DOSIMETER_RESUME_TIMEOUT);

	/* Poll udos application handle's running status.
	 */
	ASSERT("udos application is not running", peek_semaphore(kitp->teledyne->power_ctrl, USE_POLLING) == SEMAPHORE_AVAILABLE);

	/* Stop the udos application
	 */
	ground_station_nanomind_insert(kitp->gs, TEST_UDOS_STOP, TEST_UDOS_STOP_LEN);
	app_start_time = task_time_elapsed();
	while( peek_semaphore(kitp->teledyne->power_ctrl, USE_POLLING) == SEMAPHORE_AVAILABLE ) {
		app_end_time = task_time_elapsed();
		if( app_end_time > (app_start_time + DOSIMETER_RESUME_TIMEOUT) ) {
			break;
		}
		task_delay(10);
	}

	/* Poll udos application handle's running status.
	 */
	ASSERT("udos application didn't stop running", peek_semaphore(kitp->teledyne->power_ctrl, USE_POLLING) == SEMAPHORE_BUSY);
}

TEST( data_requests )
{
	/* Test that peripheral applications can request udos samples
	 */
	struct udos_event_t event;
	int err;
	uint32_t time_before_sampling, time_of_samples;

	/* Setup test data.
	 */
	memset(&event, 0, sizeof(event));
	time_before_sampling= (uint32_t) kitp->rtc->get_ds1302_time();

	/* Request data sample
	 */
	err = udos_take_samples(&event);

	/* Verify.
	 */
	time_of_samples = event.time_stamp;
	ASSERT("Could not take samples due to error", err == 0);
	ASSERT("Incorrect time stamp; %" PRIu32 " stamped, but should be %" PRIu32,
			time_of_samples >= time_before_sampling && time_of_samples <= time_before_sampling+1,
			time_of_samples, time_before_sampling);
}

TEST( data_logging )
{
	/* Test that the udos application correctly logs data
	 */
	size_t udos_system_cadence;
	file_t* udos_log;
	fs_error_t ferr;
	uint32_t cmd_start_time, cmd_end_time, bytes_read;
	struct udos_packet_t packet;
	uint8_t pcount;
	size_t i, j, k;
	uint32_t start_time[TEST_UDOS_RECORDED_PACKET_COUNT], end_time[TEST_UDOS_RECORDED_PACKET_COUNT];
	int stream_err;
	const char* log_name_ref;
	char log_name[FILESYSTEM_MAX_NAME_LENGTH+1];
	telemetry_stream_next_f udos_stream_constraints;

	UNIT_PRINT("This test assumes channel 0 and 1 of the uDos ADC is wired for\n"
			"a digital reading of ~%d and ~%d respectively",
			(TEST_UDOS_CHAN0_MAX+TEST_UDOS_CHAN0_MIN)/2, (TEST_UDOS_CHAN1_MAX+TEST_UDOS_CHAN1_MIN)/2);

	/* Empty binary semaphore
	 */
	new_semaphore(constraint_notification, 1, 0);

	/* Reduce sampling cadence so that a full packet can be created quickly
	 * Force the data stream to start in a new bucket
	 */
	udos_system_cadence = UDOS_EVENT_PERIOD;
	UDOS_EVENT_PERIOD = 2;
	telemetry_stream_next_file(&kitp->teledyne->event_stream);

	/* Take note of the new buckets name incase we allow data logging to run for too
	 * long and a new bucket is inserted in the teletemtry stream.
	 */
	udos_log = telemetry_stream_get_file(&kitp->teledyne->event_stream, &stream_err);
	log_name_ref = fs_handle_get_name((fs_handle_t*) udos_log);
	strncpy(log_name, log_name_ref, FILESYSTEM_MAX_NAME_LENGTH+1);

	/* Change the telemetry stream constraints function to notify us when
	 * two packets have been logged.
	 */
	udos_stream_constraints = kitp->teledyne->event_stream.next;
	kitp->teledyne->event_stream.next = test_udos_stream_constraint;

	/* Reset the data packet being constructed in the udos' memory space
	 * Note, this init_packet function is suppose to be private to the udos class.
	 * Start the udos application and enable debug printing.
	 */
	extern void udos_init_packet( struct teledyne_t*, uint8_t );
	udos_init_packet(kitp->teledyne, 0);
	ground_station_nanomind_insert(kitp->gs, TEST_UDOS_RESUME, TEST_UDOS_RESUME_LEN);
	peek_semaphore(kitp->teledyne->power_ctrl, DOSIMETER_RESUME_TIMEOUT);

	for( i = 0; i < TEST_UDOS_RECORDED_PACKET_COUNT; ++i ) {
		/* Record approximate time when packet starts getting filled
		 */
		start_time[i] = (uint32_t) kitp->rtc->get_ds1302_time();

		/* Wait until a packet has been logged.
		 */
		ASSERT("Timed out waiting for data P%d to log", take_semaphore(constraint_notification, TEST_UDOS_LOG_TIMEOUT) == SEMAPHORE_AVAILABLE, i);
		end_time[i] = (uint32_t) kitp->rtc->get_ds1302_time();
	}

	/* Stop the udos application
	 */
	ground_station_nanomind_insert(kitp->gs, TEST_UDOS_STOP, TEST_UDOS_STOP_LEN);
	cmd_start_time = task_time_elapsed();
	cmd_end_time = cmd_start_time;
	while( peek_semaphore(kitp->teledyne->power_ctrl, USE_POLLING) == SEMAPHORE_AVAILABLE ) {
		cmd_end_time = task_time_elapsed();
		if( cmd_end_time > (cmd_start_time + DOSIMETER_RESUME_TIMEOUT) ) {
			break;
		}
		task_delay(10);
	}
	ASSERT("Timeout waiting for udos to power off: %" PRIu32, cmd_end_time <= (cmd_start_time + DOSIMETER_RESUME_TIMEOUT), cmd_end_time-cmd_start_time);
	if( cmd_end_time > (cmd_start_time + DOSIMETER_RESUME_TIMEOUT) ) {
		ABORT_TEST("couldn't stop udos app");
	}

	/* udos is powered down now, but the task needs to run until it blocks on the power_ctrl semaphore. Suspend this test task now so the
	 * udos task can run. I can't think of a more deterministic way to do this without adding more code to the udos task.
	 */
	task_delay(100);

	/* Restore stream constraints function.
	 */
	kitp->teledyne->event_stream.next = udos_stream_constraints;
	delete_semaphore(constraint_notification);

	/* Restore udos state.
	 */
	UDOS_EVENT_PERIOD = udos_system_cadence;

	/* Read out packets from non volatile memory.
	 */
	telemetry_stream_close_file(&kitp->teledyne->event_stream);
	udos_log = kitp->fs->open(kitp->fs, &ferr, log_name, FS_OPEN_EXISTING, USE_POLLING);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed to open udos log: %d", ferr);
	}
	ferr = udos_log->seek(udos_log, 0);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed to seek to beginning of file");
	}

	/* Verify packet contents
	 */
	pcount = 0;
	for( i = 0; i < TEST_UDOS_RECORDED_PACKET_COUNT; ++i ) {
		/* Read out a packet then verify it.
		 */
		ferr = udos_log->read(udos_log, (uint8_t*) &packet, sizeof(packet), &bytes_read);
		if( ferr != FS_OK || bytes_read != sizeof(packet) ) {
			ABORT_TEST("Failed to read packet%d: err %d, bytes %" PRIu32, i, ferr, bytes_read);
		}
		ASSERT("P%d: Incorrect events logged (%" PRIu8 ")", packet.events_recorded == UDOS_EVENTS_PER_PACKET, i, packet.events_recorded);
		ASSERT("P%d: Incorrect samples per event (%" PRIu8 ")", packet.samples_per_event == UDOS_SAMPLES_PER_EVENT, i, packet.samples_per_event);
		ASSERT("P%d: Incorrect packet version (%" PRIu8 ")", packet.pv == UDOS_PACKET_VERSION, i, packet.pv);
		ASSERT("P%d: Incorrect control bytes", packet.dle == 0x10 && packet.eot == 0x04 && packet.etx == 0x03 && packet.stx == 0x02, i);
		ASSERT("P%d: Incorrect PID (%" PRIu8 ") expected %" PRIu8, pcount == packet.pid, i, packet.pid, pcount);

		/* This verifies the time stamps.
		 */
		for( j = 0; j < packet.events_recorded; ++j ) {
			uint32_t time_stamp = packet.events[j].time_stamp;
			ASSERT("P%d E%d: Incorrect time stamp %" PRIu32 " ![%" PRIu32 ", %" PRIu32 "]", time_stamp <= end_time[i] && time_stamp >= start_time[i], i, j, time_stamp, start_time[i], end_time[i]);

			/* This verifies channel readings
			 */
			for( k = 0; k < packet.samples_per_event; ++k ) {
				ASSERT("P%d E%d S%d: Incorrect channel 0", packet.events[j].channel0[k] <= TEST_UDOS_CHAN0_MAX && packet.events[j].channel0[k] >= TEST_UDOS_CHAN0_MIN,i, j, k);
				ASSERT("P%d E%d S%d: Incorrect channel 1", packet.events[j].channel1[k] <= TEST_UDOS_CHAN1_MAX && packet.events[j].channel1[k] >= TEST_UDOS_CHAN1_MIN,i, j, k);
			}
		}

		pcount = (pcount + 1) % UINT8_MAX;
	}
}

TEST_SUITE( udos_system )
{
	ADD_TEST( telecommanding );
	ADD_TEST( data_requests );
	ADD_TEST( data_logging);
}


