/*
 *
 * Copyright (C) 2018  Keith Mills
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * kgmills@ualberta.ca
 */

/*
 * test_eps_hysteresis.c
 *
 *  Created on: Jul 19, 2018
 *      Author: Keith Mills
 */

#include <unit.h>
#include <eps/eps_nanomind.h>
#include <core_defines.h>

#define MAX_VOLTAGE			16000
#define err_str  			"Improper state returned!"

eps_nanomind_t testEPS;

/**
 * @brief Set initial state for EPS
 * @param initState
 */
static inline void setEPS(eps_mode_t initState) {
	((eps_t *)&testEPS)->currentBatteryState = initState;
}

TEST_SETUP() {

	initialize_eps_nanomind(&testEPS);
}

TEST_TEARDOWN() {

	((eps_t *)&testEPS)->destroy((eps_t *)&testEPS);
}

/*
 * ALL TESTS TAKE THE FORM OF
 * PREVSTATE_NEWSTATE
 *
 * Tests where the initial state is OPTIMAL.
 */
TEST(OPTIMAL_OPTIMAL) {

	eps_mode_t curr = EPS_MODE_OPTIMAL;
	eps_mode_t next = EPS_MODE_OPTIMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), MAX_VOLTAGE);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(OPTIMAL_NORMAL)  {

	eps_mode_t curr = EPS_MODE_OPTIMAL;
	eps_mode_t next = EPS_MODE_NORMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(OPTIMAL_POWER_SAFE) {

	eps_mode_t curr = EPS_MODE_OPTIMAL;
	eps_mode_t next = EPS_MODE_POWER_SAFE;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(OPTIMAL_CRITICAL) {

	eps_mode_t curr = EPS_MODE_OPTIMAL;
	eps_mode_t next = EPS_MODE_CRITICAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

/*
 * Tests where the initial state is NORMAL.
 */
TEST(NORMAL_OPTIMAL) {

	eps_mode_t curr = EPS_MODE_NORMAL;
	eps_mode_t next = EPS_MODE_OPTIMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), MAX_VOLTAGE);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(NORMAL_NORMAL)  {

	eps_mode_t curr = EPS_MODE_NORMAL;
	eps_mode_t next = EPS_MODE_NORMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(NORMAL_POWER_SAFE) {

	eps_mode_t curr = EPS_MODE_NORMAL;
	eps_mode_t next = EPS_MODE_POWER_SAFE;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(NORMAL_CRITICAL) {

	eps_mode_t curr = EPS_MODE_NORMAL;
	eps_mode_t next = EPS_MODE_CRITICAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD -1 );
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

/*
 * Tests where the initial state is POWER SAFE.
 */
TEST(POWER_SAFE_OPTIMAL) {

	eps_mode_t curr = EPS_MODE_POWER_SAFE;
	eps_mode_t next = EPS_MODE_OPTIMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), MAX_VOLTAGE);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(POWER_SAFE_NORMAL)  {

	eps_mode_t curr = EPS_MODE_POWER_SAFE;
	eps_mode_t next = EPS_MODE_NORMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(POWER_SAFE_POWER_SAFE) {

	eps_mode_t curr = EPS_MODE_POWER_SAFE;
	eps_mode_t next = EPS_MODE_POWER_SAFE;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(POWER_SAFE_CRITICAL) {

	eps_mode_t curr = EPS_MODE_POWER_SAFE;
	eps_mode_t next = EPS_MODE_CRITICAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

/*
 * Tests where the initial state is CRITICAL.
 */
TEST(CRITICAL_OPTIMAL) {

	eps_mode_t curr = EPS_MODE_CRITICAL;
	eps_mode_t next = EPS_MODE_OPTIMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), MAX_VOLTAGE);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(CRITICAL_NORMAL) {

	eps_mode_t curr = EPS_MODE_CRITICAL;
	eps_mode_t next = EPS_MODE_NORMAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), OPTIMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(CRITICAL_POWER_SAFE) {

	eps_mode_t curr = EPS_MODE_CRITICAL;
	eps_mode_t next = EPS_MODE_POWER_SAFE;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), NORMAL_THRESHOLD);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), POWER_SAFE_THRESHOLD + 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST(CRITICAL_CRITICAL) {

	eps_mode_t curr = EPS_MODE_CRITICAL;
	eps_mode_t next = EPS_MODE_CRITICAL;

	setEPS(curr);
	eps_hysteresis(((eps_t *)&testEPS), CRITICAL_THRESHOLD - 1);
	ASSERT(err_str, ((eps_t *)&testEPS)->currentBatteryState == next);
}

TEST_SUITE(eps_hysteresis) {

	ADD_TEST(OPTIMAL_OPTIMAL);
	ADD_TEST(OPTIMAL_NORMAL);
	ADD_TEST(OPTIMAL_POWER_SAFE);
	ADD_TEST(OPTIMAL_CRITICAL);
	ADD_TEST(NORMAL_OPTIMAL);
	ADD_TEST(NORMAL_NORMAL);
	ADD_TEST(NORMAL_POWER_SAFE);
	ADD_TEST(NORMAL_CRITICAL);
	ADD_TEST(POWER_SAFE_OPTIMAL);
	ADD_TEST(POWER_SAFE_NORMAL);
	ADD_TEST(POWER_SAFE_POWER_SAFE);
	ADD_TEST(POWER_SAFE_CRITICAL);
	ADD_TEST(CRITICAL_OPTIMAL);
	ADD_TEST(CRITICAL_NORMAL);
	ADD_TEST(CRITICAL_POWER_SAFE);
	ADD_TEST(CRITICAL_CRITICAL);
}
