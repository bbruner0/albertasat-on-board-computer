/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_file_handle.c
 * @author Brendan Bruner
 * @date May 8, 2015
 */

#include <filesystems/filesystem.h>
#include <filesystems/fatfs/file_fatfs.h>
#include <filesystems/fatfs/dir_fatfs.h>
#include <test_suites.h>
#include <string.h>
#include <dependency_injection.h>

#define ONE_HANDLE 1
#define TEST_DATA_LEN 355
#define NULL_MSG_LEN 1
#define HANDLE_TEST_FLUSH_LIMIT 1

static filesystem_t *fs, *filesystem;
static driver_toolkit_t *kit;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( fatfs_file_methods_bound )
{
	file_t 	*file;
	uint8_t init_err;

	file_fatfs_t handle;
	init_err = initialize_file_fatfs( &handle, fs );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize fatfs file for testing" );
	}

	file = (file_t *) &handle;

	ASSERT( "fatfs write not bound", file->write != NULL );
	ASSERT( "fatfs read not bound", file->read != NULL );
	ASSERT( "fatfs seek not bound", file->seek != NULL );
	ASSERT( "fatfs size not bound", file->size != NULL );
	ASSERT( "fatfs position not bound", file->position != NULL );
	ASSERT( "fatfs truncate not bound", file->truncate != NULL );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( file_methods_bound )
{
	file_t 	*file;
	uint8_t init_err;

	file_t handle;
	init_err = _initialize_file( &handle, fs, HANDLE_NULL );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize file for testing" );
	}

	file = (file_t *) &handle;

	ASSERT( "file write not bound", file->write != NULL );
	ASSERT( "file read not bound", file->read != NULL );
	ASSERT( "file seek not bound", file->seek != NULL );
	ASSERT( "file size not bound", file->size != NULL );
	ASSERT( "file position not bound", file->position != NULL );
	ASSERT( "file truncate not bound", file->truncate != NULL );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( fatfs_dir_methods_bound )
{
	dir_t 	*file;
	uint8_t init_err;

	dir_fatfs_t handle;
	init_err = initialize_dir_fatfs( &handle, fs );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize fatfs dir for testing" );
	}

	file = (dir_t *) &handle;

	ASSERT( "fatfs list not bound", file->list != NULL );
	ASSERT( "fatfs reset list not bound", file->reset_list != NULL );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( dir_methods_bound )
{
	dir_t 	*file;
	uint8_t init_err;

	dir_t handle;
	init_err = _initialize_dir( &handle, fs, HANDLE_NULL );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize fatfs dir for testing" );
	}

	file = (dir_t *) &handle;

	ASSERT( "list not bound", file->list != NULL );
	ASSERT( "reset list not bound", file->reset_list != NULL );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( fs_handle_methods_bound )
{
	uint8_t 	init_err;

	fs_handle_t handle;
	init_err = _initialize_fs_handle( &handle, fs, HANDLE_NULL );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		ABORT_TEST( "Failed to initialize fatfs dir for testing" );
	}

	ASSERT( "destroy not bound", handle.destroy != NULL );
}

TEST( write_read )
{
	file_t 				*file;
	fs_error_t			err, rerr, werr;
	char const *		test_msg = "test message";
	char 				test_read[strlen( test_msg )];
	uint32_t			read_out, iter, written_out;
	uint8_t				test_data_out[TEST_DATA_LEN], test_data_in[TEST_DATA_LEN];

	file = fs->open( fs, &err, "tw.txt", FS_CREATE_ALWAYS, USE_POLLING );

	/* Test a simple, small, read / write */
	werr = file->write( file, (uint8_t *) test_msg, strlen( test_msg ), &written_out );
	file->seek( file, BEGINNING_OF_FILE );
	rerr = file->read( file, (uint8_t *) test_read, strlen( test_msg ), &read_out );

	ASSERT( "no error on write, err = %d", werr == FS_OK, werr );
	ASSERT( "no error on read", rerr == FS_OK );
	ASSERT( "correct number of byte written", written_out == strlen( test_msg ) );
	ASSERT( "correct number of bytes read", read_out == strlen( test_msg ) );
	ASSERT( "read matches write", strncmp( test_msg, test_read, strlen( test_msg ) ) == 0 );

	/* Test a long read / write */
	for( iter = 0; iter < TEST_DATA_LEN; ++iter )
	{
		test_data_out[iter] = iter;
	}

	file->seek( file, BEGINNING_OF_FILE );
	werr = file->write( file, test_data_out, TEST_DATA_LEN, &written_out );
	file->seek( file, BEGINNING_OF_FILE );
	rerr = file->read( file, test_data_in, TEST_DATA_LEN, &read_out );

	ASSERT( "no error on long write", werr == FS_OK );
	ASSERT( "no error on long read", rerr == FS_OK );
	ASSERT( "correct number of byte written", written_out == TEST_DATA_LEN);
	ASSERT( "correct number of bytes read on long", read_out == TEST_DATA_LEN );
	ASSERT( "read matches write on long", strncmp( (char *) test_data_in, (char *) test_data_out, TEST_DATA_LEN ) == 0 );

	fs->close( fs, file );
}

TEST( seek )
{
	file_t			*file;
	fs_error_t			serr, werr, oerr, temp_err;
	char				byte_read, msg[5];
	char *				test_msg = "ab zq";
	char *				apnd_msg = "derp";
	uint32_t			num_read;

	file = filesystem->open( filesystem, &oerr, "st.txt", FS_CREATE_ALWAYS, USE_POLLING );

	/* Seek randomly and assert it works */
	werr = file->write( file, (uint8_t *) test_msg, strlen( test_msg ), &num_read );
	serr = file->seek( file, 2 );
	file->read( file, (uint8_t *) &byte_read, 1, &num_read );

	ASSERT( "no error, err = %d, oerr = %d, werr = %d", serr == FS_OK, serr, oerr, werr );
	ASSERT( "seeked to correct position", byte_read == test_msg[2] );

	/* Seek to begining and assert it works */
	serr = file->seek( file, BEGINNING_OF_FILE );
	file->read( file, (uint8_t *) &byte_read, 1, &num_read );

	ASSERT( "no error on seek to beginning, err = %d", serr == FS_OK, serr );
	ASSERT( "seeked to beginning correctly", byte_read == test_msg[0] );

	/* Seek to end, add data, seek to beginning, assert original data */
	/* was not overwritten and new data was appended to original data. */
	serr = file->seek( file, file->size( file, &temp_err ) );
	ASSERT( "no error on seek to end, err = %d", serr == FS_OK, serr );

	file->write( file, (uint8_t *) apnd_msg, strlen( apnd_msg ), &num_read );
	file->seek( file, BEGINNING_OF_FILE );
	file->read( file, (uint8_t *) msg, strlen( test_msg ), &num_read );
	ASSERT( "reads correct msg", strncmp( test_msg, msg, strlen( test_msg ) ) == 0 );

	/* Seek to appended message. */
	file->seek( file, strlen( test_msg ) );
	file->read( file, (uint8_t *) msg, strlen( apnd_msg ), &num_read );
	ASSERT( "reads correct appended msg", strncmp( apnd_msg, msg, strlen( apnd_msg) ) == 0 );

	filesystem->close( filesystem, file );
}

TEST( size )
{
	file_t			*file;
	fs_error_t			serr, temp_err;
	char *				test_msg = "this is a test message";
	char				long_test_msg[TEST_DATA_LEN];
	uint32_t			iter, byte_count;

	file = filesystem->open( filesystem, &temp_err, "st.txt", FS_CREATE_ALWAYS, USE_POLLING );

	/* Write a message to a new file, and assert the file is the same size as the written */
	/* message */
	file->write( file, (uint8_t *) test_msg, strlen( test_msg ), &byte_count );
	ASSERT( "file size is correct", file->size( file, &serr ) == strlen( test_msg ) );
	ASSERT( "no error on query", serr == FS_OK );

	/* Write a very long message and assert the file is the correct size */
	for( iter = 0; iter < TEST_DATA_LEN; ++iter )
	{
		long_test_msg[iter] = iter;
	}
	file->seek( file, file->size( file, &temp_err ) );
	file->write( file, (uint8_t *) long_test_msg, TEST_DATA_LEN, &byte_count );
	ASSERT( "long file size is correct", file->size( file, &serr ) == (TEST_DATA_LEN + strlen( test_msg )) );
	ASSERT( "no error on long query", serr == FS_OK );

	filesystem->close( filesystem, file );
}

TEST( position )
{
	file_t			*file;
	fs_error_t			perr, serr, werr, temp_err;
	char				long_test_msg[TEST_DATA_LEN];
	uint32_t			iter, file_pos, byte_count;

	//filesystem->close_all( filesystem );
	file = filesystem->open( filesystem, &perr, "pt.txt", FS_CREATE_ALWAYS, USE_POLLING );

	/* Create the long message. */
	for( iter = 0; iter < TEST_DATA_LEN; ++iter )
	{
		long_test_msg[iter] = iter;
	}

	/* Write a long msg, seek randomly in it, assert the position returns the location seeked */
	werr = file->write( file, (uint8_t *) long_test_msg, TEST_DATA_LEN, &byte_count );
	serr = file->seek( file, TEST_DATA_LEN >> 1 );
	file_pos = file->position( file, &perr );
	ASSERT( "no error on position query", perr == FS_OK );
	ASSERT( "correct position, should be %d, is %d, werr = %d, serr = %d, perr = %d", file_pos == TEST_DATA_LEN >> 1, TEST_DATA_LEN >> 1, file_pos, werr, serr, perr);

	/* Seek to the begining and test position. */
	file->seek( file, BEGINNING_OF_FILE );
	file_pos = file->position( file, &perr );
	ASSERT( "no error on beginning position", perr == FS_OK );
	ASSERT( "correct position on beginning", file_pos == BEGINNING_OF_FILE );

	/* Seek to the end and test position. */
	file->seek( file, file->size( file, &temp_err ) );
	file_pos = file->position( file, &perr );
	ASSERT( "no error on end position", perr == FS_OK );
	ASSERT( "correct position on end", file_pos == file->size( file, &temp_err ) && file_pos != 0 );

	filesystem->close( filesystem, file );
}

TEST( truncate )
{
	file_t			*file;
	fs_error_t			terr, temp_err;
	char				long_test_msg[TEST_DATA_LEN];
	uint32_t			iter, size;

	file = filesystem->open( filesystem, &terr, "tt.txt", FS_CREATE_ALWAYS, USE_POLLING );

	/* Create the long message and write it. */
	for( iter = 0; iter < TEST_DATA_LEN; ++iter )
	{
		long_test_msg[iter] = iter;
	}
	terr = file->write( file, (uint8_t *) long_test_msg, TEST_DATA_LEN, &size );

	/* truncate from end of file. */
	terr = file->seek( file, file->size( file, &temp_err ) );
	terr = file->truncate( file );
	size = file->size( file, &temp_err );
	ASSERT( "correct size after eof truncate", size == TEST_DATA_LEN );
	ASSERT( "no error on eof truncate", terr == FS_OK );

	/* seek halfway and test truncate */
	terr = file->seek( file, TEST_DATA_LEN >> 1 );
	terr = file->truncate( file );
	size = file->size( file, &temp_err );
	ASSERT( "correct file size after truncate", size == (TEST_DATA_LEN >> 1) );
	ASSERT( "no error from midway truncate", terr == FS_OK );

	/* truncate from beggining of file. */
	terr = file->seek( file, BEGINNING_OF_FILE );
	size = file->size( file, &temp_err );
	terr = file->truncate( file );
	size = file->size( file, &temp_err );
	ASSERT( "correct size after bof truncate", size == 0 );
	ASSERT( "no error on bof truncate", terr == FS_OK );

	filesystem->close_all( filesystem );
}

TEST( handle_status )
{
	file_t			*file;
	fs_error_t			err;

	filesystem->close_all( filesystem );

	file = filesystem->open( filesystem, &err, "tt.txt", FS_CREATE_ALWAYS, USE_POLLING );
	ASSERT( "handle busy", fs_handle_get_busy_status( (fs_handle_t *) file ) == HANDLE_BUSY );

	fs_handle_set_free( (fs_handle_t *) file );
	ASSERT( "clear busy", fs_handle_get_busy_status( (fs_handle_t *) file ) == HANDLE_FREE );
	fs_handle_set_busy( (fs_handle_t *) file );
	ASSERT( "set busy", fs_handle_get_busy_status( (fs_handle_t *) file ) == HANDLE_BUSY );

	filesystem->close( filesystem, file );
	ASSERT( "free after close", fs_handle_get_busy_status( (fs_handle_t *) file ) == HANDLE_FREE );
}


TEST( get_native )
{
	file_t			*file;
	fs_error_t		temp_err;

	file = filesystem->open( filesystem, &temp_err, "nt.txt", FS_CREATE_ALWAYS, USE_POLLING );

	ASSERT( "get native handle", fs_handle_get_native( (fs_handle_t *) file ) != NULL );

	filesystem->close_all( filesystem );
}

TEST( null_handle )
{
	file_t 		*file;
	file_null_t null_file_m;
	dir_t 		*dir;
	dir_null_t	null_dir_m;
	uint32_t 	count;
	fs_error_t 	err;

	initialize_file_null( &null_file_m, fs );
	initialize_dir_null( &null_dir_m, fs );
	file = (file_t *) &null_file_m;
	dir = (dir_t *) &null_dir_m;

	ASSERT( "null write not bound", file->write != NULL );
	ASSERT( "null read not bound", file->read != NULL );
	ASSERT( "null seek not bound", file->seek != NULL );
	ASSERT( "null size not bound", file->size != NULL );
	ASSERT( "null position not bound", file->position != NULL );
	ASSERT( "null truncate not bound", file->truncate != NULL );

	err = file->write( file, NULL, 0, &count );
	ASSERT( "null write does nothing", err == FS_NULL_HANDLE && count == 0 );

	err = file->read( file, NULL, 0, &count );
	ASSERT( "null read does nothing", err == FS_NULL_HANDLE && count == 0 );

	err = file->seek( file, 0 );
	ASSERT( "null seek does nothing", err == FS_NULL_HANDLE );

	count = file->size( file, &err );
	ASSERT( "null size does nothing", err == FS_NULL_HANDLE && count == 0 );

	count = file->position( file, &err );
	ASSERT( "null position does nothing", err == FS_NULL_HANDLE && count == 0 );

	err = file->truncate( file );
	ASSERT( "null truncate does nothing", err == FS_NULL_HANDLE );

	ASSERT( "null list not bound", dir->list != NULL );
	ASSERT( "null reset list not bound", dir->reset_list != NULL );

	dir->list( dir, &err );
	ASSERT( "null dir list", err == FS_NULL_HANDLE );

	err = dir->reset_list( dir );
	ASSERT( "null dir reset list", err == FS_NULL_HANDLE );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
	((fs_handle_t *) dir)->destroy( (fs_handle_t *) dir );
}

TEST( file_io_with_free_handle )
{
	file_t			*file;
	fs_error_t		err;
	uint8_t			null_msg[NULL_MSG_LEN];
	uint32_t		count;
	file_fatfs_t	handle;

	file = (file_t *) &handle;
	initialize_file_fatfs( &handle, fs );

	err = file->write( file, null_msg, NULL_MSG_LEN, &count );
	ASSERT( "free write does nothing, err = %d", err == FS_INVALID_OBJECT, err );

	err = file->read( file, null_msg, NULL_MSG_LEN, &count );
	ASSERT( "free read does nothing, err = %d", err == FS_INVALID_OBJECT, err );

	err = file->seek( file, 0 );
	ASSERT( "free seek does nothing, err = %d", err == FS_INVALID_OBJECT, err );

	count = file->size( file, &err );
	ASSERT( "free size does nothing, err = %d, count = %d", err == FS_INVALID_OBJECT && count == 0, err, count );

	count = file->position( file, &err );
	ASSERT( "free position does nothing, err = %d, count = %d", err == FS_INVALID_OBJECT && count == 0, err, count );

	err = file->truncate( file );
	ASSERT( "free truncate does nothing, err = %d", err == FS_INVALID_OBJECT, err );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( kill )
{
	file_t		*file;
	fs_error_t	err, temp_err;
	char		*msg = "This is a msg and some of it will be killed";
	char		*act = "is a msg and some of it will be killed";
	char		out_msg[100];
	uint32_t	msg_len = strlen( msg );
	uint32_t	act_len = strlen( act );
	uint32_t	byte_count;

	file = fs->open( fs, &temp_err, "kt.txt", FS_CREATE_ALWAYS, portMAX_DELAY );
	temp_err = file->write( file, (uint8_t *) msg, msg_len, &byte_count );
	err = file->kill( file, "ktb.txt", portMAX_DELAY, 0, 5 );
	ASSERT( "no error on kill, err = %d", err == FS_OK, err );

	file->read( file, (uint8_t *) out_msg, act_len, &byte_count );
	ASSERT( "kill performed correctly, read msg = %.*s", strncmp( out_msg, act, act_len ) == 0, act_len, msg );

	fs->close( fs, file );
}

TEST_SUITE( file_handle_api )
{
	uint32_t redo;
	kit = kitp;

	fs = kit->fs;
	filesystem = kit->fs;

	for( redo = 0; redo < HANDLE_TEST_FLUSH_LIMIT; ++redo )
	{
		ADD_TEST( fs_handle_methods_bound );
		ADD_TEST( fatfs_file_methods_bound );
		ADD_TEST( file_methods_bound );
		ADD_TEST( fatfs_dir_methods_bound );
		ADD_TEST( dir_methods_bound );
		ADD_TEST( write_read );
		ADD_TEST( seek );
		ADD_TEST( size );
		ADD_TEST( position );
		ADD_TEST( truncate );
		ADD_TEST( handle_status );
		ADD_TEST( get_native );
		ADD_TEST( null_handle );
		ADD_TEST( file_io_with_free_handle );
		ADD_TEST( kill );
	}

}
