/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_power_on
 * @author Brendan Bruner
 * @date Feb 11, 2015
 */

#include <states/state_relay.h>
#include <states/state.h>
#include <telecommands/telecommand.h>

#include <test_suites.h>
#include <dependency_injection.h>

#define TEST_STATE_CONFIG_NAME "tsln_l.txt"

static driver_toolkit_t *kit;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( query_state_id )
{
	state_relay_t relay;

	initialize_state_relay_simple( &relay, kit );

	ASSERT( "bring up id is %d, not %d",
			query_state_id( (state_t *) &relay.bring_up ) == STATE_BRING_UP_ID,
			query_state_id( (state_t *) &relay.bring_up ), STATE_BRING_UP_ID );
	ASSERT( "low power id is %d, not %d",
			query_state_id( (state_t *) &relay.low_power ) == STATE_LOW_POWER_ID,
			query_state_id( (state_t *) &relay.low_power ), STATE_LOW_POWER_ID);
	ASSERT( "science id is %d, not %d",
			query_state_id( (state_t *) &relay.science ) == STATE_SCIENCE_ID,
			query_state_id( (state_t *) &relay.science ), STATE_SCIENCE_ID );

	destroy_state_relay( &relay );
}

TEST_SUITE( system_states )
{
	kit = kitp;
	ADD_TEST( query_state_id );
}
