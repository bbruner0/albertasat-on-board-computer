/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_next_state.c
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <states/state_relay.h>
#include <core_defines.h>

static 	state_relay_t*			relay;
static 	filesystem_t*			fs;
static 	eps_mode_t 				eps_mode_state;

/* Dependency inject, into eps_t, a way to control what mode its in. */
static eps_mode_t eps_mode( eps_t *self )
{
	return eps_mode_state;
}

/*
static void set_eps_mode( eps_mode_t mode )
{
	eps_mode_state = mode;
}
*/


TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{

}

TEST( bring_up )
{
	ABORT_TEST( "not implemented" );
}

TEST( low_power )
{
	ABORT_TEST( "not implemented" );
}

TEST( science )
{
	ABORT_TEST( "not implemented" );
}

TEST_SUITE( next_state )
{
	typedef eps_mode_t (*eps_mode_method_t)( eps_t* );

	relay = (state_relay_t*) OBCMalloc( sizeof(state_relay_t) );
	fs = kitp->fs;
	if( relay == NULL )
	{
		UNIT_PRINT( "failure to allocate memory for test\n" );
		return;
	}
	uint8_t err = initialize_state_relay_simple( relay, kitp );
	if( err != STATE_RELAY_SUCCESS )
	{
		UNIT_PRINT( "failure to initialize object for testing\n" );
		OBCFree( relay );
		return;
	}

	/* dependency inject, into eps_t, a way to control its mode. */
	eps_mode_method_t old_mode;
	old_mode = kitp->eps->mode;
	kitp->eps->mode = eps_mode;

	ADD_TEST( bring_up );
	ADD_TEST( low_power );
	ADD_TEST( science );

	/* Restore operations altered by dependency injection. */
	kitp->eps->mode = old_mode;

	destroy_state_relay( relay );
	OBCFree( relay );
}
