/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_persistent_timer.c
 * @author Brendan Bruner
 * @date Sep 23, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <ptimer/ptimer.h>
#include <rtc.h>

extern driver_toolkit_t* kitp;
static ptimer_t timer;
static rtc_t *rtc;
#define TIMER_DURATION 6
#define TIMER_RESOLUTION 2
#define PT_LOG_FILE "ptlf.txt"

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

TEST( timer_expires )
{
	/* Assert the timer expires within the documented range. */
	uint32_t 	start_time;
	uint32_t 	end_time;
	uint32_t	duration;
	fs_error_t	err;
	bool_t		timer_err;

	/* Delete the timers log file to ensure it starts from the beginning. */
	err = kitp->fs->file_exists( kitp->fs, PT_LOG_FILE );
	if( err == FS_OK )
	{
		kitp->fs->delete( kitp->fs, PT_LOG_FILE );
	}

	timer_err = initialize_ptimer( &timer, TIMER_DURATION, TIMER_RESOLUTION, kitp->fs, PT_LOG_FILE );
	(void) timer_err;

	/* Get the current time before starting the timer. */
	start_time = rtc->seconds_since_epoch( rtc, UNIX_EPOCH );

	/* Wait until the timer expires, then get the new time. */
	timer.start( &timer );
	timer.is_expired( &timer, BLOCK_FOREVER );
	end_time = rtc->seconds_since_epoch( rtc, UNIX_EPOCH );

	duration = end_time - start_time;
	ASSERT( "timer ran incorrect amount of time, %ds", duration >= (TIMER_DURATION), duration );

	/* Assert persistent_timer_t::is_expired continues to return true. */
	ASSERT( "0 persistent_timer_t::is_expired is broken", timer.is_expired( &timer, USE_POLLING ) == true );

	timer.destroy( &timer );
}

TEST( timer_non_volatile )
{
	/* Asserting the timers duration is a function of the log file it uses and not variables */
	/* in ram. */
	uint32_t	duration;
	rtc_time_t	start_time, end_time;

	initialize_ptimer( &timer, TIMER_DURATION, TIMER_RESOLUTION, kitp->fs, PT_LOG_FILE );

	/* Get the current time before starting the timer. */
	rtc->get_time( rtc, &start_time );

	/* Wait until the timer expires, then get the new time. */
	timer.start( &timer );
	timer.is_expired( &timer, BLOCK_FOREVER );
	rtc->get_time( rtc, &end_time );

	duration = end_time.seconds - start_time.seconds;
	ASSERT( "expired timer ran for %ds", duration <= 1, duration );

	timer.destroy( &timer );
}

TEST_SUITE( persistent_timer )
{
	rtc = kitp->rtc;

	ADD_TEST( timer_expires );
	ADD_TEST( timer_non_volatile );
}
