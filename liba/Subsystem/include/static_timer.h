/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file static_timer
 * @author Brendan Bruner
 * @date Feb 19, 2015
 */
#ifndef INCLUDE_STATIC_TIMER_H_
#define INCLUDE_STATIC_TIMER_H_

typedef struct static_timer_t static_timer_t;
typedef unsigned char expired_status_t;

#define STATIC_TIMER_NOT_EXPIRED	((expired_status_t) 0)
#define STATIC_TIMER_EXPIRED		((expired_status_t) 1)

/**
 * @struct static_timer_t
 * @brief
 * 		Structure used for the static timer API
 *
 * @details
 * 		Structure used for the static timer API. The static timer acts
 * 		as a one time count down which persists through power cycles.
 *		In other words, power cycles do not change the duration remaining
 *		on this timer.
 *
 * @var static_timer_t::is_expired
 * 		A function pointer. The method has the prototype:
 * 			@code
 * 			expired_status_t is_expired( static_timer_t * );
 * 			@endcode
 * 		It takes a pointer to a static_timer_t structure. This must be the
 * 		same structure used to access this function pointer. The return value
 * 		is one of:
 * 			@code
 * 			STATIC_TIMER_EXPIRED
 * 			STATIC_TIMER_NOT_EXPIRED
 * 			@endcode
 */
struct static_timer_t
{
	void *data;
	expired_status_t (*is_expired)( static_timer_t * );
};

/**
 * @brief
 * 		Initialize a mock up of the static timer.
 *
 * @details
 * 		Initialize a mock up of the static timer. When the is_expired
 * 		method is called, it will return STATIC_TIMER_NOT_EXPIRED the
 * 		first time and STATIC_TIMER_EXPIRED everytime after.
 *
 * @param timer
 * 		A pointer to the static_timer_t structure to initialize.
 *
 * @memberof static_timer_t
 */
void initialize_static_timer_ram_soft( static_timer_t *timer );


/********************************************************************************/
/* Destructor																	*/
/********************************************************************************/
/**
 * @memberof ejection_pin_t
 * @brief destroy the structure
 * @details destroy the structure
 * @param[in] self A pointer to the structure to destroy
 */
void destroy_static_timer( static_timer_t *self );

#endif /* INCLUDE_STATIC_TIMER_H_ */
