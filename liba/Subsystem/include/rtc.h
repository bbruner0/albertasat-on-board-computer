/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file rtc.h
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */
#ifndef INCLUDE_RTC_H_
#define INCLUDE_RTC_H_

#include <stdint.h>
#include <time.h>
#include <dev/arm/ds1302.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define RTC_TIME_SET 1
#define RTC_TIME_NOT_SET 0
#define RTC_INIT 1
#define RTC_INIT_FAIL 0

/* This is QB50 required epoch. */ // TODO Make sure this is right. Hours = 12?
#define QB50_EPOCH &(rtc_time_t){	.seconds = 0, 		\
									.minutes = 0, 		\
									.hours = 0,			\
									.day_of_month = 0,	\
									.month = 0,			\
									.year = 2000		\
								}
#define UNIX_EPOCH  &(rtc_time_t){	.seconds = 0, 		\
									.minutes = 0, 		\
									.hours = 0,			\
									.day_of_month = 0,	\
									.month = 0,			\
									.year = 1970		\
								}
//#define OBC_EPOCH UNIX_EPOCH

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct rtc_t
 * @brief
 * 		Structure used with RTC functions. This is a singleton class.
 * @details
 * 		Structure used with RTC functions. It is a singleton, meaning multiple
 * 		instances of this structure will act on the same real time clock.
 *
 * @var rtc_t::enable
 * 		A function pointer with declare:
 * 			@code
 * 			void enable( rtc_t *rtc );
 * 			@endcode
 * 		This method enables the rtc to run. The inverse is rtc_t::disable which
 * 		will stop the rtc from running.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>rtc</b>: A pointer to the rtc_t structure to enable.</li>
 * 		</ul>
 *
 * @var rtc_t::disable
 * 		A function pointer with declare:
 * 			@code
 * 			void disable( rtc_t *rtc );
 * 			@endcode
 * 		This method siables the rtc. The inverse is rtc_t::enable which
 * 		will start the rtc.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>rtc</b>: A pointer to the rtc_t structure to disable.</li>
 * 		</ul>
 *
 * @var rtc_t::set_time
 * 		A function pointer with declare:
 * 			@code
 * 			void uint8_t set_time( rtc_t *rtc, rtc_time_t *time );
 * 			@endcode
 * 		Sets the time in rtc and returns a success / failure error code.
 * 		If the time being set is out of bounds, a failure error code is returned
 * 		and the time will default back to its state before the method was called.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>rtc</b>: A pointer to the rtc_t structure to set the time in.</li>
 * 		<li><b>time</b>: A pointer to a rtc_time_t structure containing the
 * 		time to set the rtc to.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br><b>RTC_TIME_SET</b>: The time was successfully set.
 * 		<br><b>RTC_TIME_NOT_SET</b>: The time was not set. The time before the function
 * 		was called remains the current time.
 *
 * @var rtc_t::get_time
 * 		A function pointer with prototype:
 * 			@code
 * 			void get_time( rtc_t *rtc, rtc_time_t *time );
 * 			@endcode
 * 		This method gets the current time in the rtc.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>rtc</b>: A pointer to the rtc_t structure to get the time from.</li>
 * 		<li><b>time</b>: A pointer to the rtc_time_t structure to put the current time in.</li>
 * 		</ul>
 *
 * @var rtc_t::seconds_since_epoch
 * 		A function with prototype:
 * 			@code
 * 			uint32_t second_since_epoch( rtc_t *rtc, rtc_time_t* epoch );
 * 			@endcode
 * 		The method gets the seconds since the epoch passed in as a function argument.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>rtc</b>: A pointer to the rtc_t structure to get the time since the epoch from.</li>
 * 		<li><b>epoch</b>: The epoch to calculate seconds from. This can be custom defined or use
 * 		one of the predefined epochs: <b>QB50_EPOCH</b> or <b>UNIX_EPOCH</b>.
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>The seconds since the epoch.
 */
typedef struct rtc_t rtc_t;

/**
 * @struct rtc_time_t
 * @brief
 * 		Contains a calendar time.
 * @details
 * 		Contains a calendar time. The fields are:
 * 		<br>seconds, [0,59]
 * 		<br>minutes, [0,59]
 * 		<br>hours, [0,23]
 * 		<br>day_of_month, [0,30]
 * 		<br>month, [0,11]
 * 		<br>year, [0,4000]
 */
typedef struct rtc_time_t rtc_time_t;

/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct rtc_time_t
{
	uint32_t seconds;
	uint32_t minutes;
	uint32_t hours;
	uint32_t day_of_month;
	uint32_t month;
	uint32_t year;
};

struct rtc_t
{
	void (*enable)( rtc_t * );
	void (*disable)( rtc_t * );
	uint8_t (*set_time)( rtc_t *, rtc_time_t * );
	void (*set_ds1302_time)(struct ds1302_clock *);
	void (*get_time)( rtc_t *, rtc_time_t * );
	time_t (*get_ds1302_time)( void );
	uint32_t (*seconds_since_epoch)( rtc_t *, rtc_time_t* );
	struct
	{
		time_t (*mktime)( struct tm* );
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor / Destructor	Declare												*/
/********************************************************************************/
/**
 * @memberof rtc_t
 * @brief
 * 		Initialize an rtc_t structure.
 * @details
 * 		Initialize an rtc_t structure for use on the lpc1769 board.
 * 		The structure is a singleton and is automatically disabled.
 * 		It can be enabled with rtc_t::enable( ).
 * @param rtc[in]
 * 		A pointer to the rtc_t structure to initialize.
 * @returns
 * 		A pointer to the structure. NULL if the singleton could not be initialized.
 */
rtc_t* get_rtc_lpc( );

/**
 * @memberof rtc_t
 * @brief
 * 		Initialize an rtc_t structure.
 * @details
 * 		Initialize an rtc_t structure for use on the nanomind board.
 * 		The structure is a singleton and is automatically disabled.
 * 		It can be enabled with rtc_t::enable( ).
 * @param rtc[in]
 * 		A pointer to the rtc_t structure to initialize.
 * @returns
 * 		A pointer to the structure. NULL if the singleton could not be initialized.
 */
rtc_t* get_rtc_nanomind( );

/**
 * @memberof rtc_t
 * @brief
 * 		destroy an rtc_t structure.
 * @details
 * 		destroy an rtc_t structure. Call this when you are done with
 * 		the structure.
 * @param rtc[in]
 * 		A pointer to the rtc_t structure to destroy.
 */
void destroy_rtc( rtc_t *rtc );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_RTC_H_ */
