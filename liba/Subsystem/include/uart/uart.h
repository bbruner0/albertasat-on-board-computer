/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file uart
 * @author Brendan Bruner
 * @date Mar 28, 2015
 */
#ifndef INCLUDE_UART_H_
#define INCLUDE_UART_H_

#include <stdint.h>
#include <portable_types.h>

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct uart_t
 * @brief
 * 		Abstract structure to send data over a uart
 * @details
 * 		Abstract structure to send data over a uart
 * @var uart_t::send
 * 		<b>Public</b>
 * 		@code
 * 			uint32_t send( uart_t*, uint8_t const* data, uint32_t length );
 * 		@endcode
 * 		Put data on the serial line.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>data</b>: The data which will be put on the bus.</li>
 * 		<li><b>length</b>: The number of bytes in the <b>data</b> array.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>The number of bytes actually put on the bus.
 * @var uart_t::read
 * 		<b>Public</b>
 * 		@code
 * 			uint32_t read( uart_t*, uint8_t* data, uint32_t length );
 * 		@endcode
 * 		Read data off the uart.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>data</b>: An array where the read data will be copied to.</li>
 * 		<li><b>length</b>: The length of the <b>data</b> array.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>The number of bytes actually read.
 * @var uart_t::available
 * 		<b>Public</b>
 * 		@code
 * 			uint32_t available( uart_t* );
 * 		@endcode
 * 		Poll the number of bytes in the uarts read buffer.
 *
 * 		<b>Returns</b>
 * 		<br>The number of bytes which can be read by uart_t::read.
 * @var uart_t::reset
 * 		<b>Public</b>
 * 		@code
 * 			void reset( uart_t* );
 * 		@endcode
 * 		Flush both the tx and rx buffers of the uart.
 * @var uart_t::reset_rx
 * 		<b>Public</b>
 * 		@code
 * 			void reset_rx( uart_t* );
 * 		@endcode
 * 		Flush only the rx buffer of the uart.
 * @var uart_t::destroy
 * 		<b>Public</b>
 * 		@code
 * 			void destroy( uart_t* );
 * 		@endcode
 * 		Destructor.
 */
typedef struct uart_t uart_t;

/********************************************************************************/
/* Structure Define																*/
/********************************************************************************/
struct uart_t
{
	uint32_t (*send)( uart_t*, uint8_t const*, uint32_t );
	uint32_t (*read)( uart_t*, uint8_t*, uint32_t );
	uint32_t (*available)( uart_t* );
	void (*reset)( uart_t* );
	void (*reset_rx)( uart_t* );
	void (*destroy)( uart_t* );
};

/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
/**
 * @memberof uart_t
 * @protected
 * @brief
 * 		Constructor for uart_t structures.
 * @details
 * 		Constructor for uart_t structures.
 * @returns
 * 		true if successfully constructed, false otherwise.
 */
bool_t initialize_uart_( uart_t* );

#endif /* INCLUDE_UART_H_ */
