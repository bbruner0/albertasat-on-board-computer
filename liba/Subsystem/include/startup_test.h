/*
 * Copyright (C) 2015 Alex Hamilton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file startup_test.h
 * @author Alex Hamilton
 * @date 2014-12-28
 */

#ifndef STARTUP_TEST_H_
#define STARTUP_TEST_H_
/*
int subsystem_test(void);
*/

#endif /*STARTUP_TEST_H_*/
