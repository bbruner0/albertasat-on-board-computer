/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file i2c.h
 * @author Brendan Bruner
 * @date May 6, 2015
 */
#ifdef LPC1769
#ifndef INCLUDE_I2C_H_
#define INCLUDE_I2C_H_

typedef struct i2c_t i2c_t;

enum i2c_channel_t
{
	I2C_CHANNEL_0 = 0,
	I2C_CHANNEL_1 = 1,
	I2C_CHANNEL_2 = 2
};

struct i2c_t
{
	uint32_t (*master_transfer)( i2c_t *, uint32_t, uint8_t *, uint32_t, uint8_t *, uint32_t );
};

#endif /* INCLUDE_I2C_H_ */
#endif
