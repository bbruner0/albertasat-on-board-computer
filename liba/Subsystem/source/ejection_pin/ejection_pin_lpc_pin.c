/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ejection_pin_lpc_pin
 * @author Brendan Bruner
 * @date Feb 24, 2015
 */

#include "ejection_pin.h"
#include "portable_types.h"

/************************************************************************/
/* LPC GPIO ejection pin												*/
/************************************************************************/
void initialize_ejection_pin_lpc( ejection_pin_t *driver )
{
	DEV_ASSERT( driver );
	return;
}
