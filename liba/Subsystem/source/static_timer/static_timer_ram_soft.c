/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file static_timer_ram_soft
 * @author Brendan Bruner
 * @date Feb 23, 2015
 */

#include "static_timer.h"
#include "portable_types.h"

#define FIRST_CALL 			((void *) 0)
#define AFTER_FIRST_CALL	((void *) 1)

static expired_status_t ram_soft_is_expired( static_timer_t *timer )
{
	DEV_ASSERT( timer );

	if( timer->data == FIRST_CALL )
	{
		timer->data = AFTER_FIRST_CALL;
		return STATIC_TIMER_NOT_EXPIRED;
	}
	return STATIC_TIMER_EXPIRED;
}

void initialize_static_timer_ram_soft( static_timer_t *timer )
{
	DEV_ASSERT( timer );

	timer->data = FIRST_CALL;
	timer->is_expired = &ram_soft_is_expired;
}
