/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file comm_nanomind.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_COMM_COMM_NANOMIND_H_
#define INCLUDE_COMM_COMM_NANOMIND_H_

#include "comm.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct comm_nanomind_t
 * @extends comm_t
 * @brief
 * 		Provides an API to talk to Gomspace's nanocomm.
 * @details
 * 		Provides an API to talk to Gomspace's nanocomm.
 */
typedef struct comm_nanomind_t comm_nanomind_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct comm_nanomind_t
{
	comm_t s_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof comm_nanomind_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_comm_nanomind( comm_nanomind_t * );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/

int com_download(char * mem);
int com_init(char * mem);
int com_set(char * name, char * value,comm_t *com);
int com_getall(comm_t *com);
int com_save(char * from, char * to);


#endif /* INCLUDE_COMM_COMM_NANOMIND_H_ */
