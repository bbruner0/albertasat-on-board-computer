
#ifndef CMD_PARAM_H_
#define CMD_PARAM_H_

#ifdef NANOMIND

#include <comm/comm.h>
#include <command/command.h>

int cmd_rparam_set_autosend(struct command_context *ctx);
int cmd_rparam_list(struct command_context *ctx);
int cmd_rparam_init(struct command_context *ctx);
int cmd_rparam_download(struct command_context *ctx);
int cmd_rparam_send(struct command_context *ctx);
int cmd_rparam_get(struct command_context *ctx);
int cmd_rparam_getall(struct command_context *ctx);
int cmd_rparam_set(struct command_context *ctx);
int cmd_rparam_print(struct command_context *ctx);
int cmd_rparam_copy(struct command_context *ctx);
int cmd_rparam_load(struct command_context *ctx);
int cmd_rparam_save(struct command_context *ctx);
int cmd_rparam_clear(struct command_context *ctx);
int cmd_rparam_reset(struct command_context *ctx);

void comm_read_out(comm_t *com);

#endif /* NANOMIND */
#endif /* CMD_PARAM_H_ */
