/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file deployment_lines.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_HUB_DEPLOYMENT_LINES_H_
#define INCLUDE_HUB_DEPLOYMENT_LINES_H_

#include "hub.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define ANTENNA_DEPLOYMENT_LINE			HUB_DEPLOYMENT_LINE1
#define DFGM_BOOM_DEPLOYMENT_LINE 		HUB_DEPLOYMENT_LINE0

#endif /* INCLUDE_HUB_DEPLOYMENT_LINES_H_ */
