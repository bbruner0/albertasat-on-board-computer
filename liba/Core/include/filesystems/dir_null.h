/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dir_null.h
 * @author Brendan Bruner
 * @date Aug 17, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_DIR_NULL_H_
#define INCLUDE_FILESYSTEMS_FATFS_DIR_NULL_H_

#include "dir.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dir_null_t
 * @extends dir_t
 * @brief
 * 		This structure enables all dir_t methods to be used safetly with no effect
 * 		at all.
 * @details
 * 		This structure enables all dir_t methods to be used safetly with no effect
 * 		at all.
 * @var dir_null_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 */
typedef struct dir_null_t dir_null_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dir_null_t
{
	dir_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dir_null_t
 * @brief
 * 		Initialize dir_null_t structure.
 * @details
 *		Initialize dir_null_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: On successful initialization.
 * 		<br><b>FS_HANDLE_FAILURE</b>: On failure to initialize. Structure is not
 * 		safe to use.
 */
uint8_t initialize_dir_null( dir_null_t *, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_DIR_NULL_H_ */
