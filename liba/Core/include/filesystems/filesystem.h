/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem.h
 * @author Brendan Bruner
 * @date May 7, 2015
 */
#ifndef INCLUDE_FILESYSTEM_H_
#define INCLUDE_FILESYSTEM_H_

#include <portable_types.h>

#include "fs_common.h"
#include "file.h"
#include "file_null.h"
#include "dir.h"
#include "dir_null.h"


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FILESYSTEM_SUCCESS	1
#define FILESYSTEM_FAILURE	0


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct filesystem_t
 * @brief
 *		Abstract structure for opening, closing, and deleting files in an SD card.
 *
 * @details
 * 		Abstract structure for opening, closing, and deleting files in an SD card.
 * 		In order to do work on opened files, the fs_handle_t structure is needed.
 * 		Therefore, a file which does IO on files will typically have these two
 * 		includes:
 * 		<br>#include <filesystem/filesystem.h>
 * 		#include <filesystem/fs_handle.h>
 *
 * @var filesystem_t::open
 * 		<b>Public</b>
 * 			@code
 *				file_t *open( filesystem_t *fs, fs_error_t *err, char const *name, fs_open_attribute_t type, block_time_t block );
 *			@endcode
 *		This method opens a file and returns a handle which allows io to be performed
 *		on it. The method does not check for duplicate file opening. fs_handle_t structures
 *		returned by this method must NEVER be destroyed with fs_handle_t::destroy_fs_handle( ).
 *		They must be closed with filesystem_t::close or filesystem_t::close_all when no longer
 *		in use. The file pointer (adjustable with fs_handle_t::seek) should be at the
 *		beginning of the file, however, this is not guaranteed.
 *		This method can be instructed to block - if too many files are open - until
 *		there is room to open a new file.
 *
 *		<b>Parameters</b>
 *		<ul>
 *		<li><b>fs</b>: A pointer to the filesystem_t structure initialized for the current hardware and
 *		desired filesystem.</li>
 *		<li><b>err</b>: A pointer to an error code. Dereferencing this pointer after function
 *		completion gives that status of the file which was opened. Use IGNORE_ERROR as
 *		the argument to ignore the error.</li>
 *		<li><b>name</b>: Name of the file to open.</li>
 *		<li><b>type</b>: Specify how the file is to be opened.</li>
 *		<li><b>block</b>: Time to block for in ms until success.</li>
 *		</ul>
 *
 *		<b>Returns</b>
 *		<br>A pointer to a file_t structure to perform file io on the freshly opened file.
 *		This will be the null file handle if the method times out.
 *
 * @var filesystem_t::close
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t close( filesystem_t *fs, file_t *file );
 * 			@endcode
 * 		This method will close an open file.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t structure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>file</b>: A pointer to the file_t structure of the file which will be closed.
 * 		This is the same pointer returned by filesystem_t::open.</li>
 * 		</ul>
 *
 *		<b>Returns</b>
 *		<br>An error code.
 *
 * @var filesystem_t::close_all
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t close_all( filesystem_t *fs );
 * 			@endcode
 * 		Close all open files on the SD card being accessed by <b>fs</b>.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t structure initializid for the
 * 		current hardware and desired filesystem.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::delete
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t delete( filesystem_t *fs, char const *name );
 * 			@endcode
 * 		This method deletes a file.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t structure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>name</b>: Name of the file to delete.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::delete_handle
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t delete_handle( filesystem_t *fs, file_t *handle );
 * 			@endcode
 * 		This method deletes the file opened with <b>handle</b>. After successful completion,
 * 		the handle is invalidated (ie, it cannot be used for file io).
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t structure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>handle</b>: A pointer to the handle which has the open file to delete.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::reformat
 *		<b>Public</b>
 *		@code
 *			fs_error_t reformat( filesystem_t *fs );
 *		@endcode
 *		Reformats the entire SD card. This method will gracefully close all open
 *		files before reformatting.
 *
 *		<b>Parameters</b>
 *		<ul>
 *		<li><b>fs</b>: A pointer to the filesystem_t structure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::rename
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t rename( filesystem_t *fs, char const *old_name, char const *new_name );
 * 			@endcode
 * 		Renames the file with <b>old_name</b> to be <b>new_name</b>.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t strucure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>old_name</b>: The name of the file to rename.</li>
 * 		<li><b>new_name</b>: The new name the file will have.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::rename_handle
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t rename_handle( filesystem_t *fs, file_t *handle, char const *new_name );
 * 			@endcode
 * 		Renames the file opened by <b>handle</b> to have the name <b>new_name</b>.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t strucure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains the file to
 * 		be renamed.</li>
 * 		<li><b>new_name</b>: The new name the file will have.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::remap
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t remap( filesystem_t *fs, file_t *handle, char const *name, fs_open_attribute_t type );
 * 			@endcode
 * 		This method takes the fs_handle_t <b>handle</b> and changes the file it is operating on. The function
 * 		does not block to accomplish this. This method will fail if the <b>handle</b> is not already open
 *		on a file, ie, it must be an file_t structure returned by filesystem_t::open with an error code
 *		of FS_OK.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t strucure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>handle</b>: A pointer to the file_t structure to map to a different file.</li>
 * 		<li><b>name</b>: Name of the file to open.</li>
 * 		<li><b>type</b>: Attribute to use when opening the file.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code
 *
 * @var filesystem_t::close_dir
 * 		<b>Public</b>
 * 		@code
 * 			fs_error_t close_dir( filesystem_t *fs, dir_t *dir );
 * 		@endcode
 * 		Closes an fs_dir_t structure returned by filesystem_t::open_dir. Note, only call this function
 * 		on fs_dir_t structures returned by filesystem_t::open_dir
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>fs</b>: A pointer to the filesystem_t strucure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>dir</b>: The dir_t structure to close. Must have been returned by filesystem_t::open_dir.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var filesystem_t::open_dir
 * 		<b>Public</b>
 * 		@code
 *			dir_t *open_dir( filesystem_t *fs, fs_error_t *err, char const *dir_name, block_time_t block );
 *		@endcode
 *		Opens a directory structure which can be used to take actions on the open directory. See fs_dir_t for
 *		actions that can be taken on a directory. The returned structure must be closed with a call to
 *		filesystem_t::close_dir only if the error code is FS_OK when no longer needed.
 *		Never call destroy_fs_dir( ) on the returned structure.
 *
 *		<b>Parameters</b>
 *		<ul>
 *		<li><b>fs</b>: A pointer to the filesystem_t strucure initialized for the current hardware
 * 		and desired filesystem.</li>
 * 		<li><b>err</b>:</li>
 * 		<li><b>dir_name</b>:</li>
 * 		<li><b>block</b>:</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>A pointer to an dir_t structure. If there was a failure the returned pointer is still
 * 		valid, however, using its functions will do nothing.
 *
 * @var filesystem_t::file_exists
 * 		<b>Public</b>
 * 		@code
 * 			fs_error_t filesystem_t::file_exists( filesystem_t *, char const *name );
 * 		@endcode
 * 		Query if a file exists.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>name</b>: Name of the file to query for existance.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br><b>FS_OK</b>: If the file exists.
 * 		<br><b>FS_NO_FILE</b>: The file doesn't exists
 * 		<br>Any other fs_error_t code.
 *
 * @var filesystem_t::destroy
 * 		<b>Public</b>
 * 		@code
 * 			void destroy( filesystem_t *self );
 * 		@endcode
 * 		Destructor for filesystem_t structures.
 *
 * @var filesystem_t::_null_file_
 * 		<b>Private</b>
 * 		An fs_handle_t which does nothing. This is returned by filesystem_t::open
 * 		when there is failure.
 *
 * @var filesystem_t::_null_dir_
 * 		<b>Private</b>
 * 		An fs_dir_t which does nothing. This is returned by filesystem_t::open_dir
 * 		when there is failure.
 */

/** Attributes for opening files with filesystem_t::open. */
typedef enum {
	FS_OPEN_EXISTING = 0,	/*!< Open an existing file, fails if the file doesn't exist. */
	FS_OPEN_ALWAYS = 1,		/*!< Always open the file. */
	FS_CREATE_NEW = 2,		/*!< Create new file, fails if the file already exists. */
	FS_CREATE_ALWAYS = 3	/*!< Create new file, over writes existing file. */
} fs_open_attribute_t;


/********************************************************************************/
/* Structure Definition															*/
/********************************************************************************/
struct filesystem_t {
	file_null_t *_null_file_;
	dir_null_t	*_null_dir_;

	file_t *(*open)(filesystem_t *, fs_error_t *, char const *, fs_open_attribute_t, block_time_t );
	fs_error_t (*close)(filesystem_t *, file_t *);
	fs_error_t (*close_all)( filesystem_t * );
	fs_error_t (*delete)(filesystem_t *, char const *);
	fs_error_t (*delete_handle)(filesystem_t *, file_t *);
	fs_error_t (*reformat)(filesystem_t *);
	fs_error_t (*rename)( filesystem_t *, char const *, char const * );
	fs_error_t (*rename_handle)( filesystem_t *, file_t *, char const * );
	fs_error_t (*remap)( filesystem_t *, file_t *, char const *, fs_open_attribute_t );
	fs_error_t (*close_dir)( filesystem_t *, dir_t * );
	dir_t *(*open_dir)( filesystem_t *, fs_error_t *, char const *, block_time_t  );
	fs_error_t (*file_exists)( filesystem_t *, char const * );
	void (*destroy)( filesystem_t * );
};


/********************************************************************************/
/* Public Methods																*/
/********************************************************************************/



/********************************************************************************/
/* Protected Methods															*/
/********************************************************************************/
/**
 * @memberof filesystem_t
 * @brief
 * 		Used to get the null fs_handle_t of the filesystem.
 * @details
 * 		Used to get the null fs_handle_t of the filesystem.
 */
file_t *_filesystem_get_null_file( filesystem_t * );

/**
 * @memberof filesystem_t
 * @protected
 * @brief
 * 		Used to get the null fs_dir_t of the filesystem.
 * @details
 * 		Used to get the null fs_dir_t of the filesystem.
 */
dir_t *_filesystem_get_null_dir( filesystem_t * );

/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
/**
 * @memberof filesystem_t
 * @protected
 * @brief
 * 		Used to initialize a filesystem.
 * @details
 * 		Used to initialize a filesystem. This method is protected since the filesystem
 * 		structure is abstract.
 * @returns
 * 		<b>FILESYSTEM_SUCCESS</b>: On success
 * 		<br><b>FILESYSTEM_FAILURE</b>: On failure. The structure is not safe to use.
 */
uint8_t _initialize_filesystem( filesystem_t *fs );


#endif /* INCLUDE_FILESYSTEM_H_ */
