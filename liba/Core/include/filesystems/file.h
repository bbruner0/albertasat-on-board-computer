/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file file_t.h
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_FILE_T_H_
#define INCLUDE_FILESYSTEMS_FATFS_FILE_T_H_

#include "fs_handle.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct file_t
 * @extends fs_handle_t
 * @brief
 * 		Abstract structure for a file handle to do IO with an open file.
 *
 * @details
 * 		Abstract structure for a file handle to do IO with an open file.
 * 		This structure provides access to functions
 * 		which can write, read, seek, query size, location, and truncate open
 * 		files. Handles should not be shared between different tasks without
 * 		proper synchronization! This means a task should never use the handle
 * 		unless every other task with access to the handle is not using it.
 *
 * 		These handles are never to be initialized directly. They are returned
 * 		by filesystem_t::open - initialized and ready to do IO on the file
 * 		which was requested to be open.
 *
 * @var file_t::write
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t write( file_t *handle, uint8_t *msg, uint32_t size, uint32_t *written );
 * 			@endcode
 * 		This method will write data to file. The file pointer (adjustable with file_t::seek)
 * 		advances the number of bytes written.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		<li><b>msg</b>: A pointer to the array containing the data to write to file.</li>
 * 		<li><b>size</b>: The number of bytes in the array pointed to be msg.</li>
 * 		<li><b>written</b>: The number of bytes actually written to file. Use
 * 		IGNORE_WRITTEN_COUNT to avoid providing a valid pointer. if written < size this
 * 		means the volume is full and nothing else</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var file_t::read
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t (*read)( file_t *handle, uint8_t *msg, uint32_t size, uint32_t *read );
 * 			@endcode
 * 		This method will read data from file. The file pointer (adjustable with file_t::seek)
 * 		advances the number of bytes read.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		<li><b>msg</b>: A pointer to an array where data read from file will be put.</li>
 * 		<li><b>size</b>: The number of bytes to read from file. <b>size</b> must be
 * 		greater than the length of the <b>msg</b> array.</li>
 * 		<li><b>read</b>: The number of bytes actually read from file. This variable
 * 		is a pointer to the location to save the number of bytes read. There is
 * 		only one case where read < size, when the end of file was reached.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var file_t::seek
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t seek( file_t *handle, uint32_t position );
 * 			@endcode
 * 		This method seeks to a new position in the open file. Like arrays, position
 * 		0 is the first byte.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		<li><b>position</b>: The position to seek to in file. The macro BEGINNING_OF_FILE
 * 		can be used to seek to the beginning of the file.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var file_t::size
 * 		<b>Public</b>
 * 		@code
 * 			uint32_t size( file_t *handle, fs_error_t *err );
 * 		@endcode
 * 		Get the size, in bytes, of file.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		<li><b>err</b>: A pointer to an fs_error_t enum. If this function encounters
 * 		an error, it will be saved here. Use the macro IGNORE_ERROR to avoid allocating
 * 		memory for an fs_error_t enum.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>The size of the file in bytes. Returns 0 if an error is encounters. Check
 * 		the <b>err</b> variable for more information.
 *
 * @var file_t::position
 *		<b>Public</b>
 *			@code
 *				uint32_t position( file_t *handle, fs_error_t *err );
 *			@endcode
 *		This method gets the current position of the cursor in file.
 *
 *		<b>Parameters</b>
 *		<ul>
 *		<li><b>handle</b>: A pointer to the file_t structure which contains
 *		the open file.</li>
 *		<li><b>err</b>: A pointer to the location to save the error code of this
 *		method. Use the macro IGNORE_ERROR to avoid allocating memory for this
 *		argument.</li>
 *		</ul>
 *
 *		<b>Returns</b>
 *		<br>The position of the cursor in file.
 *
 * @var file_t::truncate
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t truncate( file_t *handle );
 * 			@endcode
 * 		This method deletes all bytes in file from the file pointer (adjustable
 * 		with file_t::seek) to the end of file.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var file_t::kill
 * 		<b>Public</b>
 * 			@code
 * 				fs_error_t kill
 * 				(
 * 					file_t *handle,
 * 					char const *buffer,
 * 					TickType_t time_out,
 * 					uint32_t begin,
 * 					uint32_t end
 * 				);
 * 			@endcode
 * 		This method deletes bytes from the file. It will seek to position <b>begin</b>
 * 		in the file and remove all bytes from <b>begin</b> to <b>end</b> in the file. It deletes
 * 		a total of <b>end</b> - <b>begin</b> bytes. It effectively highlights the bytes and hits
 * 		the metaphorical delete key.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>handle</b>: A pointer to the file_t structure which contains
 * 		the open file.</li>
 * 		<li><b>buffer</b>: The name to use for a buffer file. This must be a unique name
 * 		in the filesystem. If it is not, all data in that file will be lost.</li>
 * 		<li><b>time_out</b>: The time in ms to block for when trying to open a delete
 * 		buffer file. If time out is reached when blocking, the method returns without
 * 		killing any bytes.</li>
 * 		<li><b>begin</b>: The seek location in file to start deleting bytes.</li>
 * 		<li><b>end</b>: The seek location in file to stop deleting bytes. The data pointed
 * 		by <b>end</b> will not be deleted (ie, the byte in front of <b>end</b> is not
 * 		deleted, but the byte behind it is).</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>An error code. If FS_KILL_SWAP is returned this means that a file by the name of <b>buffer</b>
 * 		exists which contains all the data that was in the file <b>handle</b> with the successful kill of
 * 		bytes from <b>begin</b> to <b>end</b>. In addition, it means <b>handle</b> may or may not be a valid
 * 		handle and <b>buffer</b> must be opened to recover data in the case it is not valid.
 *
 * @var file_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 */
typedef struct file_t file_t;

/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
 struct file_t
 {
	fs_handle_t _super_; /* MUST be first struct variable. */

	fs_error_t (*write)( file_t *, uint8_t *, uint32_t, uint32_t * );
	fs_error_t (*read)( file_t *, uint8_t *, uint32_t, uint32_t * );
	fs_error_t (*seek)( file_t *, uint32_t );
	uint32_t (*size)( file_t *, fs_error_t * );
	uint32_t (*position)( file_t *, fs_error_t * );
	fs_error_t (*truncate)( file_t * );
	fs_error_t (*kill)( file_t *, char const *, block_time_t, uint32_t, uint32_t );
	void (*close)( file_t* );
 };


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof file_t
 * @protected
 * @brief
 * 		Initialize an abstract file_t structure.
 * @details
 *		Initialize an abstract file_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @param type[in]
 * 		The type of the handle.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: On successful initialization.
 * 		<br><b>FS_HANDLE_FAILURE</b>: On failure to initialize. Structure is not
 * 		safe to use.
 */
uint8_t _initialize_file( file_t *, filesystem_t *fs, fs_handle_type_t type );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_FILE_T_H_ */
