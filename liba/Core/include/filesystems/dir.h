/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dir_t.h
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_DIR_H_
#define INCLUDE_FILESYSTEMS_DIR_H_

#include "fs_handle.h"


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dir_t
 * @extends fs_handle_t
 * @brief
 * 		Abstract handle for doing IO with a directories properties
 * @details
 * 		Abstract handle for doing IO with a directories properties.
 * @var dir_t::list
 * 		<b>Public</b>
 * 		@code
 * 			char const *list( dir_t *dir, fs_error_t *err );
 * 		@endcode
 * 		This method will iteratively list all files in the directory. Each call will find
 * 		one file in the current directory with a succesive call finding a successive file. When all
 * 		files and directories have been read out a NULL string will be returned, "\0". To reset
 * 		and start listing at the beginning call fs_dir_t::reset.
 *
 * 		Do not write to the location pointed to. The string pointed to is null terminated.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>err</b>: Error code.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>A pointer to a null terminated string which is the name of a file or directory in
 * 		the directory.
 *
 * @var dir_t::reset_list
 * 		<b>Public</b>
 * 		@code
 * 			fs_error_t reset_list( dir_t *dir );
 * 		@endcode
 * 		This function will reset the listing from dir_t::list.
 *
 * 		<b>Returns</b>
 * 		<br>An error code.
 *
 * @var dir_t::_super_
 * 		<b>Prviate</b>
 * 		Super structure data.
 *
 * @var dir_t::_file_name_
 * 		<b>Private</b>
 * 		This what the return value of dir_t::list points to.
 */
typedef struct dir_t dir_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dir_t
{
	fs_handle_t	_super_;

	char _file_name_[FILESYSTEM_MAX_NAME_LENGTH+1];

	char const *(*list)( dir_t *, fs_error_t *);
	fs_error_t (*reset_list)( dir_t * );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dir_t
 * @protected
 * @brief
 * 		Initializes a dir_t structure.
 * @details
 * 		Initializes a dir_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @param type[in]
 * 		The type of the handle.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: Initialized successfully.
 * 		<br><b>FS_HANDLE_FAILURE</b>: Initialization failed. Structure not safe to use.
 */
uint8_t _initialize_dir( dir_t *, filesystem_t *fs, fs_handle_type_t type );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof dir_t
 * @protected
 * @brief
 * 		Set the file name being listed.
 * @details
 * 		Sets the file name which is being listed by dir_t::list. For example,
 * 		dir_t::list finds the name of the file to list, calls this method and sets
 * 		that file name, then returns a pointer to that string with _dir_get_file_name( ).
 * @param name[in]
 * 		The file name to set.
 */
void _dir_set_file_name( dir_t *, char const *name );

/**
 * @memberof dir_t
 * @protected
 * @brief
 * 		Gets the file name being listed.
 * @details
 * 		Gets the file name which is being listed by dir_t::list. For example,
 * 		dir_t::list finds the name of the file to list, calls _dir_set_file_name( )
 * 		to set that name, then calls this method to return a pointer to that string.
 * 		The returned string must never be written to.
 * @returns
 * 		The file name.
 */
char const *_dir_get_file_name( dir_t * );


#endif /* INCLUDE_FILESYSTEMS_DIR_H_ */
