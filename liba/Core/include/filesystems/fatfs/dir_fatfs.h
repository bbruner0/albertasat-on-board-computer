/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dir_fatfs.h
 * @author Brendan Bruner
 * @date Aug 17, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_DIR_FATFS_H_
#define INCLUDE_FILESYSTEMS_FATFS_DIR_FATFS_H_

#include "../dir.h"
#ifdef __LPC17XX__
#include "ff.h"
#else
#include "fat_sd/ff.h"
#endif

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dir_fatfs_t
 * @extends dir_t
 * @brief
 * 		Structure for working on fatfs directories.
 * @details
 * 		Structure for working on fatfs directories.
 * @var dir_fatfs_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 * @var dir_fatfs_t::_file_info_
 * 		<b>Private</b>
 * 		Fatfs structure containing information on files in the directory.
 * @var dir_fatfs_t::_native_dir_
 * 		<b>Private</b>
 * 		Fatfs structure containing information about the directory.
 */
typedef struct dir_fatfs_t dir_fatfs_t;

/* The nanomind API renamed the fatfs structure for directories... */
#ifdef NANOMIND
#define fat_dir_t FATDIR
#else
#define fat_dir_t DIR
#endif


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dir_fatfs_t
{
	dir_t 	_super_;

	FILINFO 	_file_info_;
	fat_dir_t	_native_dir_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dir_fatfs_t
 * @protected
 * @brief
 * 		Initializer for dir_fatfs_t structure.
 * @details
 * 		Initializer for dir_fatfs_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: On successful initialization.
 * 		<br><b>FS_HANDLE_FAILURE</b>: On failure to initialize. Structure is not
 * 		safe to use.
 */
uint8_t initialize_dir_fatfs( dir_fatfs_t *, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_DIR_FATFS_H_ */
