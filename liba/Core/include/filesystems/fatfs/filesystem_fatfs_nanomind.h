/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_fatfs_lpc.h
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_NANOMIND_H_
#define INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_NANOMIND_H_

#include "filesystem_fatfs.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct filesystem_fatfs_nanomind_t
 * @extends filesystem_fatfs_t
 * @brief
 * 		Enables the use of the nanomind's SD card through the fatfs filesystem.
 * @details
 * 		Enables the use of the nanomind's SD card through the fatfs filesystem.
 *
 * 		<b>Example Usage</b>:
 * 		@code
 * 			#include <filesystems/fatfs/filesystem_fatfs_lpc.h>
 *
 * 			int main( int argc, char *argv[] )
 * 			{
 * 				filesystem_fatfs_lpc_t 	lpc_filesys;
 * 				filesystem_t 			*filesys;
 * 				file_t					*file;
 * 				uint8_t 				init_err;
 * 				fs_error_t				fs_err;
 * 				uint32_t				byte_count;
 *
 * 				// Initialize the filesystem.
 * 				init_err = initialize_filesystem_fatfs_lpc( &lpc_filesys );
 * 				if( init_err != FILESYSTEM_SUCCESS )
 * 				{
 * 					// throw error
 * 					return 0;
 * 				}
 *
 * 				// Assign to pointer for easier use.
 * 				filesys = (filesystem_t *) &lpc_filesys;
 *
 * 				// Open a file.
 * 				file = filesys->open( filesys, &fs_err, "hello world.txt", FS_CREATE_ALWAYS, USE_POLLING );
 * 				if( fs_err != FS_OK )
 * 				{
 * 					// throw error
 * 					filesys->destroy( filesys );
 * 					return 0;
 * 				}
 *
 * 				// Write to the file
 * 				fs_err = file->write( file, "hello world!", strlen( "hello world!" ), &byte_count );
 * 				if( fs_err != FS_OK )
 * 				{
 * 					// throw error
 * 					filesys->close( filesys, file );
 * 					filesys->destroy( filesys );
 * 					return 0;
 * 				}
 * 				if( byte_count != strlen( "hello world!" ) )
 * 				{
 * 					// out of memory, throw error
 * 					filesys->close( filesys, file );
 * 					filesys->destroy( filesys );
 * 					return 0;
 * 				}
 *
 * 				// Write successful, close file and destroy filesystem.
 * 				filesys->close( filesys, file );
 * 				filesys->destroy( filesys );
 *
 * 				// Done.
 * 				return 1;
 * 			}
 * 		@endcode
 * @attention
 * 		<b>Public</b>
 * 		<br><b>Overrode</b>
 * 		@code
 * 			fs_error_t filesystem_t::close_dir( filesystem_t *fs, dir_t *dir );
 * 		@endcode
 * 		The nanomind does not implement the fatfs functions to close directories. Therefore, once
 * 		a directory is opened, it cannot be closed. This function will always return FS_UNABLE.
 * 		<br><br><b>Parameters</b>
 * 		<ul>
 * 		<li><b>dir</b>: A directory to close (which will never actually get closed).</li>
 * 		</ul>
 * 		<br><b>Returns</b>
 * 		<br><b>FS_UNABLE</b>: Always, the nanomind is incapable of closing directories.
 * @var filesystem_fatfs_nanomind_t::_super_
 * 		<b>Private</b>
 * 		Super structure data.
 */
typedef struct filesystem_fatfs_nanomind_t filesystem_fatfs_nanomind_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct filesystem_fatfs_nanomind_t
{
	filesystem_fatfs_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof filesystem_fatfs_nanomind_t
 * @brief
 * 		Initialize a filesystem_fatfs_nanomind_t structure.
 * @details
 * 		Initialize a filesystem_fatfs_nanomind_t structure.
 * @returns
 * 		<b>FILESYSTEM_SUCCESS</b>: On success
 * 		<br><b>FILESYSTEM_FAILURE</b>: On failure. The structure is not safe to use.
 */
uint8_t initialize_filesystem_fatfs_nanomind( filesystem_fatfs_nanomind_t * );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_NANOMIND_H_ */
