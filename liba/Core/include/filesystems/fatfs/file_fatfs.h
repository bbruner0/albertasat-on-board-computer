/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file file_fatfs_t.h
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_FILE_FATFS_H_
#define INCLUDE_FILESYSTEMS_FATFS_FILE_FATFS_H_

#include "../file.h"
#include "../fs_common.h"
#ifdef __LPC17XX__
#include <ff.h>
#else
#include <fat_sd/ff.h>
#endif

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct file_fatfs_t
 * @extends file_t
 * @brief
 *		File handle for use with the fatfs file system.
 * @details
 * 		File handle for use with the fatfs file system.
 * @var file_fatfs_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 * @var file_fatfs_t::_native_handle_
 * 		<b>Private</b>
 * 		Data for the native fatfs file handle.
 */
typedef struct file_fatfs_t file_fatfs_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct file_fatfs_t
{
	file_t	_super_; /* MUST be first struct variable. */

	FIL 	_native_handle_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof file_fatfs_t
 * @protected
 * @brief
 * 		Initializer for file_fatfs_t structure.
 * @details
 * 		Initializer for file_fatfs_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: On successful initialization.
 * 		<br><b>FS_HANDLE_FAILURE</b>: On failure to initialize. Structure is not
 * 		safe to use.
 */
uint8_t initialize_file_fatfs( file_fatfs_t *, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_FILE_FATFS_H_ */
