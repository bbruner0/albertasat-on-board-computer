/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file states
 * @author Brendan Bruner
 * @date 
 *
 * Defines the common structure used  by states and prototypes the
 * methods to initialize this structure depending on the state.
 *
 * All state ids are #defined here.
 */
#ifndef INCLUDE_stateS_H_
#define INCLUDE_stateS_H_

#include <filesystems/filesystem.h>
#include <filesystems/fs_handle.h>
#include "state_config.h"
#include "state_relay_common.h"

#include <telecommands/telecommand.h>

#ifndef NULL
#define NULL ((void *) 0)
#endif


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define STATE_NULL					((uint8_t) 0)
#define STATE_BRING_UP_ID 			((uint8_t) 1)
#define STATE_LOW_POWER_ID			((uint8_t) 2)
#define STATE_ALIGNMENT_ID			((uint8_t) 3)
#define STATE_SCIENCE_ID			((uint8_t) 4)
#define STATE_BLACK_OUT_ID			((uint8_t) 5)
#define STATE_DETUMBLE_ID			((uint8_t) 6)
#define STATE_LEOP_POWER_CHARGE_ID	((uint8_t) 7)
#define STATE_BOOM_DEPLOYMENT_ID 	((uint8_t) 8)
#define STATE_ANTENNA_DEPLOYMENT_ID	((uint8_t) 9)
#define STATE_LEOP_CHECK_ID			((uint8_t) 10)

#define STATE_SUCCESS 1
#define STATE_FAILURE 0
#define STATE_DEFAULTED 2

extern char* state_to_name_map[];


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct state_t
 * @brief
 * 		The abstract structure of a system state.
 *
 * @details
 * 		The abstract structure of a system state.
 *
 * @var state_t::_id_
 * 		<b>Private</b>
 * 		A unique integer number which represents the state. This
 * 		is useful for querying the current state of the system
 * 		or for logging state changes.
 *
 * @var state_t::_fs_
 * 		<b>Private</b>
 * 		Defines what filesystem is used when altering configuration files.
 *
 * @var state_t::_config_name_
 * 		<b>Private</b>
 * 		Defines the name of the file in non volatile memory that contains the
 * 		states configuration.
 *
 * @var state_t::_config_
 * 		<b>Private</b>
 * 		A structure kept in ram that defines the states active configuration. It
 * 		has the same contents as the file kept in non volatile memory. A ram copy
 * 		is used to speed up access times to the states configuration.
 *
 * @var state_t::_default_config_
 * 		<b>Private</b>
 * 		Contains the hard coded default configuration of the state.
 *
 * @var state_t::_config_mutex_
 * 		<b>Private</b>
 * 		Provides mutual exclusion to several private variables.
 *
 * @var state_t::enter_state
 * 		<b>Virtual<br>Public</b>
 * 			@code
 * 			void enter_state( state_t *self, driver_toolkit_t *kit );
 * 			@endcode
 * 		The method is used to define behaviour before a state is entered. For example,
 * 		this method could be used to initialize peripheral devices.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>kit[in]</b>: Used to access APIs like eps_t.</li>
 * 		</ul>
 *
 * @var state_t::exit_state
 * 		<b>Virtual<br>Public</b>
 * 			@code
 * 			void exit_state( state_t *self, driver_toolkit_t *kit );
 * 			@endcode
 * 		The method is used to define behaviour when a state is being exited. For
 * 		example, this method could be used to tear down peripheral devices.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>kit[in]</b>: Used to access APIs like eps_t.</li>
 * 		</ul>
 *
 * @var state_t::next_state
 * 		<b>Virtual<br>Public</b>
 * 			@code
 * 			state_t *next_state( 	state_t *self,
 * 												state_relay_t *relay );
 * 			@endcode
 * 		This method defines next state logic. It
 * 		uses the state_relay_t structure passed in to find the
 * 		pointer to the next state.
 *
 *		<b>Returns</b>
 *		<br>A pointer to the next state. The method will return NULL
 *		if the system does not need to transition states.
 *
 * @var state_t::destroy
 * 		<b>Virtual<br>Public</b>
 * 			@code
 * 			void destroy( state_t *self );
 * 			@endcode
 *	 	Destroys a state_t structure when it is no longer being used.
 */
typedef struct state_t state_t;


/********************************************************************************/
/* Structure Declares															*/
/********************************************************************************/
struct state_t
{
	uint8_t			 	_id_;				/* Numeric ID of state. */

	filesystem_t		*_fs_; 				/* fs for loading / setting configuration. */
	char const			*_config_name_; 	/* Name of configuration file for the state. */

	state_config_t	_config_;		/* Active configuration used for the state. */
	state_config_t	_default_config_;/* Default configuration of state. */

	void (*enter_state)( state_t *, driver_toolkit_t * );
	void (*exit_state)( state_t *, driver_toolkit_t * );
	state_t * (*next_state)( state_t *, state_relay_t * );

	mutex_t* _mutex_;

	void (*destroy)( state_t * );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_t
 * @brief
 * 		Initialize a state_t structure.
 * @details
 * 		Initialize a state_t structure.
 * @param state[in]
 * 		A pointer to the state_t structure to initialize.
 * @param log[in]
 * 		The name of the log file the state will use when loading and changing
 * 		its configuration.
 * @param fs[in]
 * 		A pointer to an initialized filesystem_t structure. This is the file system
 * 		which will be used when loading and setting the states configuration file.
 * @param config[in]
 * 		This will be the hard coded initial configuration of the state.
 * @returns
 * 		<b>STATE_SUCCESS</b>: State initialized correctly.
 * 		<br><b>STATE_DEFAULTED</b>: Error loading configuration file from non volatile
 * 		memory or no configuration file exists. The hard coded default <b>config</b> is being used.
 * 		<br><b>STATE_FAILURE</b>: Fatal failure initializing state. The structure is
 * 		not safe to use.
 */
uint8_t initialize_state( state_t *state, char const *log, filesystem_t *fs, state_config_t *config );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof state_t
 * @brief
 * 		Attempts to execute a housekeeping related command.
 * @details
 * 		Attempts to execute a housekeeping related command. The
 * 		command will only be executed if the states configuration enables
 * 		housekeeping commands to be executed. Note, the function does
 * 		not actively check that the command is strictly a housekeeping
 * 		command. In fact, it could be a destroy satellite command and
 * 		this function will happily treat it as a housekeeping command.
 * @param command[in]
 * 		The housekeeping command who's execution is being requested.
 */
 void state_execution_hk( state_t *state, telecommand_t *command );

 /**
  * @memberof state_t
  * @brief
  * 		Attempts to execute a transmission related command.
  * @details
  * 		Attempts to execute a transmission related command. The
  * 		command will only be executed if the states configuration enables
  * 		transmission commands to be executed. Note, the function does
  * 		not actively check that the command is strictly a transmission
  * 		command. In fact, it could be a destroy satellite command and
  * 		this function will happily treat it as a transmission command.
  * @param command[in]
  * 		The transmission command who's execution is being requested.
  */
 void state_execution_transmit( state_t *state, telecommand_t *command );

 /**
  * @memberof state_t
  * @brief
  * 		Attempts to execute a response related command.
  * @details
  * 		Attempts to execute a response related command. The
  * 		command will only be executed if the states configuration enables
  * 		response commands to be executed. Note, the function does
  * 		not actively check that the command is strictly a response
  * 		command. In fact, it could be a destroy satellite command and
  * 		this function will happily treat it as a response command.
  * @param command[in]
  * 		The reponse command who's execution is being requested.
  */
 void state_execution_response( state_t *state, telecommand_t *command );

 /**
  * @memberof state_t
  * @brief
  * 		Attempts to execute an mnlp related command.
  * @details
  * 		Attempts to execute an mnlp related command. The
  * 		command will only be executed if the states configuration enables
  * 		mnlp commands to be executed. Note, the function does
  * 		not actively check that the command is strictly an mnlp
  * 		command. In fact, it could be a destroy satellite command and
  * 		this function will happily treat it as a mnlp command.
  * @param command[in]
  * 		The mnlp command who's execution is being requested.
  */
 void state_execution_mnlp( state_t *state, telecommand_t *command );

 /**
  * @memberof state_t
  * @brief
  * 		Attempts to execute a dfgm related command.
  * @details
  * 		Attempts to execute a dfgm related command. The
  * 		command will only be executed if the states configuration enables
  * 		dfgm commands to be executed. Note, the function does
  * 		not actively check that the command is strictly a dfgm
  * 		command. In fact, it could be a destroy satellite command and
  * 		this function will happily treat it as a dfgm command.
  * @param command[in]
  * 		The dfgm command who's execution is being requested.
  */
 void state_execution_dfgm( state_t *state, telecommand_t *command );

 /**
  * @memberof state_t
  * @brief
  * 		Executes a diagnostics related command.
  * @details
  * 		executes a diagnostics related command. The
  * 		command will always be executed. Note, the function does
  * 		not actively check that the command is strictly a diagnostics
  * 		command. In fact, it could be a destroy satellite command and
  * 		this function will happily treat it as a diagnostics command.
  * @param command[in]
  * 		The diagnostics command who's execution is being requested.
  */
 void state_execution_diagnostics( state_t *state, telecommand_t *command );

/**
 * @memberof state_t
 * @brief
 * 		Set the priority configuration for the state.
 * @details
 * 		Changes the priority configuration for the state. For example, this function
 * 		can be used to stop a state from executing housekeeping commands. The changes
 * 		are logged in non volatile memory and are therefore persistant through
 * 		power cycles.
 * @param state[in]
 * 		A pointer to the state_t structure to change the configuration of.
 * @param config[in]
 * 		A pointer to the state_config_t structure containing the new
 * 		configuration.
 * @returns
 * 		An error code:
 * 		<br><b>STATE_SUCCESS</b>: On success.
 * 		<br><b>STATE_FAILURE</b>: On failure. The new configuration was not
 * 		loaded into non volatile memory due to a file system error and therefore
 * 		the changes were not set.
 */
uint8_t state_set_config( state_t *state, state_config_t *config );

/**
 * @memberof state_t
 * @brief
 * 		Get the priority configuration for the state.
 * @details
 * 		Gets the priority configuration for the state. For example, this function
 * 		can be used to load the configuration of a state, change one parameter,
 * 		then set the new configuration.
 * @param state[in]
 * 		A pointer to the state_t structure to get the configuration of.
 * @param config[out]
 * 		A pointer to the state_config_t structure containing the state's
 * 		configuration.
 */
void state_get_config( state_t *state, state_config_t *config );

/**
 * @memberof state_t
 * @brief
 * 		Reset a states configuration to default.
 * @details
 * 		Reset a states configuration to default.
 * @param state[in]
 * 		A pointer to the state_t structure to change the configuration of.
 * @returns
 * 		An error code:
 * 		<br><b>STATE_SUCCESS</b>: On success.
 * 		<br><b>STATE_FAILURE</b>: On failure. The default configuration
 * 		was not set.
 */
uint8_t state_reset_config( state_t *state );

/**
 * @brief
 * 		Gets the id of a state.
 * @details
 * 		Gets the id of a state.	For example,
 * @param state
 * 		The state_t object to get the id of.
 * @returns
 * 		The id of the state.
 * @memberof state_t
 */
uint8_t query_state_id( state_t *state );

/**
 * @memberof state_t
 * @brief
 * 		Lock out all updates to state configuration files.
 * @details
 * 		Lock out all updates to state configuration files. This means that any call
 * 		to a function which updates the state's configuration will be blocked. The block will
 * 		last until a five minutes timeout is reached, or a call to state_unlock_config_globally( )
 * 		is made.
 * @param block_time
 * 		The time to block for to acquire the lock.
 * @returns
 * 		<b>MUTEX_ACQUIRED</b>: If successful.
 * 		<br><b>MUTEX_BUSY</b>: If the lock was not acquired.
 *
 */
base_t state_lock_config_globally( block_time_t bock_time );

/**
 * @memberof state_t
 * @brief
 * 		Unlock the configuration lock.
 * @details
 * 		Unlock the configuration lock. This is the inverse operation to
 * 		state_lock_config_globally( ).
 */
void state_unlock_config_globally( );

/**
 * memberof state_t
 * @brief
 * 		Refreshes the states meta data and operational configuration.
 * @details
 * 		Refreshes the cached config file with whats in non volatile memory.
 * 		Then, turns off/on all peripherals which are disabled/enabled.
 */
void state_refresh( state_t *, driver_toolkit_t * );

/**
 * @memberof state_t
 * @brief
 * 		Method to call when going into a state.
 * @details
 * 		Runs entry routines required by all states then calls the state_t::enter_state
 * 		hook.
 * @param kit
 * 		The driver toolkit being used.
 */
void state_enter( state_t *, driver_toolkit_t *kit );

/**
 * @memberof state_t
 * @brief
 * 		Method to call when leaving a state.
 * @details
 * 		Runs exit routines required by all states then calls the state_t::exit_state
 * 		hook.
 * @param kit
 * 		The driver toolkit being used.
 */
void state_exit( state_t *, driver_toolkit_t *kit );

#endif /* INCLUDE_stateS_H_ */
