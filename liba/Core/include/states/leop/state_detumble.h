/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_detumble.h
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#ifndef INCLUDE_STATES_LEOP_STATE_DETUMBLE_H_
#define INCLUDE_STATES_LEOP_STATE_DETUMBLE_H_

#include <states/state.h>
#include <ptimer/ptimer.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_detumble_t
 * @brief
 * 		Actively detumbles satellite whilst in leop.
 * @details
 * 		Actively detumbles satellite whilst in leop.
 * @var state_detumble_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct state_detumble_t state_detumble_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_detumble_t
{
	state_t _super_;
	struct
	{
		ptimer_t timer; /* Used to implement a detumble time out. */ // Not required
		void (*supers_destroy)( state_t* ); /* Maintain reference to supers destructor. */
	} priv_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_detumble_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file.
 * @param time_out
 * 		The time out in seconds that detumbling should be done for before giving up.
 * @param log_file
 * 		Name of the log file to track time between power cycles.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_detumble( state_detumble_t *state, filesystem_t *fs, uint32_t time_out, char const* log_file );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_STATES_LEOP_STATE_DETUMBLE_H_ */
