/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file system_state_config.h
 * @author Brendan Bruner
 * @date Aug 10, 2015
 */
#ifndef INCLUDE_SYSTEM_STATE_CONFIG_H_
#define INCLUDE_SYSTEM_STATE_CONFIG_H_

#include <stdint.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define STATE_DISABLES_EXECUTION 	0
#define STATE_ENABLES_EXECUTION		1

#define STATE_CONFIG_OK			1
#define STATE_CONFIG_CORRUPT	0


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct state_config_t
 * @brief
 * 		Contains the configuration meta data of a system_state_t structure. This
 * 		meta data determines what types of commands the state executes.
 * @details
 * 		Contains the configuration meta data of a system_state_t structure. This
 * 		meta data determines what types of commands the state executes.
 *
 * 		For example, the code below disables the use of dfgm commands. In
 * 		other words, if the state allowed dfgm commands to executed they become
 * 		disabled. Otherwise, nothign happens.
 * 		Note, the variable state is a pointer to a system_state_t
 * 		structure defined outside this code snipit.
 * 		@code
 * 			state_config_t state_config;
 * 			uint8_t err;
 *
 * 			err = system_state_get_config( state, &state_config );
 * 			if( err == STATE_SUCCESS )
 * 			{
 * 				state_config_set_dfgm( &state_config, STATE_DISABLES_EXECUTION );
 * 				err = system_state_set_config( state, &state_config );
 * 				if( err == STATE_SUCCESS )
 * 				{
 * 					printf( "The state no longer allows dfgm commands to be executed!" );
 * 				}
 * 			}
 * 		@endcode
 */
typedef struct state_config_t state_config_t;


/********************************************************************************/
/* Structure Declares															*/
/********************************************************************************/
struct state_config_t
{
		uint8_t	_uses_hk, /* determines if this type of command is executed or not. */
				_uses_transmit,
				_uses_response,
				_uses_mnlp,
				_uses_dfgm;
};


/********************************************************************************/
/* Constructor / Destructor	Declare												*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof state_config_t
 * @brief
 * 		Set the configuration of housekeeping commands.
 * @details
 * 		Set the configuration of housekeeping commands.
 * @param config[in]
 * 		A pointer to the state_config_t structure to set housekeping
 * 		command configuration of.
 * @param option[in]
 * 		The configuration option for housekeeping commands. Must be one of:
 * 		<br><b>STATE_ENABLES_EXECUTION</b>: Housekeeping commands can be executed.
 * 		<br><b>STATE_DISABLES_EXECUTION</b>: Housekeeping commands cannot be executed.
 */
void state_config_set_hk( state_config_t *config, uint8_t option );

/**
 * @memberof state_config_t
 * @brief
 * 		Set the configuration of transmit commands.
 * @details
 * 		Set the configuration of transmit commands.
 * @param config[in]
 * 		A pointer to the state_config_t structure to set transmit
 * 		command configuration of.
 * @param option[in]
 * 		The configuration option for transmit commands. Must be one of:
 * 		<br><b>STATE_ENABLES_EXECUTION</b>: Transmit commands can be executed.
 * 		<br><b>STATE_DISABLES_EXECUTION</b>: Transmit commands cannot be executed.
 */
void state_config_set_transmit( state_config_t *config, uint8_t option );

/**
 * @memberof state_config_t
 * @brief
 * 		Set the configuration of response commands.
 * @details
 * 		Set the configuration of response commands.
 * @param config[in]
 * 		A pointer to the state_config_t structure to set response
 * 		command configuration of.
 * @param option[in]
 * 		The configuration option for response commands. Must be one of:
 * 		<br><b>STATE_ENABLES_EXECUTION</b>: response commands can be executed.
 * 		<br><b>STATE_DISABLES_EXECUTION</b>: response commands cannot be executed.
 */
void state_config_set_response( state_config_t *config, uint8_t option );

/**
 * @memberof state_config_t
 * @brief
 * 		Set the configuration of dfgm commands.
 * @details
 * 		Set the configuration of dfgm commands.
 * @param config[in]
 * 		A pointer to the state_config_t structure to set dfgm
 * 		command configuration of.
 * @param option[in]
 * 		The configuration option for dfgm commands. Must be one of:
 * 		<br><b>STATE_ENABLES_EXECUTION</b>: dfgm commands can be executed.
 * 		<br><b>STATE_DISABLES_EXECUTION</b>: dfgm commands cannot be executed.
 */
void state_config_set_dfgm( state_config_t *config, uint8_t option );

/**
 * @memberof state_config_t
 * @brief
 * 		Set the configuration of mnlp commands.
 * @details
 * 		Set the configuration of mnlp commands.
 * @param config[in]
 * 		A pointer to the state_config_t structure to set mnlp
 * 		command configuration of.
 * @param option[in]
 * 		The configuration option for mnlp commands. Must be one of:
 * 		<br><b>STATE_ENABLES_EXECUTION</b>: mnlp commands can be executed.
 * 		<br><b>STATE_DISABLES_EXECUTION</b>: mnlp commands cannot be executed.
 */
void state_config_set_mnlp( state_config_t *config, uint8_t option );

/**
 * @memberof state_config_t
 * @brief
 * 		Checks for corruption in the configuration structure.
 * @details
 * 		Checks for corruption in the configuration structure.
 * @param config[in]
 * 		The structure to check for corruption.
 * @returns
 * 		<b>STATE_CONFIG_OK</b>: If not corrupt.
 * 		<br><b>STATE_CONFIG_CORRUPT</b>: If corrupt.
 */
uint8_t state_config_detect_corruption( state_config_t *config );

#endif /* INCLUDE_SYSTEM_STATE_CONFIG_H_ */
