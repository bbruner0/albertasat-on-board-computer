/*
 * nanomind_mag.h
 *
 *  Created on: Jun 23, 2016
 *      Author: bbruner
 */

#ifndef LIB_LIBCORE_INCLUDE_DFGM_NANOMIND_MAG_H_
#define LIB_LIBCORE_INCLUDE_DFGM_NANOMIND_MAG_H_

#include <portable_types.h>

bool_t initialize_nanomind_mag( );
bool_t enable_nanomind_mag( );
bool_t disable_nanomind_mag( );

#endif /* LIB_LIBCORE_INCLUDE_DFGM_NANOMIND_MAG_H_ */
