/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_hk.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_EPS_EPS_HK_H_
#define INCLUDE_EPS_EPS_HK_H_

/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
/**
 * @struct chkparam_t
 * @brief
 * 		A mock up of possible housekeeping in Gomspace's eps.
 * @details
 * 		A mock up of possible housekeeping in Gomspace's eps.
 */
typedef struct __attribute__ ((__packed__)) {
	uint16_t vboost[3];								//! Voltage of boost converters [mV] [PV1, PV2, PV3]
	uint16_t vbatt;									//! Voltage of battery [mV]
	uint16_t curin[3];								//! Current in [mA]
	uint16_t cursun;								//! Current from boost converters
	uint16_t cursys;								//! Current out of battery
	uint16_t reserved;
	uint16_t curout[6];								//! Current out [mA]
	uint8_t output[8];								//! Status of outputs
	uint16_t output_on_delta[8];					//! Time till power on
	uint16_t output_off_delta[8];					//! Time till power off
	uint16_t latchup[6];							//! Number of latch-ups
	uint32_t wdt_i2c_time_left;						//! Time left on I2C wdt
	uint32_t wdt_gnd_time_left;						//! Time left on I2C wdt
	uint8_t  wdt_csp_pings_left[2];					//! Pings left on CSP wdt
	uint32_t counter_wdt_i2c;						//! Number of WDT I2C reboots
	uint32_t counter_wdt_gnd;						//! Number of WDT GND reboots
	uint32_t counter_wdt_csp[2];					//! Number of WDT CSP reboots
	uint32_t counter_boot; 							//! Number of EPS reboots
	int16_t temp[6];								//! Temperature sensors [0 = TEMP1, TEMP2, TEMP3, TEMP4, BATT0, BATT1]
	uint8_t	bootcause;								//! Cause of last EPS reset
	uint8_t battmode;								//! Mode for battery [0 = normal, 1 = undervoltage, 2 = overvoltage]
	uint8_t pptmode;								//! Mode of PPT tracker
	uint16_t reserved2;
} eps_hk_t;


#endif /* INCLUDE_EPS_EPS_HK_H_ */
