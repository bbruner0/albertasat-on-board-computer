/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_nanomind.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_EPS_EPS_NANOMIND_H_
#define INCLUDE_EPS_EPS_NANOMIND_H_

#include "eps.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct eps_nanomind_t
 * @extends eps_t
 * @brief
 * 		Provides a portable API into Gomspace's nanopower board.
 * @details
 * 		Provides a portable API into Gomspace's nanopower board by implementing the
 * 		interface of its abstract class, eps_t.
 * 		<br>
 * 		<b>Overrides</b>: eps_t::power_line( )
 * 		<b>Overrides</b>: eps_t::hk_refresh( )
 */
typedef struct eps_nanomind_t eps_nanomind_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct eps_nanomind_t
{
	eps_t s_; /* MUST be first. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof eps_nanomind_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 */
void initialize_eps_nanomind( eps_nanomind_t* );

/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_EPS_EPS_NANOMIND_H_ */
