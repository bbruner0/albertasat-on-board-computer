/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mnlp_dm.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_mnlp_mnlp_EXTERNAL_H_
#define INCLUDE_mnlp_mnlp_EXTERNAL_H_

#include <mnlp/mnlp.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct mnlp_external_t
 * @extends mnlp_t
 * @brief
 * 		Interface to physical mnlp of satellite.
 * @details
 * 		Interface to physical mnlp of satellite.
 */
typedef struct mnlp_external_t mnlp_external_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct mnlp_external_t
{
	mnlp_t s_; /* MUST be first. */
	struct
	{
		bool_t (*supers_power)( mnlp_t*, hub_t*, bool_t );
		bool_t (*supers_start_stop)(mnlp_t*, bool_t);
	}_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof mnlp_external_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_mnlp_external( mnlp_external_t* );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_mnlp_mnlp_EXTERNAL_H_ */
