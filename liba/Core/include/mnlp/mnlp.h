/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mnlp.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_mnlp_mnlp_H_
#define INCLUDE_mnlp_mnlp_H_

#include <eps/eps.h>
#include <hub/hub.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct mnlp_t
 * @brief
 * 		Abstract class defining interface to the mnlp.
 * @details
 * 		Abstract class defining interface to the mnlp.
 */
typedef struct mnlp_t mnlp_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct mnlp_t
{
	bool_t (*power)( mnlp_t*, hub_t*, bool_t );
	bool_t (*start_stop)( mnlp_t*, bool_t );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_mnlp_mnlp_H_ */
