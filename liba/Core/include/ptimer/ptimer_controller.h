/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ptimer_controller.h
 * @author Brendan Bruner
 * @date Oct 1, 2015
 */
#ifndef INCLUDE_ptimer_ptimer_CONTROLLER_H_
#define INCLUDE_ptimer_ptimer_CONTROLLER_H_

#include <portable_types.h>
#include <ptimer/ptimer.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define PTIMER_CONTROLLER_MAX_OBSERVERS 8


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/\
/**
 * @struct ptimer_controller_t
 * @brief
 * 		Singleton controller for ptimer_t instances.
 * @details
 * 		Singleton controller for ptimer_t instances. This class has an observer-pattern
 * 		relationship with the ptimer_t class. Where, the controller acts as the
 * 		model and the ptimer_t as the observer. The pattern is modified to make the
 * 		model an active class which autonomously calls the classic "notify( )" method.
 * @attention
 * 		This class is a singleton. There should only ever be one instance of it. That
 * 		instance is retrieved via initialize_ptimer_controller( ).
 */
typedef struct ptimer_controller_t ptimer_controller_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
typedef struct {
	ptimer_t*	ptimer;
	uint32_t	time_elapsed;
} ptimer_observer_block_t;

struct ptimer_controller_t
{
	struct
	{
		task_t controller_task;
		ptimer_observer_block_t observers_stack[PTIMER_CONTROLLER_MAX_OBSERVERS];
		uint32_t stack_size;
		mutex_t register_mutex;
	} _; /* private */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof ptimer_controller_t
 * @brief
 * 		Get the instance of the ptimer controller.
 * @details
 * 		Get the instance of the ptimer controller.
 * @returns
 * 		The only ptimer_controller instance. NULL if that single instance has a
 * 		failure in it.
 */
ptimer_controller_t *initialize_ptimer_controller( );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof ptimer_controller_t
 * @brief
 * 		Register a timer with the controller.
 * @details
 * 		Register a timer with the controller. This is analogous to the classic
 * 		"observer( )" method found in the observer-pattern.
 * @attention
 * 		This method should not be called by application code.
 * @param observer[in]
 * 		The ptimer which will observer the controller.
 * @returns
 * 		<b>true</b> on successful registration, <b>false</b> otherwise.
 */
bool_t ptimer_controller_register( ptimer_controller_t*, ptimer_t* observer );

/**
 * @memberof ptimer_controller_t
 * @brief
 * 		Remove a ptimer from its registration with the controller.
 * @details
 * 		Remove a ptimer from its registration with the controller. The controller
 * 		will no longer call its update method.
 * @attention
 * 		This method should not be called by application code.
 * @param observer[in]
 * 		The ptimer to remove.
 */
void ptimer_controller_deregister( ptimer_controller_t*, ptimer_t* observer );

#endif /* INCLUDE_ptimer_ptimer_CONTROLLER_H_ */
