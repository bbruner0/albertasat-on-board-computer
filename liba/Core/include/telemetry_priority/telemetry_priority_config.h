/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telemetry_priority_config.h
 * @author Brendan Bruner
 * @date Aug 24, 2015
 */
#ifndef INCLUDE_TELEMETRY_PRIORITY_TELEMETRY_PRIORITY_CONFIG_H_
#define INCLUDE_TELEMETRY_PRIORITY_TELEMETRY_PRIORITY_CONFIG_H_


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct telemetry_priority_config_t
 * @brief
 * 		Contains the meta data on telemetry priorities.
 * @details
 * 		Contains the meta data on telemetry priorities.
 */
typedef struct telemetry_priority_config_t telemetry_priority_config_t;

/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
/* All fields must be uint8_t, struct must be packed. If ether is changed,
 * the algorithm in the function telemetry_priority_refresh_configuration( )
 * must be changed.
 */
struct telemetry_priority_config_t
{
	/* **DO NOT** reorder. */
	uint8_t _wod_priority;
	uint8_t _dfgm_priority;
	uint8_t _mnlp_priority;
	uint8_t _cmnd_status_priority;
	uint8_t _state_priority;
} __attribute__((packed));


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_TELEMETRY_PRIORITY_TELEMETRY_PRIORITY_CONFIG_H_ */
