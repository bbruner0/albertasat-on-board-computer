/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file file_variable.h
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */
#ifndef INCLUDE_NON_VOLATILE_FILE_VARIABLE_H_
#define INCLUDE_NON_VOLATILE_FILE_VARIABLE_H_

#include "non_volatile_variable.h"
#include <filesystems/filesystem.h>
#include <portable_types.h>
#include <stdint.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FILE_VARIABLE_MAX_NAME_LENGTH FILESYSTEM_MAX_NAME_LENGTH


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct file_variable_t
 * @extends non_volatile_variable_t
 * @brief
 * 		Implements non_volatile_variable_t interface using a file.
 * @details
 * 		Implements non_volatile_variable_t interface using a file.
 * 		Variables are initialized to zero on first initialization.
 */
typedef struct file_variable_t file_variable_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct file_variable_t
{
	non_volatile_variable_t s_; /* MUST be first. Super class. */
	struct
	{
		char variable_name[FILE_VARIABLE_MAX_NAME_LENGTH+1];
		filesystem_t* fs;
		uint32_t size;
		mutex_t* mutex;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof file_variable_t
 * @brief
 * 		Constructor
 * @details
 * 		Constructor
 * @param fs[in]
 * 		Since this implementation uses a file, it needs a filesystem to use.
 * @param size
 * 		The size, in bytes, of the variable which is being kept in non volatile memory.
 * @param name[in]
 * 		Copied. This is the name of the non volatile variable. Since a file is used,
 * 		this will also be the name of the file. It must be null terminated and
 * 		less than <b>FILE_VARIABLE_MAX_NAME_LENGTH</b> bytes long. Longer
 * 		variable names are silently truncated.
 * @returns
 * 		<b>true</b> on success, <b>false</b> on failure. Not safe to use if <b>false</b> is
 * 		returned.
 */
bool_t initialize_file_variable
(
	file_variable_t*,
	filesystem_t* fs,
	char const* name,
	uint32_t size
);


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
static inline bool_t file_variable_set( file_variable_t* self, void* value )
{
	return ((non_volatile_variable_t*) self)->set((non_volatile_variable_t*) self, value);
}

static inline bool_t file_variable_get( file_variable_t* self, void* value )
{
	return ((non_volatile_variable_t*) self)->get((non_volatile_variable_t*) self, value);
}



#endif /* INCLUDE_NON_VOLATILE_FILE_VARIABLE_H_ */
