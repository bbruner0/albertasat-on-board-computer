/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file gpio.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_GPIO_GPIO_H_
#define INCLUDE_GPIO_GPIO_H_


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct gpio_t
 * @brief
 * 		Abstract class defining interface to gpio's
 * @details
 * 		Abstract class defining interface to gpio's
 * @var gpio_t::set_state
 * 		@code
 * 			void set_state( gpio_t* self, gpio_state_t state );
 * 		@endcode
 * 		Set the logic level of a gpio pin. If the pin's direction
 * 		is input, this method has no effect.
 * @var gpio_t::set_direction
 * 		@code
 * 			void set_direction( gpio_t* self, gpio_direction_t direction );
 * 		@endcode
 * 		Set the pin as an input or output.
 * @var gpio_t::get_state
 * 		@code
 * 			gpio_state_t get_state( gpio_t* self );
 * 		@endcode
 * 		Returns the logic level on the pin.
 */
typedef struct gpio_t gpio_t;

/** Possible logic levels of a GPIO. */
typedef enum
{
	GPIO_LOW = 0,			/*!< (0) Logic level 0. */
	GPIO_HIGH = !GPIO_LOW	/*!< (1) Logic levle 1. */
} gpio_state_t;

/** Possible pin directions of a GPIO. */
typedef enum
{
	GPIO_INPUT = 0,	/*!< (0) GPIO is input. */
	GPIO_OUTPUT		/*!< (1) GPIO is output. */
} gpio_direction_t;

/** Possible ways the hardware pin is wired up. */
typedef enum
{
	GPIO_PULLUP = 0,	/*!< (0) Internal pullup. */
	GPIO_PULLDOWN,		/*!< (1) Internal pulldown. */
	GPIO_BARE,			/*!< (2) Non extra wiring. */
	GPIO_REPEATER		/*!< (3) Automatic pulldown / pullup to retain last input given to pin. */
} gpio_wire_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct gpio_t
{
	void (*set_state)( gpio_t*, gpio_state_t );
	void (*set_direction)( gpio_t*, gpio_direction_t );
	gpio_state_t (*get_state)( gpio_t* );
	void (*destroy)( gpio_t* );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_GPIO_GPIO_H_ */
