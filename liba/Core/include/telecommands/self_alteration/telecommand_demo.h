/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_demo.h
 * @author Brendan Bruner
 * @date Oct 9, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_DEMO_H_
#define INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_DEMO_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define TELECOMMAND_DEMO_MSG_LENGTH 100


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_demo_t
 * @brief
 * 		A telecommand which will use the DEMO_PRINTF macro to print
 * 		messages.
 * @details
 * 		A telecommand which will use the DEMO_PRINTF macro to print
 * 		messages.
 * @var telecommand_demo_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_demo_t telecommand_demo_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_demo_t
{
	telecommand_t _super_;
	struct
	{
		char msg[TELECOMMAND_DEMO_MSG_LENGTH];
	}_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_demo_t
 * @brief
 * 		Initialize for telecommand_demo_t
 * @details
 * 		Initializer for telecommand_demo_t
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @param msg[in]
 * 		Null terminated string which is printed by the command. It is copied into the command.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
bool_t initialize_telecommand_demo( telecommand_demo_t *self, driver_toolkit_t *kit, char const* msg );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_SELF_ALTERATION_TELECOMMAND_DEMO_H_ */
