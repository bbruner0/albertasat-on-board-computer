/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Alex Hamilton, Oleg Oleynikov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file Detumble.c
 * @author Alex Hamilton
 * @author Collin Cupido
 * @author Brendan Bruner
 * @author Oleg Oleynikov
 * @date 2015-02-11
 */

#ifndef ADCS_H_
#define ADCS_H_

#include <stdint.h>
#include <portable_types.h>
#include <eps/power_lines.h>
#include <eps/eps.h>
//#include <dev/usart.h>

#define ADCS_I2C_ADDR 0x24
#define ADCS_I2C_HANDLE 0
#define ADCS_I2C_TIMEOUT 1000

#define ADCS_USART_HANDLE 0
#define ADCS_ESC 0x1F

#define ADCS_DEBUG 0

#define ADCS_PRINT_DELAY 1000

/*Ignores regular ADCS functions*/
//#define ADCS_LOCAL 0
/*Uses the pretend mission time below.*/
//#define ADCS_TIME_SPOOF 0
//fake mission start date 08/28/2015 (just to make date more real)
//#define MISSION_START_DATE 1440794129


/* Telecommand and telemetry ID's */
#define ADCS_RESET_ID 1
#define ADCS_SET_UNIX_TIME_ID 2
#define ADCS_ADCS_RUN_MODE_ID 3
#define ADCS_SELECTED_LOGGED_DATA_ID 4
#define ADCS_POWER_CONTROL_ID 5
#define ADCS_DEPLOY_MAGNETOMETER_BOOM_ID 6
#define ADCS_TRIGGER_ADCS_LOOP_ID 7
#define ADCS_SET_ATTITUDE_ESTIMATION_MODE_ID 17
#define ADCS_SET_ATTITUDE_CONTROL_MODE_ID 18
#define ADCS_SET_COMMANDED_ATTITUDE_ANGLES_ID 19
#define ADCS_SET_WHEEL_ID 32
#define ADCS_SET_MAGNETORQUER_OUTPUT_ID 33
#define ADCS_SET_SGP4_ORBIT_PARAMETERS_ID 64
#define ADCS_SET_CONFIGURATION_ID 80
#define ADCS_SET_MAGNETORQUER_ID 81
#define ADCS_SET_WHEEL_CONFIGURATION_ID 82
#define ADCS_SET_CSS_CONFIGURATION_ID 83
#define ADCS_SET_SUN_SENSOR_CONFIGURATION_ID 84
#define ADCS_SET_NADIR_SENSOR_CONFIGURATION_ID 85
#define ADCS_SET_MAGNETOMETER_CONFIGURATION_ID 86
#define ADCS_SET_RATE_SENSOR_CONFIGURATION_ID 87
#define ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_ID 88
#define ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_ID 89
#define ADCS_SET_MOMENT_OF_INERTIA_ID 90
#define ADCS_SET_ESTIMATION_PARAMETERS_ID 91
#define ADCS_SAVE_CONFIGURATION_ID 100
#define ADCS_SAVE_ORBIT_PARAMETERS_ID 101
#define ADCS_SET_START_UP_MODE_ID 102
#define ADCS_CAPTURE_AND_SAVE_IMAGE_ID 110
#define ADCS_RESET_FILE_LIST_ID 114
#define ADCS_ADVANCE_FILE_LIST_INDEX_ID 115
#define ADCS_INITIALIZE_FILE_DOWNLOAD_ID 116
#define ADCS_ADVANCE_FILE_READ_POINTER_ID 117
#define ADCS_ERASE_FILE_ID 118
#define ADCS_ERASE_ALL_FILES_ID 119
#define ADCS_SET_BOOT_INDEX_ID 120
#define ADCS_ERASE_PROGRAM_ID 121
#define ADCS_UPLOAD_PROGRAM_BLOCK_ID 122
#define ADCS_FINALIZE_PROGRAM_UPLOAD_ID 123
#define ADCS_RESET_MCU_ID 124
#define ADCS_RUN_PROGRAM_NOW_ID 125
#define ADCS_COPY_TO_INTERNAL_FLASH_ID 126
#define ADCS_RESET_BOOT_STATISTICS_ID 127

#define ADCS_IDENTIFICATION_ID 128
#define ADCS_EXTENDED_IDENTIFICATION_ID 129
#define ADCS_COMMUNICATION_STATUS_ID 130
#define ADCS_TELECOMMAND_ACKNOWLEDGE_ID 131
//#define ADCS_RESET_CAUSE_ID 133
#define ADCS_POWER_CONTROL_SELECTION_ID 134
#define ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_ID 135
#define ADCS_ADCS_STATE_ID 136
#define ADCS_ADCS_MEASUREMENTS_ID 137
#define ADCS_ACTUATOR_COMMANDS_ID 138
#define ADCS_RAW_SENSOR_MEASUREMENTS_ID 139
#define ADCS_ESTIMATION_DATA_ID 140
#define ADCS_CURRENT_TIME_ID 143
#define ADCS_CURRENT_STATE_ID 144
#define ADCS_ESTIMATED_ATTITUDE_ANGLES_ID 145
#define ADCS_ESTIMATED_ANGULAR_RATES_ID 146
#define ADCS_SATELLITE_POSITION_LLH_ID 147
#define ADCS_SATELLITE_VELOCITY_ECI_ID 148
#define ADCS_MAGNETIC_FIELD_VECTOR_ID 149
#define ADCS_COARSE_SUN_VECTOR_ID 150
#define ADCS_FINE_SUN_VECTOR_ID 151
#define ADCS_NADIR_VECTOR_ID 152
#define ADCS_RATE_SENSOR_RATES_ID 153
#define ADCS_WHEEL_SPEED_ID 154
#define ADCS_MAGNETORQUER_COMMAND_ID 155
#define ADCS_WHEEL_SPEED_COMMANDS_ID 156
#define ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_ID 157
#define ADCS_MODELLED_SUN_VECTOR_ID 158
#define ADCS_ESTIMATED_GYRO_BIAS_ID 159
#define ADCS_ESTIMATION_INNOVATION_VECTOR_ID 160
#define ADCS_QUATERNION_ERROR_VECTOR_ID 161
#define ADCS_QUATERNION_COVARIANCE_ID 162
#define ADCS_ANGULAR_RATE_COVARIANCE_ID 163
#define ADCS_RAW_NADIR_SENSOR_ID 164
#define ADCS_RAW_SUN_SENSOR_ID 165
#define ADCS_RAW_CSS_ID 166
#define ADCS_RAW_MAGNETOMETER_ID 167
#define ADCS_RAW_GPS_STATUS_ID 168
#define ADCS_RAW_GPS_TIME_ID 169
#define ADCS_RAW_GPS_X_ID 170
#define ADCS_RAW_GPS_Y_ID 171
#define ADCS_RAW_GPS_Z_ID 172
#define ADCS_CUBESENSE_CURRENT_ID 173
#define ADCS_CUBECONTROL_CURRENT_ID 174
#define ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_ID 175
#define ADCS_ACP_EXECUTION_STATE_ID 189
#define ADCS_ACP_EXECUTION_TIMES_ID 190
#define ADCS_SGP4_ORBIT_PARAMETERS_ID 191
#define ADCS_CONFIGURATION_ID 192
#define ADCS_EDAC_AND_LATCHUP_COUNTERS_ID 193
#define ADCS_START_UP_MODE_ID 194
#define ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_ID 230
#define ADCS_FILE_INFORMATION_ID 240
#define ADCS_FILE_BLOCK_CRC_ID 241
#define ADCS_FILE_DATA_BLOCK_ID 242
#define ADCS_UPLOADED_PROGRAM_STATUS_ID 250
#define ADCS_GET_FLASH_PROGRAM_LIST_ID 251
#define ADCS_BOOT_INDEX_ID 252
#define ADCS_BOOTLOADER_STATUS_ID 253


#define ADCS_RESET_SIZE 1
#define ADCS_SET_UNIX_TIME_SIZE 6
#define ADCS_ADCS_RUN_MODE_SIZE 1
#define ADCS_SELECTED_LOGGED_DATA_SIZE 13
#define ADCS_POWER_CONTROL_SIZE 5
#define ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE 1
#define ADCS_TRIGGER_ADCS_LOOP_SIZE 0
#define ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE 1
#define ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE 4
#define ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE 6
#define ADCS_SET_WHEEL_SIZE 6
#define ADCS_SET_MAGNETORQUER_OUTPUT_SIZE 6
#define ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE 64
//#define ADCS_SET_CONFIGURATION_SIZE 240
#define ADCS_SET_MAGNETORQUER_SIZE 13
#define ADCS_SET_WHEEL_CONFIGURATION_SIZE 13
#define ADCS_SET_CSS_CONFIGURATION_SIZE 14
#define ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE 17
#define ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE 57
#define ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE 30
#define ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE 6
#define ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE 10
#define ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE 30
#define ADCS_SET_MOMENT_OF_INERTIA_SIZE 24
#define ADCS_SET_ESTIMATION_PARAMETERS_SIZE 26
#define ADCS_SAVE_CONFIGURATION_SIZE 0
#define ADCS_SAVE_ORBIT_PARAMETERS_SIZE 0
#define ADCS_SET_START_UP_MODE_SIZE 1
#define ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE 10
#define ADCS_RESET_FILE_LIST_SIZE 0
#define ADCS_ADVANCE_FILE_LIST_INDEX_SIZE 0
#define ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE 13
#define ADCS_ADVANCE_FILE_READ_POINTER_SIZE 2
#define ADCS_ERASE_FILE_SIZE 13
#define ADCS_ERASE_ALL_FILES_SIZE 0
#define ADCS_SET_BOOT_INDEX_SIZE 1
#define ADCS_ERASE_PROGRAM_SIZE 1
#define ADCS_UPLOAD_PROGRAM_BLOCK_SIZE 259
#define ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE 71
#define ADCS_RESET_MCU_SIZE 0
#define ADCS_RUN_PROGRAM_NOW_SIZE 0
#define ADCS_COPY_TO_INTERNAL_FLASH_SIZE 2
#define ADCS_RESET_BOOT_STATISTICS_SIZE 0
#define ADCS_IDENTIFICATION_SIZE 8
#define ADCS_EXTENDED_IDENTIFICATION_SIZE 6
#define ADCS_COMMUNICATION_STATUS_SIZE 6
#define ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE 4
//#define ADCS_RESET_CAUSE_SIZE 12
#define ADCS_POWER_CONTROL_SELECTION_SIZE 5
#define ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE 18
#define ADCS_ADCS_STATE_SIZE 48
#define ADCS_ADCS_MEASUREMENTS_SIZE 36
#define ADCS_ACTUATOR_COMMANDS_SIZE 12
#define ADCS_RAW_SENSOR_MEASUREMENTS_SIZE 6
#define ADCS_ESTIMATION_DATA_SIZE 42
#define ADCS_CURRENT_TIME_SIZE 6
#define ADCS_CURRENT_STATE_SIZE 6
#define ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE 6
#define ADCS_ESTIMATED_ANGULAR_RATES_SIZE 6
#define ADCS_SATELLITE_POSITION_LLH_SIZE 6
#define ADCS_SATELLITE_VELOCITY_ECI_SIZE 6
#define ADCS_MAGNETIC_FIELD_VECTOR_SIZE 6
#define ADCS_COARSE_SUN_VECTOR_SIZE 6
#define ADCS_FINE_SUN_VECTOR_SIZE 6
#define ADCS_NADIR_VECTOR_SIZE 6
#define ADCS_RATE_SENSOR_RATES_SIZE 6
#define ADCS_WHEEL_SPEED_SIZE 6
#define ADCS_MAGNETORQUER_COMMAND_SIZE 6
#define ADCS_WHEEL_SPEED_COMMANDS_SIZE 6
#define ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE 6
#define ADCS_MODELLED_SUN_VECTOR_SIZE 6
#define ADCS_ESTIMATED_GYRO_BIAS_SIZE 6
#define ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE 6
#define ADCS_QUATERNION_ERROR_VECTOR_SIZE 6
#define ADCS_QUATERNION_COVARIANCE_SIZE 6
#define ADCS_ANGULAR_RATE_COVARIANCE_SIZE 6
#define ADCS_RAW_NADIR_SENSOR_SIZE 6
#define ADCS_RAW_SUN_SENSOR_SIZE 6
#define ADCS_RAW_CSS_SIZE 6
#define ADCS_RAW_MAGNETOMETER_SIZE 6
#define ADCS_RAW_GPS_STATUS_SIZE 6
#define ADCS_RAW_GPS_TIME_SIZE 6
#define ADCS_RAW_GPS_X_SIZE 6
#define ADCS_RAW_GPS_Y_SIZE 6
#define ADCS_RAW_GPS_Z_SIZE 6
#define ADCS_CUBESENSE_CURRENT_SIZE 6
#define ADCS_CUBECONTROL_CURRENT_SIZE 6
#define ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE 6
#define ADCS_ACP_EXECUTION_STATE_SIZE 3
#define ADCS_ACP_EXECUTION_TIMES_SIZE 8
#define ADCS_SGP4_ORBIT_PARAMETERS_SIZE 64
#define ADCS_CONFIGURATION_SIZE 240
#define ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE 10
#define ADCS_START_UP_MODE_SIZE 1
#define ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE 2
#define ADCS_FILE_INFORMATION_SIZE 22
#define ADCS_FILE_BLOCK_CRC_SIZE 6
#define ADCS_FILE_DATA_BLOCK_SIZE 256
#define ADCS_UPLOADED_PROGRAM_STATUS_SIZE 3
#define ADCS_GET_FLASH_PROGRAM_LIST_SIZE 568
#define ADCS_BOOT_INDEX_SIZE 2
#define ADCS_BOOTLOADER_STATUS_SIZE 7

void ADCS_server(void* args);
void ADCS_callback(uint8_t length, uint8_t* data);
uint16_t CRC_calc(uint8_t *start, uint8_t *end);


/* Required typedefs */
typedef struct adcs_t adcs_t;
typedef struct adcs_identification_t adcs_identification_t;
typedef struct adcs_extended_identification_t adcs_extended_identification_t;
typedef struct adcs_communication_status_t adcs_communication_status_t;
typedef struct adcs_telecommand_acknowledge_t adcs_telecommand_acknowledge_t;
//typedef struct adcs_reset_cause_t adcs_reset_cause_t;
typedef struct adcs_actuator_commands_t adcs_actuator_commands_t;
typedef struct adcs_acp_execution_state_t adcs_acp_execution_state_t;
typedef struct adcs_acp_execution_times_t adcs_acp_execution_times_t;
typedef struct adcs_edac_and_latchup_counters_t adcs_edac_and_latchup_counters_t;
typedef struct adcs_start_up_mode_t adcs_start_up_mode_t;
typedef struct adcs_file_information_t adcs_file_information_t;
typedef struct adcs_file_block_crc_t adcs_file_block_crc_t;
typedef struct adcs_file_data_block_t adcs_file_data_block_t;
typedef struct adcs_power_control_selection_t adcs_power_control_selection_t;
typedef struct adcs_power_and_temperature_measurements_t adcs_power_and_temperature_measurements_t;
typedef struct adcs_adcs_state_t adcs_adcs_state_t;
typedef struct adcs_adcs_measurements_t adcs_adcs_measurements_t;
typedef struct adcs_current_time_t adcs_current_time_t;
typedef struct adcs_current_state_t adcs_current_state_t;
typedef struct adcs_estimated_attitude_angles_t adcs_estimated_attitude_angles_t;
typedef struct adcs_estimated_angular_rates_t adcs_estimated_angular_rates_t;
typedef struct adcs_satellite_position_llh_t adcs_satellite_position_llh_t;
typedef struct adcs_satellite_velocity_eci_t adcs_satellite_velocity_eci_t;
typedef struct adcs_magnetic_field_vector_t adcs_magnetic_field_vector_t;
typedef struct adcs_coarse_sun_vector_t adcs_coarse_sun_vector_t;
typedef struct adcs_fine_sun_vector_t adcs_fine_sun_vector_t;
typedef struct adcs_nadir_vector_t adcs_nadir_vector_t;
typedef struct adcs_rate_sensor_rates_t adcs_rate_sensor_rates_t;
typedef struct adcs_wheel_speed_t adcs_wheel_speed_t;
typedef struct adcs_cubesense_current_t adcs_cubesense_current_t;
typedef struct adcs_cubecontrol_current_t adcs_cubecontrol_current_t;
typedef struct adcs_peripheral_current_and_temperature_measurements_t adcs_peripheral_current_and_temperature_measurements_t;
typedef struct adcs_raw_sensor_measurements_t adcs_raw_sensor_measurements_t;
typedef struct adcs_angular_rate_covariance_t adcs_angular_rate_covariance_t;
typedef struct adcs_raw_nadir_sensor_t adcs_raw_nadir_sensor_t;
typedef struct adcs_raw_sun_sensor_t adcs_raw_sun_sensor_t;
typedef struct adcs_raw_css_t adcs_raw_css_t;
typedef struct adcs_raw_magnetometer_t adcs_raw_magnetometer_t;
typedef struct adcs_raw_gps_status_t adcs_raw_gps_status_t;
typedef struct adcs_raw_gps_time_t adcs_raw_gps_time_t;
typedef struct adcs_raw_gps_x_t adcs_raw_gps_x_t;
typedef struct adcs_raw_gps_y_t adcs_raw_gps_y_t;
typedef struct adcs_raw_gps_z_t adcs_raw_gps_z_t;
typedef struct adcs_estimation_data_t adcs_estimation_data_t;
typedef struct adcs_igrf_modelled_magnetic_field_vector_t adcs_igrf_modelled_magnetic_field_vector_t;
typedef struct adcs_modelled_sun_vector_t adcs_modelled_sun_vector_t;
typedef struct adcs_estimated_gyro_bias_t adcs_estimated_gyro_bias_t;
typedef struct adcs_estimation_innovation_vector_t adcs_estimation_innovation_vector_t;
typedef struct adcs_quaternion_error_vector_t adcs_quaternion_error_vector_t;
typedef struct adcs_quaternion_covariance_t adcs_quaternion_covariance_t;
typedef struct adcs_magnetorquer_command_t adcs_magnetorquer_command_t;
typedef struct adcs_wheel_speed_commands_t adcs_wheel_speed_commands_t;
typedef struct adcs_sgp4_orbit_parameters_t adcs_sgp4_orbit_parameters_t;
typedef struct adcs_configuration_t adcs_configuration_t;
typedef struct adcs_status_of_image_capture_and_save_operation_t adcs_status_of_image_capture_and_save_operation_t;
typedef struct adcs_uploaded_program_status_t adcs_uploaded_program_status_t;
typedef struct adcs_get_flash_program_list_t adcs_get_flash_program_list_t;
typedef struct adcs_reset_t adcs_reset_t;
typedef struct adcs_set_unix_time_t adcs_set_unix_time_t;
typedef struct adcs_adcs_run_mode_t adcs_adcs_run_mode_t;
typedef struct adcs_selected_logged_data_t adcs_selected_logged_data_t;
typedef struct adcs_power_control_t adcs_power_control_t;
typedef struct adcs_deploy_magnetometer_boom_t adcs_deploy_magnetometer_boom_t;
typedef struct adcs_trigger_adcs_loop_t adcs_trigger_adcs_loop_t;
typedef struct adcs_set_attitude_estimation_mode_t adcs_set_attitude_estimation_mode_t;
typedef struct adcs_set_attitude_control_mode_t adcs_set_attitude_control_mode_t;
typedef struct adcs_set_commanded_attitude_angles_t adcs_set_commanded_attitude_angles_t;
typedef struct adcs_set_wheel_t adcs_set_wheel_t;
typedef struct adcs_set_magnetorquer_output_t adcs_set_magnetorquer_output_t;
typedef struct adcs_set_start_up_mode_t adcs_set_start_up_mode_t;
typedef struct adcs_set_sgp4_orbit_parameters_t adcs_set_sgp4_orbit_parameters_t;
//typedef struct adcs_set_configuration_t adcs_set_configuration_t;
typedef struct adcs_set_magnetorquer_t adcs_set_magnetorquer_t;
typedef struct adcs_set_wheel_configuration_t adcs_set_wheel_configuration_t;
typedef struct adcs_set_css_configuration_t adcs_set_css_configuration_t;
typedef struct adcs_set_sun_sensor_configuration_t adcs_set_sun_sensor_configuration_t;
typedef struct adcs_set_nadir_sensor_configuration_t adcs_set_nadir_sensor_configuration_t;
typedef struct adcs_set_magnetometer_configuration_t adcs_set_magnetometer_configuration_t;
typedef struct adcs_set_rate_sensor_configuration_t adcs_set_rate_sensor_configuration_t;
typedef struct adcs_set_detumbling_control_parameters_t adcs_set_detumbling_control_parameters_t;
typedef struct adcs_set_y_momentum_control_parameters_t adcs_set_y_momentum_control_parameters_t;
typedef struct adcs_set_moment_of_inertia_t adcs_set_moment_of_inertia_t;
typedef struct adcs_set_estimation_parameters_t adcs_set_estimation_parameters_t;
typedef struct adcs_save_configuration_t adcs_save_configuration_t;
typedef struct adcs_save_orbit_parameters_t adcs_save_orbit_parameters_t;
typedef struct adcs_capture_and_save_image_t adcs_capture_and_save_image_t;
typedef struct adcs_reset_file_list_t adcs_reset_file_list_t;
typedef struct adcs_advance_file_list_index_t adcs_advance_file_list_index_t;
typedef struct adcs_initialize_file_download_t adcs_initialize_file_download_t;
typedef struct adcs_advance_file_read_pointer_t adcs_advance_file_read_pointer_t;
typedef struct adcs_erase_file_t adcs_erase_file_t;
typedef struct adcs_erase_all_files_t adcs_erase_all_files_t;
typedef struct adcs_set_boot_index_t adcs_set_boot_index_t;
typedef struct adcs_erase_program_t adcs_erase_program_t;
typedef struct adcs_upload_program_block_t adcs_upload_program_block_t;
typedef struct adcs_finalize_program_upload_t adcs_finalize_program_upload_t;
typedef struct adcs_boot_index_t adcs_boot_index_t;
typedef struct adcs_reset_mcu_t adcs_reset_mcu_t;
typedef struct adcs_run_program_now_t adcs_run_program_now_t;
typedef struct adcs_copy_to_internal_flash_t adcs_copy_to_internal_flash_t;
typedef struct adcs_reset_boot_statistics_t adcs_reset_boot_statistics_t;
typedef struct adcs_bootloader_status_t adcs_bootloader_status_t;



/**
* @brief
*	Identification information for this node
*/
struct __attribute__((packed)) adcs_identification_t
{
	uint8_t node_type;
	uint8_t interface_version; // should have value of 2
	uint8_t firmware_version_major;
	uint8_t firmware_version_minor;
	uint16_t runtime_seconds;
	uint16_t runtime_millis;
};

/**
* @brief
*	Extended identification information for this node
*/
struct __attribute__((packed)) adcs_extended_identification_t
{
	uint8_t boot_cause;
	uint16_t boot_count;
	uint8_t boot_index;
	uint8_t firmware_major;
	uint8_t firmware_minor;
};

 /**
* @brief
*	Communication status for the CubeSense. Includes command and telemetry counters and error flags
*/
struct __attribute__((packed)) adcs_communication_status_t
{
	uint16_t telecommand_counter;
	uint16_t telemetry_request_counter;
	/*
	 * comm_status_booleans contains 4 bool values. From least struct offset to most:
	 * 1. if telecommand buffer was overrun while receiving a telecommand
	 * 2. i2c telemetry file read error, if occurred
	 * 3. UART protocol error if occured
	 * 4. if there has been an incomplete UART message received (start-of-message id received without preceding end-of-message-id)
	 */
	uint8_t comm_status_booleans;
	/*uint8_t telecommand_buffer_overrun; // actually is bool, also one bit long
	uint8_t i2c_TLM_read_error; // actually is bool, also one bit long
	uint8_t UART_protocol_error; // actually is bool, also one bit long
	uint8_t UART_incomplete_message; // actually is bool, also one bit long*/
};

 /**
* @brief
*	Telemetry frame with acknowledge status of the previously sent command
*/
struct __attribute__((packed)) adcs_telecommand_acknowledge_t
{
	uint8_t last_telecommand_id;
	uint8_t processed; // actually is bool
	uint8_t telecommand_error_status;
	uint8_t telecommand_parameter_error_index;
};

 /**
* @brief
*	Cause of last reset
*//*
struct __attribute__((packed)) adcs_reset_cause_t
{
	uint8_t cause;
};*/

 /**
* @brief
*	Actuator commands
*/
struct __attribute__((packed)) adcs_actuator_commands_t
{
	int16_t magnetorquer_command_x;
	int16_t magnetorquer_command_y;
	int16_t magnetorquer_command_z;
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

 /**
* @brief
*	Returns information about the ACP loop
*/
struct __attribute__((packed)) adcs_acp_execution_state_t
{
	uint16_t time_since_iteration_start;
	uint8_t current_execution_point;
};

 /**
* @brief
*	Returns information about execution times of ACP functions
*/
struct __attribute__((packed)) adcs_acp_execution_times_t
{
	uint16_t time_to_perform_ADCS_update;
	uint16_t time_to_perform_sensor_or_actuator_comms;
	uint16_t time_to_execute_SGP4_propagator;
	uint16_t time_to_execute_IGRF_model;
};

 /**
* @brief
*	Counters for SRAM SEUs and latch-up
*/
struct __attribute__((packed)) adcs_edac_and_latchup_counters_t
{
	uint16_t single_SRAM_upsets;
	uint16_t double_SRAM_upsets;
	uint16_t multiple_SRAM_upsets;
	uint16_t SRAM1_latchups;
	uint16_t SRAM2_latchups;
};

 /**
* @brief
*	The Current Start-up Behaviour of the ACP
*/
struct __attribute__((packed)) adcs_start_up_mode_t
{
	enum {on,off} mode;
};

 /**
* @brief
*	File information for current file index
*/
struct __attribute__((packed)) adcs_file_information_t
{
	/*
	 * file_info_flags contains 2 bools. From least struct offset to most:
	 * 1. processing flag (this file entry only allowed if this flag is true)
	 * 2. end reached flag
	 */
	uint8_t file_info_flags;
	/*uint8_t processing_flag; // actually is bool, and is 1 bit long
	uint8_t end_reached_flag; // actually is bool, and is 1 bit long, but the two together are 8 bits long*/

	char filename[13];
	uint32_t filesize;
	uint16_t date;
	uint16_t time;
};

 /**
* @brief
*	File block CRC
*/
struct __attribute__((packed)) adcs_file_block_crc_t
{
	uint16_t block_number;
	uint8_t block_length;

	/*
	 * file_block_CRC_flags contains 4 bool flags. From least struct offset to most:
	 * 1. processing flag (busy reading file into telemetry buffer)
	 * 2. end reached flag
	 * 3. file not found
	 * read error
	*/
	uint8_t file_block_CRC_flags;
	/*uint8_t processing_flag; // actually is bool, and is 1 bit long
	uint8_t end_reached; // actually is bool, and is 1 bit long
	uint8_t file_not_found; // actually is bool, and is 1 bit long
	uint8_t read_error; // actually is bool, and is 1 bit long; the 4 together are 8 bits long*/

	uint16_t CRC16_checksum;
};

 /**
* @brief
*	File data block
*/
struct __attribute__((packed)) adcs_file_data_block_t
{
	uint8_t	 block[256];
};

 /**
* @brief
*	Power control selection for ADCS components
*/
struct __attribute__((packed)) adcs_power_control_selection_t
{
	uint8_t cube_control_signal_power_selection;
	uint8_t cube_control_motor_power_selection;
	uint8_t cube_sense_power_selection;
	uint8_t cube_motor_power_selection;
	uint8_t gps_lna_power_selection;
};

 /**
* @brief
*	Power and temperature measurements
*/
struct __attribute__((packed)) adcs_power_and_temperature_measurements_t
{
	uint16_t cubesense_3V3_current;
	uint8_t cubesense_nadir_sram_current;
	uint8_t cubesense_sun_sram_current;
	int16_t ARM_CPU_temperature;
	uint16_t cubecontrol_3V3_current;
	uint16_t cubecontrol_5V_current;
	uint16_t cubecontrol_Vbat_current;
	uint16_t magnetorquer_current;
	uint16_t momentum_wheel_current;
	int8_t rate_sensor_temperature;
	int8_t magnetometer_temperature;
};
 /**
* @brief
*	Current ADCS state
*/

struct __attribute__((packed)) adcs_adcs_state_t
{
	uint32_t current_unix_time;
	uint16_t millis;

	/*
	 * adcs modes contains three values in it. From least struct offset to most:
	 * 1. adcs loop run mode (adcs_run_mode_t, 2 bits)
	 * 2. estimation mode (adcs_attitude_estimation_mode_t, 3 bits)
	 * 3. adcs control mode (adcs_attitude_control_mode_value_t, 3 bits)
	 * 		0 - no control
	 * 		2 - detumbling control
	 * 		3 - y momentum stabilized - initial pitch acquisition
	 * 		4 - y momentum stabilized - steady state
	 */
	uint8_t adcs_modes;
	/*adcs_run_mode_t adcs_state; //2 bits long instead of 8
	adcs_attitude_estimation_mode_t attitude_estimation_mode; //3 bits long instead of 8
	adcs_attitude_control_mode_value_t attitude_control_mode; //3s bits long instead of 8*/
	/*
	 * adcs_state_bool_flags_set1 has 8 bool error flags in it. From least struct offset to most:
	 * 1. cube control signal enabled
	 * 2. cube control motor enabled
	 * 3. cube sense enabled
	 * 4. gps receiver enabled
	 * 5. gps LNA enabled
	 * 6. motor driver enabled
	 * 7. magnetometer deployment enabled
	 * 8. config load error
	 */
	uint8_t adcs_state_bool_flags_set1;
	/*uint8_t cubecontrol_signal_enabled; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_enabled; // actually is bool, and is 1 bit long
	uint8_t cubesense_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_reciever_enabled; // actually is bool, and is 1 bit long
	uint8_t gps_lna_power_enabled; // actually is bool, and is 1 bit long
	uint8_t motor_driver_enabled; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_enabled; // actually is bool, and is 1 bit long
	uint8_t config_load_error; // actually is bool, and is 1 bit long*/
	/*
	 * adcs_state_bool_flags_set2 contains 8 boolean flags. From least struct offset to most:
	 * 1. orbit parameters load error
	 * 2. cube sense communication error
	 * 3. cube control signal communication error
	 * 4. cube control motor communication error
	 * 5. cube sense node communication error
	 * 6. cube control signal identification error
	 * 7. cube control motor identification error
	 * 8. magnetometer deployment error
	 */
	uint8_t adcs_state_bool_flags_set2;
	/*uint8_t orbit_params_load_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_comm_error; // actually is bool, and is 1 bit long
	uint8_t cube_control_signal_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubesense_node_comm_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_signal_identification_error; // actually is bool, and is 1 bit long
	uint8_t cubecontrol_motor_identification_error; // actually is bool, and is 1 bit long
	uint8_t magnetometer_deployment_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set3 contains 8 boolean flags. From least struct offset to most:
	 * 1. control loop time exceeded
	 * 2. magnetometer range error
	 * 3. sun sensor overcurrent detected
	 * 4. sun sensor busy error
	 * 5. sun sensor detection error
	 * 6. wheel control error
	 * 7. nadir sensor overcurrent detected
	 * 8. nadir sensor budy error
	 */
	uint8_t adcs_state_bool_flags_set3;
	/*uint8_t control_loop_time_exceeded; // actually is bool, and is 1 bit long
	uint8_t magnetometer_range_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_busy_error; // actually is bool, and is 1 bit long
	uint8_t sun_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t wheel_control_error; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
	uint8_t nadir_sensor_busy_error; // actually is bool, and is 1 bit long*/

	/*
	 * adcs_state_bool_flags_set4 contains 8 boolean flags. From least struct offset to most:
	 * 1. nadir sensor detection error
	 * 2. estimation error
	 * 3. rate sensor range error
	 * 4. wheel speed range error
	 * 5. coarse sun sensor error
	 * 6. orbit parameters invalid
	 * 7. config invalid
	 * 8. control mode change is not allowed
	 */
	uint8_t adcs_state_bool_flags_set4;
	/*uint8_t nadir_sensor_detection_error; // actually is bool, and is 1 bit long
	uint8_t estimation_error; // actually is bool, and is 1 bit long
	uint8_t rate_sensor_range_error; // actually is bool, and is 1 bit long
	uint8_t wheel_speed_range_error; // actually is bool, and is 1 bit long
	uint8_t coarse_sun_sensor_error; // actually is bool, and is 1 bit long
	uint8_t orbit_parameters_invalid; // actually is bool, and is 1 bit long
	uint8_t config_invalid; // actually is bool, and is 1 bit long
	uint8_t control_mode_change_not_allowed; // actually is bool, and is 1 bit long*/
	/*
	 * adcs_state_bool_flags_set5 contains 8 boolean flags. From least struct offset to most:
	 * 1. estimator change is not allowed
	 * 2. modelled and measured magnetic fields differ in size
	 * 3. SRAM latchup error
	 * 4. SD card error
	 * 5. node recovery error
	 * 6. steady state Y-Thompson deteceted
	 * 7. TRIAD and EKF report same attitude
	 * 8. sun is above local horizon
	 */
	uint8_t adcs_state_bool_flags_set5;
	/*uint8_t estimator_change_not_allowed; // actually is bool, and is 1 bit long
	uint8_t modelled_and_measured_magnetic_fields_differ_in_size; // actually is bool, and is 1 bit long
	uint8_t SRAM_latchup_error; // actually is bool, and is 1 bit long
	uint8_t SD_card_error; // actually is bool, and is 1 bit long
	uint8_t node_recovery_error; // actually is bool, and is 1 bit long
	uint8_t steady_state_Y_thompson_detected; // actually is bool, and is 1 bit long
	uint8_t TRIAD_and_EKF_report_same_attitude; // actually is bool, and is 1 bit long
	uint8_t sun_above_local_horizon; // actually is bool, and is 1 bit long*/

	int16_t commanded_roll_angle;
	int16_t commanded_pitch_angle;
	int16_t commanded_yaw_angle;
	int16_t estimated_roll_angle;
	int16_t estimated_pitch_angle;
	int16_t estimated_yaw_angle;
	int16_t estimated_X_angular_rate;
	int16_t estimated_Y_angular_rate;
	int16_t estimated_Z_angular_rate;
	int16_t X_position;
	int16_t Y_position;
	int16_t Z_position;
	int16_t X_velocity;
	int16_t Y_velocity;
	int16_t Z_velocity;
	int16_t latitude;
	int16_t longitude;
	int16_t altitude;
};

 /**
* @brief
*	Calibrated sensor measurements
*/
struct __attribute__((packed)) adcs_adcs_measurements_t
{
	int16_t magnetic_field_x;
	int16_t magnetic_field_y;
	int16_t magnetic_field_z;
	int16_t coarse_sun_x;
	int16_t coarse_sun_y;
	int16_t coarse_sun_z;
	int16_t sun_x;
	int16_t sun_y;
	int16_t sun_z;
	int16_t nadir_x;
	int16_t nadir_y;
	int16_t nadir_z;
	int16_t angular_rate_x;
	int16_t angular_rate_y;
	int16_t angular_rate_z;
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

 /**
* @brief
*	Current unix time
*/
struct __attribute__((packed)) adcs_current_time_t
{
	uint32_t unix_time;
	uint16_t millis;
};

 /**
* @brief
*	Current state of the Attitude Control Processor
*/
struct __attribute__((packed)) adcs_current_state_t
{
	uint8_t adcs_modes;
		/*adcs_run_mode_t adcs_state; //2 bits long instead of 8
		adcs_attitude_estimation_mode_t attitude_estimation_mode; //3 bits long instead of 8
		adcs_attitude_control_mode_value_t attitude_control_mode; //3s bits long instead of 8*/

		/*
		 * adcs_state_bool_flags_set1 has 8 bool error flags in it. From least struct offset to most:
		 * 1. cube control signal enabled
		 * 2. cube control motor enabled
		 * 3. cube sense enabled
		 * 4. gps receiver enabled
		 * 5. gps LNA enabled
		 * 6. motor driver enabled
		 * 7. magnetometer deployment enabled
		 * 8. config load error
		 */
		uint8_t adcs_state_bool_flags_set1;
		/*uint8_t cubecontrol_signal_enabled; // actually is bool, and is 1 bit long
		uint8_t cubecontrol_motor_enabled; // actually is bool, and is 1 bit long
		uint8_t cubesense_enabled; // actually is bool, and is 1 bit long
		uint8_t gps_reciever_enabled; // actually is bool, and is 1 bit long
		uint8_t gps_lna_power_enabled; // actually is bool, and is 1 bit long
		uint8_t motor_driver_enabled; // actually is bool, and is 1 bit long
		uint8_t magnetometer_deployment_enabled; // actually is bool, and is 1 bit long
		uint8_t config_load_error; // actually is bool, and is 1 bit long*/

		/*
		 * adcs_state_bool_flags_set2 contains 8 boolean flags. From least struct offset to most:
		 * 1. orbit parameters load error
		 * 2. cube sense communication error
		 * 3. cube control signal communication error
		 * 4. cube control motor communication error
		 * 5. cube sense node communication error
		 * 6. cube control signal identification error
		 * 7. cube control motor identification error
		 * 8. magnetometer deployment error
		 */
		uint8_t adcs_state_bool_flags_set2;
		/*uint8_t orbit_params_load_error; // actually is bool, and is 1 bit long
		uint8_t cubesense_comm_error; // actually is bool, and is 1 bit long
		uint8_t cube_control_signal_comm_error; // actually is bool, and is 1 bit long
		uint8_t cubecontrol_motor_comm_error; // actually is bool, and is 1 bit long
		uint8_t cubesense_node_comm_error; // actually is bool, and is 1 bit long
		uint8_t cubecontrol_signal_identification_error; // actually is bool, and is 1 bit long
		uint8_t cubecontrol_motor_identification_error; // actually is bool, and is 1 bit long
		uint8_t magnetometer_deployment_error; // actually is bool, and is 1 bit long*/

		/*
		 * adcs_state_bool_flags_set3 contains 8 boolean flags. From least struct offset to most:
		 * 1. control loop time exceeded
		 * 2. magnetometer range error
		 * 3. sun sensor overcurrent detected
		 * 4. sun sensor busy error
		 * 5. sun sensor detection error
		 * 6. sun sensor range error
		 * 7. nadir sensor overcurrent detected
		 * 8. nadir sensor budy error
		 */
		uint8_t adcs_state_bool_flags_set3;
		/*uint8_t control_loop_time_exceeded; // actually is bool, and is 1 bit long
		uint8_t magnetometer_range_error; // actually is bool, and is 1 bit long
		uint8_t sun_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
		uint8_t sun_sensor_busy_error; // actually is bool, and is 1 bit long
		uint8_t sun_sensor_detection_error; // actually is bool, and is 1 bit long
		uint8_t sun_sensor_range_error; // actually is bool, and is 1 bit long
		uint8_t nadir_sensor_overcurrent_detected; // actually is bool, and is 1 bit long
		uint8_t nadir_sensor_busy_error; // actually is bool, and is 1 bit long*/

		/*
		 * adcs_state_bool_flags_set4 contains 8 boolean flags. From least struct offset to most:
		 * 1. nadir sensor detection error
		 * 2. nadir sensor range error
		 * 3. rate sensor range error
		 * 4. wheel speed range error
		 * 5. coarse sun sensor error
		 * 6. orbit parameters invalid
		 * 7. config invalid
		 * 8. control mode change is not allowed
		 */
		uint8_t adcs_state_bool_flags_set4;
		/*uint8_t nadir_sensor_detection_error; // actually is bool, and is 1 bit long
		uint8_t nadir_sensor_range_error; // actually is bool, and is 1 bit long
		uint8_t rate_sensor_range_error; // actually is bool, and is 1 bit long
		uint8_t wheel_speed_range_error; // actually is bool, and is 1 bit long
		uint8_t coarse_sun_sensor_error; // actually is bool, and is 1 bit long
		uint8_t orbit_parameters_invalid; // actually is bool, and is 1 bit long
		uint8_t config_invalid; // actually is bool, and is 1 bit long
		uint8_t control_mode_change_not_allowed; // actually is bool, and is 1 bit long*/

		/*
		 * adcs_state_bool_flags_set5 contains 8 boolean flags. From least struct offset to most:
		 * 1. estimator change is not allowed
		 * 2. modelled and measured magnetic fields differ in size
		 * 3. SRAM latchup error
		 * 4. SD card error
		 * 5. node recovery error
		 * 6. steady state Y-Thompson deteceted
		 * 7. TRIAD and EKF report same attitude
		 * 8. sun is above local horizon
		 */
		uint8_t adcs_state_bool_flags_set5;
		/*uint8_t estimator_change_not_allowed; // actually is bool, and is 1 bit long
		uint8_t modelled_and_measured_magnetic_fields_differ_in_size; // actually is bool, and is 1 bit long
		uint8_t SRAM_latchup_error; // actually is bool, and is 1 bit long
		uint8_t SD_card_error; // actually is bool, and is 1 bit long
		uint8_t node_recovery_error; // actually is bool, and is 1 bit long
		uint8_t steady_state_Y_thompson_detected; // actually is bool, and is 1 bit long
		uint8_t TRIAD_and_EKF_report_same_attitude; // actually is bool, and is 1 bit long
		uint8_t sun_above_local_horizon; // actually is bool, and is 1 bit long*/
};

 /**
* @brief
*	Estimated attitude angles
*/
struct __attribute__((packed)) adcs_estimated_attitude_angles_t
{
	int16_t estimated_roll_angle;
	int16_t estimated_pitch_angle;
	int16_t estimated_yaw_angle;;
};

 /**
* @brief
*	Estimated angular rates relative to orbit reference frame
*/
struct __attribute__((packed)) adcs_estimated_angular_rates_t
{
	int16_t estimated_X_angular_rate;
	int16_t estimated_Y_angular_rate;
	int16_t estimated_Z_angular_rate;
};

 /**
* @brief
*	Satellite position in WGS-84 coordinate frame
*/
struct __attribute__((packed)) adcs_satellite_position_llh_t
{
	int16_t latitude;
	int16_t longitude;
	int16_t altitude;
};

 /**
* @brief
*	Satellite velocity in ECI frame
*/
struct __attribute__((packed)) adcs_satellite_velocity_eci_t
{
	int16_t X_velocity;
	int16_t Y_velocity;
	int16_t Z_velocity;
};

 /**
* @brief
*	Measured magnetic field vector
*/
struct __attribute__((packed)) adcs_magnetic_field_vector_t
{
	int16_t magnetic_field_x;
	int16_t magnetic_field_y;
	int16_t magnetic_field_z;
};

 /**
* @brief
*	Measured coarse sun vector
*/
struct __attribute__((packed)) adcs_coarse_sun_vector_t
{
	int16_t coarse_sun_x;
	int16_t coarse_sun_y;
	int16_t coarse_sun_z;
};

 /**
* @brief
*	Measured fine sun vector
*/
struct __attribute__((packed)) adcs_fine_sun_vector_t
{
	int16_t sun_x;
	int16_t sun_y;
	int16_t sun_z;
};

 /**
* @brief
*	Measured nadir vector
*/
struct __attribute__((packed)) adcs_nadir_vector_t
{
	int16_t nadir_x;
	int16_t nadir_y;
	int16_t nadir_z;
};

 /**
* @brief
*	Rate sensor measurements
*/
struct __attribute__((packed)) adcs_rate_sensor_rates_t
{
	int16_t angular_rate_x;
	int16_t angular_rate_y;
	int16_t angular_rate_z;
};

 /**
* @brief
*	Wheel speed measurement
*/
struct __attribute__((packed)) adcs_wheel_speed_t
{
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

 /**
* @brief
*	CubeSense current measurements
*/
struct __attribute__((packed)) adcs_cubesense_current_t
{
	uint16_t cubesense_3V3_current;
	uint8_t cubesense_nadir_sram_current;
	uint8_t cubesense_sun_sram_current;
	int16_t cpu_temperature;
};

 /**
* @brief
*	CubeControl current measurements
*/
struct __attribute__((packed)) adcs_cubecontrol_current_t
{
	uint16_t cubecontrol_3V3_current;
	uint16_t cubecontrol_5V_current;
	uint16_t cubecontrol_Vbat_current;
};

 /**
* @brief
*	Magnetorquer and wheel current measurement. Rate sensor and MCU temperature measurement
*/
struct __attribute__((packed)) adcs_peripheral_current_and_temperature_measurements_t
{
	uint16_t magnetorquer_current;
	uint16_t momentum_wheel_current;
	uint8_t rate_sensor_temperature;
	uint8_t magnetometer_temperature;
};

 /**
* @brief
*	Raw sensor measurements
*/
struct __attribute__((packed)) adcs_raw_sensor_measurements_t
{
	int16_t nadir_centroid_x;
		int16_t nadir_centroid_y;
		uint8_t nadir_busy_status;
		uint8_t nadir_detection_result;
		int16_t sun_centroid_x;
		int16_t sun_centroid_y;
		uint8_t sun_busy_status;
		uint8_t sun_detection_result;
		uint8_t css1;
		uint8_t css2;
		uint8_t css3;
		uint8_t css4;
		uint8_t css5;
		uint8_t css6;
		int16_t magx;
		int16_t magy;
		int16_t magz;
		uint8_t GPS_solution_status;
		uint8_t number_of_tracked_GPS_satellites;
		uint8_t number_of_GPS_satellites_used_in_solution;
		uint8_t counter_for_XYZ_log_from_GPS;
		uint8_t counter_for_RANGE_log_from_GPS;
		uint8_t response_message_for_GPS_log_setup;
		uint8_t GPS_reference_week;
		uint8_t GPS_time_millis;
		int32_t ECEF_position_x;
		int16_t ECEF_velocity_x;
		int32_t ECEF_position_y;
		int16_t ECEF_velocity_y;
		int32_t ECEF_position_z;
		int16_t ECEF_velocity_z;
		uint8_t x_pos_stddev;
		uint8_t y_pos_stddev;
		uint8_t z_pos_stddev;
		uint8_t x_vel_stddev;
		uint8_t y_vel_stddev;
		uint8_t z_vel_stddev;
};

 /**
* @brief
*	Angular rate covariance
*/
struct __attribute__((packed)) adcs_angular_rate_covariance_t
{
	int16_t angular_rate_covariance_x;
	int16_t angular_rate_covariance_y;
	int16_t angular_rate_covariance_z;
};

 /**
* @brief
*	Nadir sensor capture and detection result
*/
struct __attribute__((packed)) adcs_raw_nadir_sensor_t
{
	int16_t nadir_centroid_x;
	int16_t nadir_centroid_y;
	uint8_t nadir_busy_status;
	uint8_t nadir_detection_result;
};

 /**
* @brief
*	Sun sensor capture and detection result
*/
struct __attribute__((packed)) adcs_raw_sun_sensor_t
{
	int16_t sun_centroid_x;
	int16_t sun_centroid_y;
	uint8_t sun_busy_status;
	uint8_t sun_detection_result;};

 /**
* @brief
*	Raw CSS measurements
*/
struct __attribute__((packed)) adcs_raw_css_t
{
	uint8_t css1;
	uint8_t css2;
	uint8_t css3;
	uint8_t css4;
	uint8_t css5;
	uint8_t css6;
};

 /**
* @brief
*	Raw magnetometer measurements
*/
struct __attribute__((packed)) adcs_raw_magnetometer_t
{
	int16_t magx;
	int16_t magy;
	int16_t magz;
};

 /**
* @brief
*	Raw GPS status
*/
struct __attribute__((packed)) adcs_raw_gps_status_t
{
	uint8_t GPS_solution_status;
	uint8_t number_of_tracked_GPS_satellites;
	uint8_t number_of_GPS_satellites_used_in_solution;
	uint8_t counter_for_XYZ_log_from_GPS;
	uint8_t counter_for_RANGE_log_from_GPS;
	uint8_t response_message_for_GPS_log_setup;
};

 /**
* @brief
*	Raw GPS time
*/
struct __attribute__((packed)) adcs_raw_gps_time_t
{
	uint8_t GPS_reference_week;
	uint8_t GPS_time_millis;
};

 /**
* @brief
*	Raw GPS X position and velocity (ECI referenced)
*/
struct __attribute__((packed)) adcs_raw_gps_x_t
{
	int32_t ECEF_position_x;
	int16_t ECEF_velocity_x;
};

 /**
* @brief
*	Raw GPS Y Position and velocity (ECI referenced)
*/
struct __attribute__((packed)) adcs_raw_gps_y_t
{
	int32_t ECEF_position_y;
	int16_t ECEF_velocity_y;
};

 /**
* @brief
*	Raw GPS Z position and velocity (ECI referenced)
*/
struct __attribute__((packed)) adcs_raw_gps_z_t
{
	int32_t ECEF_position_z;
	int16_t ECEF_velocity_z;
};

 /**
* @brief
*	Estimation meta-data
*/
struct __attribute__((packed)) adcs_estimation_data_t
{
	int16_t IGRF_modelled_mag_field_x;
	int16_t IGRF_modelled_mag_field_y;
	int16_t IGRF_modelled_mag_field_z;
	int16_t modelled_sun_vector_x;
	int16_t modelled_sun_vector_y;
	int16_t modelled_sun_vector_z;
	int16_t estimated_x_gyro_bias;
	int16_t estimated_y_gyro_bias;
	int16_t estimated_z_gyro_bias;
	int16_t innovation_vector_x;
	int16_t innovation_vector_y;
	int16_t innovation_vector_z;
	int16_t quaternion1_error;
	int16_t quaternion2_error;
	int16_t quaternion3_error;
	int16_t quaternion1_rms_covariance;
	int16_t quaternion2_rms_covariance;
	int16_t quaternion3_rms_covariance;
	int16_t angular_rate_covariance_x;
	int16_t angular_rate_covariance_y;
	int16_t angular_rate_covariance_z;
};

 /**
* @brief
*	IGRF modelled magnetic field vector (orbit frame referenced)
*/
struct __attribute__((packed)) adcs_igrf_modelled_magnetic_field_vector_t
{
	int16_t IGRF_modelled_mag_field_x;
	int16_t IGRF_modelled_mag_field_y;
	int16_t IGRF_modelled_mag_field_z;
};

 /**
* @brief
*	Modelled sun vector (orbit frame referenced)
*/
struct __attribute__((packed)) adcs_modelled_sun_vector_t
{
	int16_t modelled_sun_vector_x;
	int16_t modelled_sun_vector_y;
	int16_t modelled_sun_vector_z;
};

 /**
* @brief
*	Estimated rate sensor bias
*/
struct __attribute__((packed)) adcs_estimated_gyro_bias_t
{
	int16_t estimated_x_gyro_bias;
	int16_t estimated_y_gyro_bias;
	int16_t estimated_z_gyro_bias;
};

 /**
* @brief
*	Estimation innovation vector
*/
struct __attribute__((packed)) adcs_estimation_innovation_vector_t
{
	int16_t innovation_vector_x;
	int16_t innovation_vector_y;
	int16_t innovation_vector_z;};

 /**
* @brief
*	Quaternion error vector
*/
struct __attribute__((packed)) adcs_quaternion_error_vector_t
{
	int16_t quaternion1_error;
	int16_t quaternion2_error;
	int16_t quaternion3_error;
};

 /**
* @brief
*	Quaternion covariance
*/
struct __attribute__((packed)) adcs_quaternion_covariance_t
{
	int16_t quaternion1_rms_covariance;
	int16_t quaternion2_rms_covariance;
	int16_t quaternion3_rms_covariance;
};

 /**
* @brief
*	Magnetorquer commands
*/
struct __attribute__((packed)) adcs_magnetorquer_command_t
{
	int16_t magnetorquer_command_x;
	int16_t magnetorquer_command_y;
	int16_t magnetorquer_command_z;
};

 /**
* @brief
*	Wheel speed commands
*/
struct __attribute__((packed)) adcs_wheel_speed_commands_t
{
	int16_t wheel_speed_x;
	int16_t wheel_speed_y;
	int16_t wheel_speed_z;
};

 /**
* @brief
*	Current SPG4 orbit parameters
*/
struct __attribute__((packed)) adcs_sgp4_orbit_parameters_t
{
	double inclination;
	double eccentricity;
	double right_ascension;
	double perigee;
	double b_star_drag_term;
	double mean_motion;
	double mean_anomaly;
	double epoch;
};

 /**
* @brief
*	Current configuration
*/
struct __attribute__((packed)) adcs_configuration_t
{
	uint8_t magnetorquer_1_configuration;
	uint8_t magnetorquer_2_configuration;
	uint8_t magnetorquer_3_configuration;
	uint16_t magnetorquer_max_x_dipole_moment;
	uint16_t magnetorquer_max_y_dipole_moment;
	uint16_t magnetorquer_max_z_dipole_moment;
	uint8_t magnetorquer_max_on_time;
	uint16_t magnetorquer_on_time_resolution;
	uint8_t magnetic_control_selection;
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	uint8_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
	uint8_t css1_config;
	uint8_t css2_config;
	uint8_t css3_config;
	uint8_t css4_config;
	uint8_t css5_config;
	uint8_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
	float y_momentum_control_gain;
	//float y_momentum_damping_gain;
	float y_momentum_nut_proportional_gain;
	float y_momentum_nut_derivative_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;

	/*
	 * Mask sensors usage contains 4 bool values. From least struct offset to most:
	 * 1. sun sensor
	 * 2. nadir sensor
	 * 3. coarse sun sensor
	 * 4. rate sensor
	 */
	uint8_t mask_senors_usage;
	/*uint8_t mask_sun_sensor; // actually is bool
	uint8_t mask_nadir_sensor; // actually is bool
	uint8_t mask_css; // actually is bool
	uint8_t mask_rate_sensor; // actually is bool*/

	uint8_t sun_and_nadir_sampling_period;

};

 /**
* @brief
*	Status of image capture and save operation
*/
struct __attribute__((packed)) adcs_status_of_image_capture_and_save_operation_t
{
	uint8_t complete;
	uint8_t status;
};

 /**
* @brief
*	Status of last uploaded program block
*/
struct __attribute__((packed)) adcs_uploaded_program_status_t
{
	uint8_t programming_completed; // actually is bool, and is 8 bits long
	uint16_t CRC16_checksum;

};

 /**
* @brief
*	Retrieve the list of sizes, checksums and descriptions for all programs in flash
*/
struct __attribute__((packed)) adcs_get_flash_program_list_t
{
	uint32_t program1_length;
	uint16_t program1_CRC16;
	uint8_t program1_valid;
	char program1_description[64];
	uint32_t program2_length;
	uint16_t program2_CRC16;
	uint8_t program2_valid;
	char program2_description[64];
	uint32_t program3_length;
	uint16_t program3_CRC16;
	uint8_t program3_valid;
	char program3_description[64];
	uint32_t program4_length;
	uint16_t program4_CRC16;
	uint8_t program4_valid;
	char program4_description[64];
	uint32_t program5_length;
	uint16_t program5_CRC16;
	uint8_t program5_valid;
	char program5_description[64];
	uint32_t program6_length;
	uint16_t program6_CRC16;
	uint8_t program6_valid;
	char program6_description[64];
	uint32_t program7_length;
	uint16_t program7_CRC16;
	uint8_t program7_valid;
	char program7_description[64];
	uint32_t int_flash_length;
	uint16_t int_flash_CRC16;
	uint8_t int_flash_valid;
	char int_flash_description[64];
};

 /**
* @brief
*	Perform a reset
*/
struct __attribute__((packed)) adcs_reset_t
{
	uint8_t reset;
};

 /**
* @brief
*	Set current unix time
*/
struct __attribute__((packed)) adcs_set_unix_time_t
{
	uint32_t current_unix_time;
	uint16_t millis;
};

 /**
* @brief
*	Set ADCS enabled state & control loop behaviour
*/
struct  __attribute__((packed)) adcs_adcs_run_mode_t
{
	uint8_t mode;
	/*
	 * 0-none
	 * 1-mems rate sensing
	 * 2-magnetometer rate sensing
	 * 3-both 1 & 2
	 * 4-full state ekf
	 * 5-mag with fine sun triad
	 */
};

 /**
* @brief
*	Selected which data should be logged
*/
struct __attribute__((packed)) adcs_selected_logged_data_t
{
	uint32_t log_selection;
	uint16_t log_period;
	uint8_t compress;
	char log_identifier[6];
};

 /**
* @brief
*	Control power to selected components
*/
struct __attribute__((packed)) adcs_power_control_t
{
	uint8_t cube_control_signal_power_selection;
	uint8_t cube_control_motor_power_selection;
	uint8_t cube_sense_power_selection;
	uint8_t cube_motor_power_selection;
	uint8_t gps_lna_power_selection;
};

 /**
* @brief
*	deploy magnetometer boom
*/
struct __attribute__((packed)) adcs_deploy_magnetometer_boom_t
{
	uint8_t timeout;
};

 /**
* @brief
*	Trigger ADCS to perform one iteration of the control loop (only valid when ADCS Run Mode is Triggered)
*/
struct __attribute__((packed)) adcs_trigger_adcs_loop_t
{

};

 /**
* @brief
*	Set attitude estimation mode
*/
struct __attribute__((packed)) adcs_set_attitude_estimation_mode_t
{
	uint8_t mode;
};

 /**
* @brief
*	Set attitude control mode
*/
struct __attribute__((packed)) adcs_set_attitude_control_mode_t
{
	uint8_t control_mode;
	uint8_t override; // actually is bool
	uint16_t timeout;
};

 /**
* @brief
*	Set commanded attitude angles
*/
struct __attribute__((packed)) adcs_set_commanded_attitude_angles_t
{
	uint16_t roll;
	uint16_t pitch;
	uint16_t yaw;
};

 /**
* @brief
*	Speed Set wheel speed (only valid if Control Mode is None)
*/
struct __attribute__((packed)) adcs_set_wheel_t
{
	uint16_t x;
	uint16_t y;
	uint16_t z;

};

 /**
* @brief
*	Set magnetorquer output (only valid if Control Mode is None)
*/
struct __attribute__((packed)) adcs_set_magnetorquer_output_t
{
	uint16_t x_duty_cycle;
	uint16_t y_duty_cycle;
	uint16_t z_duty_cycle;
};

 /**
* @brief
*	Select whether the ACP should automatically start Detumbling control at start-up
*/
struct __attribute__((packed)) adcs_set_start_up_mode_t
{
	uint8_t mode;
};

 /**
* @brief
*	Set SPG4 orbit parameters
*/
struct  __attribute__((packed)) adcs_set_sgp4_orbit_parameters_t
{
	double inclination;
	double eccentricity;
	double right_ascension;
	double perigee;
	double b_star_drag_term;
	double mean_motion;
	double mean_anomaly;
	double epoch;
};

 /**
* @brief
*	Set current configuration
*/
/*struct  __attribute__((packed)) adcs_set_configuration_t
{
	uint8_t magnetorquer_1_configuration;
	uint8_t magnetorquer_2_configuration;
	uint8_t magnetorquer_3_configuration;
	uint16_t magnetorquer_max_x_dipole_moment;
	uint16_t magnetorquer_max_y_dipole_moment;
	uint16_t magnetorquer_max_z_dipole_moment;
	uint8_t magnetorquer_max_on_time;
	uint16_t magnetorquer_on_time_resolution;
	uint8_t magnetic_control_selection;
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	uint8_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
	uint8_t css1_config;
	uint8_t css2_config;
	uint8_t css3_config;
	uint8_t css4_config;
	uint8_t css5_config;
	uint8_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
	float y_momentum_control_gain;
	float y_momentum_nutation_d_gain;
	float y_momentum_nutation_p_gain;
	//float y_momentum_damping_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;

	 *
	 * Mask sensors usage contains 4 bool values. From least struct offset to most:
	 * 1. sun sensor
	 * 2. nadir sensor
	 * 3. coarse sun sensor
	 * 4. rate sensor
	 *
	uint8_t mask_senors_usage;
	//uint8_t mask_sun_sensor; // actually is bool
	//uint8_t mask_nadir_sensor; // actually is bool
	//uint8_t mask_css; // actually is bool
	//uint8_t mask_rate_sensor; // actually is bool

	uint8_t sun_and_nadir_sampling_period;
};*/

 /**
* @brief
*	Set magnetorquer configuration parameters
*/
struct  __attribute__((packed)) adcs_set_magnetorquer_t
{
	int16_t x;
	int16_t y;
	int16_t z;
};

 /**
* @brief
*	Set wheel configuration parameters
*/
struct  __attribute__((packed)) adcs_set_wheel_configuration_t
{
	int16_t wheel_transform_angle_alpha;
	int16_t wheel_transform_angle_gamma;
	uint8_t wheel_polarity;
	uint16_t wheel_inertia;
	uint16_t wheel_max_torque;
	uint16_t wheel_max_speed;
	uint8_t wheel_control_gain;
	uint8_t wheel_backup_control_mode; // actually is bool
};

 /**
* @brief
*	Set photodiode pointing directions and scale factors
*/
struct  __attribute__((packed)) adcs_set_css_configuration_t
{
	uint8_t css1_config;
	uint8_t css2_config;
	uint8_t css3_config;
	uint8_t css4_config;
	uint8_t css5_config;
	uint8_t css6_config;
	uint8_t css1_rel_scale;
	uint8_t css2_rel_scale;
	uint8_t css3_rel_scale;
	uint8_t css4_rel_scale;
	uint8_t css5_rel_scale;
	uint8_t css6_rel_scale;
	uint8_t css_autofill_missing_facets; // actually is bool
	uint8_t css_threshold;
};

 /**
* @brief
*	Set sun sensor configuration parameters
*/
struct  __attribute__((packed)) adcs_set_sun_sensor_configuration_t
{
	int16_t sun_sensor_transform_angle_alpha;
	int16_t sun_sensor_transform_angle_beta;
	int16_t sun_sensor_transform_angle_gamma;
	uint8_t sun_detection_threshold;
	uint8_t sun_sensor_auto_adjust_mode; // actually is bool
	uint8_t sun_sensor_exposure_time;
	uint8_t sun_sensor_AGC;
	uint8_t sun_blue_gain;
	uint8_t sun_red_gain;
	uint16_t sun_boresight_x;
	uint16_t sun_boresight_y;
	uint8_t sun_shift; // actually is bool
};

 /**
* @brief
*	Set nadir sensor configuration parameters
*/
struct  __attribute__((packed)) adcs_set_nadir_sensor_configuration_t
{
	int16_t nadir_sensor_transform_angle_alpha;
	int16_t nadir_sensor_transform_angle_beta;
	int16_t nadir_sensor_transform_angle_gamma;
	uint8_t nadir_detection_threshold;
	uint8_t nadir_sensor_auto_adjust; // actually is bool
	uint8_t nadir_sensor_exposure_time;
	uint8_t nadir_sensor_AGC;
	uint8_t nadir_blue_gain;
	uint8_t nadir_red_gain;
	uint16_t nadir_boresight_x;
	uint16_t nadir_boresight_y;
	uint8_t nadir_shift; // actually is bool
	uint16_t min_x_of_area_1;
	uint16_t max_x_of_area_1;
	uint16_t min_y_of_area_1;
	uint16_t max_y_of_area_1;
	uint16_t min_x_of_area_2;
	uint16_t max_x_of_area_2;
	uint16_t min_y_of_area_2;
	uint16_t max_y_of_area_2;
	uint16_t min_x_of_area_3;
	uint16_t max_x_of_area_3;
	uint16_t min_y_of_area_3;
	uint16_t max_y_of_area_3;
	uint16_t min_x_of_area_4;
	uint16_t max_x_of_area_4;
	uint16_t min_y_of_area_4;
	uint16_t max_y_of_area_4;
	uint16_t min_x_of_area_5;
	uint16_t max_x_of_area_5;
	uint16_t min_y_of_area_5;
	uint16_t max_y_of_area_5;
};

 /**
* @brief
*	Set magnetometer configuration parameters
*/
struct  __attribute__((packed)) adcs_set_magnetometer_configuration_t
{
	int16_t magnetometer_transform_angle_alpha;
	int16_t magnetometer_transform_angle_beta;
	int16_t magnetometer_transform_angle_gamma;
	int16_t magnetometer_channel1_offset;
	int16_t magnetometer_channel2_offset;
	int16_t magnetometer_channel3_offset;
	int16_t magnetometer_sens_mat_s11;
	int16_t magnetometer_sens_mat_s12;
	int16_t magnetometer_sens_mat_s13;
	int16_t magnetometer_sens_mat_s21;
	int16_t magnetometer_sens_mat_s22;
	int16_t magnetometer_sens_mat_s23;
	int16_t magnetometer_sens_mat_s31;
	int16_t magnetometer_sens_mat_s32;
	int16_t magnetometer_sens_mat_s33;
};

 /**
* @brief
*	Set Rate Sensor Configuration
*/
struct  __attribute__((packed)) adcs_set_rate_sensor_configuration_t
{
	int16_t y_rate_sensor_offset;
	int16_t y_rate_sensor_transform_angle_alpha;
	int16_t y_rate_sensor_transform_angle_gamma;
};

 /**
* @brief
*	Set controller gains and reference values for Detumbling control mode
*/
struct  __attribute__((packed)) adcs_set_detumbling_control_parameters_t
{
	float detumbling_spin_gain;
	float detumbling_damping_gain;
	int16_t ref_spin_rate;
};

 /**
* @brief
*	Set controller gains and reference values for Y-Momentum control mode
*/
struct  __attribute__((packed)) adcs_set_y_momentum_control_parameters_t
{
	float y_momentum_control_gain;
	//float y_momentum_damping_gain;
	float y_momentum_nutation_d_gain;
	float y_momentum_nutation_p_gain;
	float y_momentum_proportional_gain;
	float y_momentum_derivative_gain;
	float ref_wheel_momentum;
	float ref_wheel_momentum_init_pitch;
	uint16_t ref_wheel_torque_init_pitch;
};

 /**
* @brief
*	Set satellite moment of inertia tensor
*/
struct  __attribute__((packed)) adcs_set_moment_of_inertia_t
{
	float moment_of_inertia_xx;
	float moment_of_inertia_yy;
	float moment_of_inertia_zz;
	float moment_of_inertia_xy;
	float moment_of_inertia_xz;
	float moment_of_inertia_yz;
};

 /**
* @brief
*	Set estimation noise covariance and sensor mask
*/
struct  __attribute__((packed)) adcs_set_estimation_parameters_t
{
	float magnetometer_rate_filter_system_noise;
	float EKF_system_noise;
	float css_meas_noise;
	float sun_sensor_meas_noise;
	float nadir_sensor_meas_noise;
	float magnetometer_sensor_meas_noise;

	/*
	 * Mask sensors usage contains 3 bool values. From least struct offset to most:
	 * 1. sun sensor
	 * 2. nadir sensor
	 * 3. coarse sun sensor
	 */
	uint8_t mask_senors_usage;
	/*uint8_t mask_sun_sensor; // actually is bool
	uint8_t mask_nadir_sensor; // actually is bool
	uint8_t mask_css; // actually is bool*/

	uint8_t sun_and_nadir_sampling_period;
};

 /**
* @brief
*	Save current configuration to flash memory
*/
struct  __attribute__((packed)) adcs_save_configuration_t
{

};

 /**
* @brief
*	Save current orbit parameters to flash memory
*/
struct  __attribute__((packed)) adcs_save_orbit_parameters_t
{

};

 /**
* @brief
*	Capture and save an image from one of the CubeSense cameras to SD card
*/
struct  __attribute__((packed)) adcs_capture_and_save_image_t
{
	uint8_t percentage_complete;
	uint8_t status;
};

 /**
* @brief
*	Reset file list
*/
struct  __attribute__((packed)) adcs_reset_file_list_t
{

};

 /**
* @brief
*	Advance file list index
*/
struct  __attribute__((packed)) adcs_advance_file_list_index_t
{

};

 /**
* @brief
*	Initialize file download
*/
struct  __attribute__((packed)) adcs_initialize_file_download_t
{
	char	 filename [13];
};

 /**
* @brief
*	Advance file read pointer
*/
struct  __attribute__((packed)) adcs_advance_file_read_pointer_t
{
	uint16_t block;
};

 /**
* @brief
*	Erase file
*/
struct  __attribute__((packed)) adcs_erase_file_t
{
	char	 filename[13];
};

 /**
* @brief
*	Erase all files
*/
struct  __attribute__((packed)) adcs_erase_all_files_t
{

};

 /**
* @brief
*	Set the current boot program index
*/
struct  __attribute__((packed)) adcs_set_boot_index_t
{
	uint8_t	 index;
};

 /**
* @brief
*	Erase program at specified index
*/
struct  __attribute__((packed)) adcs_erase_program_t
{
	uint8_t	 boot_index;
};

 /**
* @brief
*	Upload a 256-byte block of program code to flash
*/
struct  __attribute__((packed)) adcs_upload_program_block_t
{
	uint8_t program_index;
	uint16_t block_number;
	uint8_t data [256];
};

 /**
* @brief
*	Finalize program upload
*/
struct  __attribute__((packed)) adcs_finalize_program_upload_t
{
	uint8_t boot_index;
	uint32_t program_length;
	uint16_t checksum;
	char prgram_description[64];
};

/**
* @brief
*	Gets programmed boot index and boot status
*/
struct  __attribute__((packed)) adcs_boot_index_t
{
	uint8_t program_index;
	uint8_t boot_status;
};

/**
* @brief
*	Resets the ADCS MCU.
*/
struct  __attribute__((packed)) adcs_reset_mcu_t
{

};

/**
* @brief
*	Runs the current program index immediately.
*/
struct  __attribute__((packed)) adcs_run_program_now_t
{

};

/**
* @brief
*	Copy indicated program index to internal flash.
*/
struct  __attribute__((packed)) adcs_copy_to_internal_flash_t
{
	uint8_t source_index;
	uint8_t NOT_USED;
};

/**
* @brief
*	Resets indicators like boot cause, boot count, etc.
*/
struct  __attribute__((packed)) adcs_reset_boot_statistics_t
{

};

/**
* @brief
*	Bootloader status if main program can't load.
*	REPLACES COMMAND 136 ON BOOTLOADER ONLY.
*/
struct __attribute__((packed)) adcs_bootloader_status_t
{
	uint16_t up_time;
	uint32_t unix_time;
	uint8_t flags;
};

/** Detumble command options for adcs_t::command_detumble( ) */
typedef enum
{
	ADCS_START_DETUMBLE = 0,		/*!< (0) Start detumbling, no effect if already detumbling. */
	ADCS_STOP_DETUMBLE				/*!< (1) Stop detumbling, no effect if already stopped. */
} adcs_detumbe_command_enum_t;


/**
 * @struct adcs_t
 * @brief
 *		A structure used for communicating with the attitude determination and control system.
 *
 * @details
 * 		A structure used for communicating with the ADCS module.
 * 		Each instance of this structure can support IO with exactly one
 * 		ADCS and must be properly initialized. For example:
 * 			@code
 * 			adcs_t surrey_adcs, linux_adcs;
 *
 * 			initialize_adcs_surrey( &surrey_adcs );
 * 			initialize_adcs_linux( &linux_adcs );
 * 			@endcode
 * 		In this example, the adcs_t structures are initialized to do IO with
 * 		gomespace's nano power and a linux mock up of a power board.
 *
 * 		Multiple instance of this structure must not be initialized to do
 * 		IO with the same power board. This will result in undefined behavior.
 *
 * 		All variables inside the adcs_t structure must be treated as private.
 * 		They must never be accessed directly.
 *
 * @attention
 * 		Multiple instance of this structure must not be initialized to do
 * 		IO with the ADCS. This will result in undefined behavior.
 *
 *
 */
struct adcs_t
{
#ifndef __LPC17XX__
	adcs_identification_t identification;
	adcs_extended_identification_t extended_identification;
	adcs_communication_status_t communication_status;
	adcs_telecommand_acknowledge_t telecommand_acknowledge;
	//adcs_reset_cause_t reset_cause;
	adcs_actuator_commands_t actuator_commands;
	adcs_acp_execution_state_t acp_execution_state;
	adcs_acp_execution_times_t acp_execution_times;
	adcs_edac_and_latchup_counters_t edac_and_latchup_counters;
	adcs_start_up_mode_t start_up_mode;
	adcs_file_information_t file_information;
	adcs_file_block_crc_t file_block_crc;
	adcs_file_data_block_t file_data_block;
	adcs_power_control_selection_t power_control_selection;
	adcs_power_and_temperature_measurements_t power_and_temperature_measurements;
#endif
	adcs_adcs_state_t adcs_state;
#ifndef __LPC17XX__
	adcs_adcs_measurements_t adcs_measurements;
	adcs_current_time_t current_time;
	adcs_current_state_t current_state;
	adcs_estimated_attitude_angles_t estimated_attitude_angles;
	adcs_estimated_angular_rates_t estimated_angular_rates;
	adcs_satellite_position_llh_t satellite_position_llh;
	adcs_satellite_velocity_eci_t satellite_velocity_eci;
	adcs_magnetic_field_vector_t magnetic_field_vector;
	adcs_coarse_sun_vector_t coarse_sun_vector;
	adcs_fine_sun_vector_t fine_sun_vector;
	adcs_nadir_vector_t nadir_vector;
	adcs_rate_sensor_rates_t rate_sensor_rates;
	adcs_wheel_speed_t wheel_speed;
	adcs_cubesense_current_t cubesense_current;
	adcs_cubecontrol_current_t cubecontrol_current;
	adcs_peripheral_current_and_temperature_measurements_t peripheral_current_and_temperature_measurements;
	adcs_raw_sensor_measurements_t raw_sensor_measurements;
	adcs_angular_rate_covariance_t angular_rate_covariance;
	adcs_raw_nadir_sensor_t raw_nadir_sensor;
	adcs_raw_sun_sensor_t raw_sun_sensor;
	adcs_raw_css_t raw_css;
	adcs_raw_magnetometer_t raw_magnetometer;
	adcs_raw_gps_status_t raw_gps_status;
	adcs_raw_gps_time_t raw_gps_time;
	adcs_raw_gps_x_t raw_gps_x;
	adcs_raw_gps_y_t raw_gps_y;
	adcs_raw_gps_z_t raw_gps_z;
	adcs_estimation_data_t estimation_data;
	adcs_igrf_modelled_magnetic_field_vector_t igrf_modelled_magnetic_field_vector;
	adcs_modelled_sun_vector_t modelled_sun_vector;
	adcs_estimated_gyro_bias_t estimated_gyro_bias;
	adcs_estimation_innovation_vector_t estimation_innovation_vector;
	adcs_quaternion_error_vector_t quaternion_error_vector;
	adcs_quaternion_covariance_t quaternion_covariance;
	adcs_magnetorquer_command_t magnetorquer_command;
	adcs_wheel_speed_commands_t wheel_speed_commands;
	adcs_sgp4_orbit_parameters_t sgp4_orbit_parameters;
	adcs_configuration_t configuration;
	adcs_status_of_image_capture_and_save_operation_t status_of_image_capture_and_save_operation;
	adcs_uploaded_program_status_t uploaded_program_status;
	adcs_get_flash_program_list_t get_flash_program_list;
	adcs_reset_t reset;
	adcs_set_unix_time_t set_unix_time;
	adcs_adcs_run_mode_t adcs_run_mode;
	adcs_selected_logged_data_t selected_logged_data;
	adcs_power_control_t power_control;
	adcs_deploy_magnetometer_boom_t deploy_magnetometer_boom;
	adcs_trigger_adcs_loop_t trigger_adcs_loop;
	adcs_set_attitude_estimation_mode_t set_attitude_estimation_mode;
	adcs_set_attitude_control_mode_t set_attitude_control_mode;
	adcs_set_commanded_attitude_angles_t set_commanded_attitude_angles;
	adcs_set_wheel_t set_wheel;
	adcs_set_magnetorquer_output_t set_magnetorquer_output;
	adcs_set_start_up_mode_t set_start_up_mode;
	adcs_set_sgp4_orbit_parameters_t set_sgp4_orbit_parameters;
	//adcs_set_configuration_t set_configuration;
	adcs_set_magnetorquer_t set_magnetorquer;
	adcs_set_wheel_configuration_t set_wheel_configuration;
	adcs_set_css_configuration_t set_css_configuration;
	adcs_set_sun_sensor_configuration_t set_sun_sensor_configuration;
	adcs_set_nadir_sensor_configuration_t set_nadir_sensor_configuration;
	adcs_set_magnetometer_configuration_t set_magnetometer_configuration;
	adcs_set_rate_sensor_configuration_t set_rate_sensor_configuration;
	adcs_set_detumbling_control_parameters_t set_detumbling_control_parameters;
	adcs_set_y_momentum_control_parameters_t set_y_momentum_control_parameters;
	adcs_set_moment_of_inertia_t set_moment_of_inertia;
	adcs_set_estimation_parameters_t set_estimation_parameters;
	adcs_save_configuration_t save_configuration;
	adcs_save_orbit_parameters_t save_orbit_parameters;
	adcs_capture_and_save_image_t capture_and_save_image;
	adcs_reset_file_list_t reset_file_list;
	adcs_advance_file_list_index_t advance_file_list_index;
	adcs_initialize_file_download_t initialize_file_download;
	adcs_advance_file_read_pointer_t advance_file_read_pointer;
	adcs_erase_file_t erase_file;
	adcs_erase_all_files_t erase_all_files;
	adcs_set_boot_index_t set_boot_index;
	adcs_erase_program_t erase_program;
	adcs_upload_program_block_t upload_program_block;
	adcs_finalize_program_upload_t finalize_program_upload;
	adcs_boot_index_t boot_index;
	adcs_reset_mcu_t reset_mcu;
	adcs_run_program_now_t run_program_now;
	adcs_copy_to_internal_flash_t copy_to_internal_flash;
	adcs_reset_boot_statistics_t reset_boot_statistics;
	adcs_bootloader_status_t bootloader_status_t;



	int (* getframe_identification) (adcs_t *);
	int (*getframe_extended_identification) (adcs_t *);
	int (* getframe_communication_status) (adcs_t *);
	int (* getframe_telecommand_acknowledge) (adcs_t *);
	//int (* getframe_reset_cause) (adcs_t *);
	int (* getframe_actuator_commands) (adcs_t *);
	int (* getframe_acp_execution_state) (adcs_t *);
	int (* getframe_acp_execution_times) (adcs_t *);
	int (* getframe_edac_and_latchup_counters) (adcs_t *);
	int (* getframe_start_up_mode) (adcs_t *);
	int (* getframe_file_information) (adcs_t *);
	int (* getframe_file_block_crc) (adcs_t *);
	int (* getframe_file_data_block) (adcs_t *);
	int (* getframe_power_control_selection) (adcs_t *);
	int (* getframe_power_and_temperature_measurements) (adcs_t *);
#endif
	int (* getframe_adcs_state) (adcs_t *);
	bool_t (*detumble_command)( adcs_t*, adcs_detumbe_command_enum_t );
	bool_t (*power)( adcs_t*, eps_t*, bool_t );
	bool_t (*gps_power)(adcs_t*, eps_t*, bool_t);
#ifndef __LPC17XX__
	int (* getframe_adcs_measurements) (adcs_t *);
	int (* getframe_current_time) (adcs_t *);
	int (* getframe_current_state) (adcs_t *);
	int (* getframe_estimated_attitude_angles) (adcs_t *);
	int (* getframe_estimated_angular_rates) (adcs_t *);
	int (* getframe_satellite_position_llh) (adcs_t *);
	int (* getframe_satellite_velocity_eci) (adcs_t *);
	int (* getframe_magnetic_field_vector) (adcs_t *);
	int (* getframe_coarse_sun_vector) (adcs_t *);
	int (* getframe_fine_sun_vector) (adcs_t *);
	int (* getframe_nadir_vector) (adcs_t *);
	int (* getframe_rate_sensor_rates) (adcs_t *);
	int (* getframe_wheel_speed) (adcs_t *);
	int (* getframe_cubesense_current) (adcs_t *);
	int (* getframe_cubecontrol_current) (adcs_t *);
	int (* getframe_peripheral_current_and_temperature_measurements) (adcs_t *);
	int (* getframe_raw_sensor_measurements) (adcs_t *);
	int (* getframe_angular_rate_covariance) (adcs_t *);
	int (* getframe_raw_nadir_sensor) (adcs_t *);
	int (* getframe_raw_sun_sensor) (adcs_t *);
	int (* getframe_raw_css) (adcs_t *);
	int (* getframe_raw_magnetometer) (adcs_t *);
	int (* getframe_raw_gps_status) (adcs_t *);
	int (* getframe_raw_gps_time) (adcs_t *);
	int (* getframe_raw_gps_x) (adcs_t *);
	int (* getframe_raw_gps_y) (adcs_t *);
	int (* getframe_raw_gps_z) (adcs_t *);
	int (* getframe_estimation_data) (adcs_t *);
	int (* getframe_igrf_modelled_magnetic_field_vector) (adcs_t *);
	int (* getframe_modelled_sun_vector) (adcs_t *);
	int (* getframe_estimated_gyro_bias) (adcs_t *);
	int (* getframe_estimation_innovation_vector) (adcs_t *);
	int (* getframe_quaternion_error_vector) (adcs_t *);
	int (* getframe_quaternion_covariance) (adcs_t *);
	int (* getframe_magnetorquer_command) (adcs_t *);
	int (* getframe_wheel_speed_commands) (adcs_t *);
	int (* getframe_sgp4_orbit_parameters) (adcs_t *);
	int (* getframe_configuration) (adcs_t *);
	int (* getframe_status_of_image_capture_and_save_operation) (adcs_t *);
	int (* getframe_uploaded_program_status) (adcs_t *);
	int (* getframe_get_flash_program_list) (adcs_t *);
	int (* getframe_boot_index) (adcs_t *);

	int (* setframe_reset) (adcs_t *, adcs_reset_t *);
	int (* setframe_set_unix_time) (adcs_t *, adcs_set_unix_time_t *);
	int (* setframe_adcs_run_mode) (adcs_t *, adcs_adcs_run_mode_t *);
	int (* setframe_selected_logged_data) (adcs_t *, adcs_selected_logged_data_t *);
	int (* setframe_power_control) (adcs_t *, adcs_power_control_t *);
	int (* setframe_deploy_magnetometer_boom) (adcs_t *, adcs_deploy_magnetometer_boom_t *);
	int (* setframe_trigger_adcs_loop) (adcs_t *, adcs_trigger_adcs_loop_t *);
	int (* setframe_set_attitude_estimation_mode) (adcs_t *, adcs_set_attitude_estimation_mode_t *);
	int (* setframe_set_attitude_control_mode) (adcs_t *, adcs_set_attitude_control_mode_t *);
	int (* setframe_set_commanded_attitude_angles) (adcs_t *, adcs_set_commanded_attitude_angles_t *);
	int (* setframe_set_wheel) (adcs_t *, adcs_set_wheel_t *);
	int (* setframe_set_magnetorquer_output) (adcs_t *, adcs_set_magnetorquer_output_t *);
	int (* setframe_set_start_up_mode) (adcs_t *, adcs_set_start_up_mode_t *);
	int (* setframe_set_sgp4_orbit_parameters) (adcs_t *, adcs_set_sgp4_orbit_parameters_t *);
	int (* setframe_set_configuration) (adcs_t *, adcs_configuration_t *);
	int (* setframe_set_magnetorquer) (adcs_t *, adcs_set_magnetorquer_t *);
	int (* setframe_set_wheel_configuration) (adcs_t *, adcs_set_wheel_configuration_t *);
	int (* setframe_set_css_configuration) (adcs_t *, adcs_set_css_configuration_t *);
	int (* setframe_set_sun_sensor_configuration) (adcs_t *, adcs_set_sun_sensor_configuration_t *);
	int (* setframe_set_nadir_sensor_configuration) (adcs_t *, adcs_set_nadir_sensor_configuration_t *);
	int (* setframe_set_magnetometer_configuration) (adcs_t *, adcs_set_magnetometer_configuration_t *);
	int (* setframe_set_rate_sensor_configuration) (adcs_t *, adcs_set_rate_sensor_configuration_t *);
	int (* setframe_set_detumbling_control_parameters) (adcs_t *, adcs_set_detumbling_control_parameters_t *);
	int (* setframe_set_y_momentum_control_parameters) (adcs_t *, adcs_set_y_momentum_control_parameters_t *);
	int (* setframe_set_moment_of_inertia) (adcs_t *, adcs_set_moment_of_inertia_t *);
	int (* setframe_set_estimation_parameters) (adcs_t *, adcs_set_estimation_parameters_t *);
	int (* setframe_save_configuration) (adcs_t *, adcs_save_configuration_t *);
	int (* setframe_save_orbit_parameters) (adcs_t *, adcs_save_orbit_parameters_t *);
	int (* setframe_capture_and_save_image) (adcs_t *, adcs_capture_and_save_image_t *);
	int (* setframe_reset_file_list) (adcs_t *, adcs_reset_file_list_t *);
	int (* setframe_advance_file_list_index) (adcs_t *, adcs_advance_file_list_index_t *);
	int (* setframe_initialize_file_download) (adcs_t *, adcs_initialize_file_download_t *);
	int (* setframe_advance_file_read_pointer) (adcs_t *, adcs_advance_file_read_pointer_t *);
	int (* setframe_erase_file) (adcs_t *, adcs_erase_file_t *);
	int (* setframe_erase_all_files) (adcs_t *, adcs_erase_all_files_t *);
	int (* setframe_set_boot_index) (adcs_t *, adcs_set_boot_index_t *);
	int (* setframe_erase_program) (adcs_t *, adcs_erase_program_t *);
	int (* setframe_upload_program_block) (adcs_t *, adcs_upload_program_block_t *);
	int (* setframe_finalize_program_upload) (adcs_t *, adcs_finalize_program_upload_t *);
	int (* setframe_reset_mcu) (adcs_t *);
	int (* setframe_run_program_now) (adcs_t *);
	int (* setframe_copy_to_internal_flash) (adcs_t *, adcs_copy_to_internal_flash_t *);
	int (* setframe_reset_boot_statistics) (adcs_t *);
#endif
};

/**
 * Possible ADCS detumbling states.
 */
typedef enum
{
	ADCS_NO_CONTROL = 0, 		/*!< (0) No control is being exerted. */
	ADCS_DETUMBLING_CONTROL, 	/*!< (1) Control to detumble. */
	ADCS_Y_INIT_PITCH,			/*!< (2) Control to change pitch, done detumbling. */
	ADCS_Y_STEADY,				/*!< (3) Maintaining current pitch, done detumbling. */
	ADCS_Y_STEADY_NO_CONTROL,	/*!< (4) Pitch still good but no control. */
	ADCS_UNKNOWN_STATE			/*!< (5) State of adcs could not be determined. */
} adcs_state_enum_t;
adcs_state_enum_t adcs_get_detumbling_state( adcs_t* );

/**
 * @brief
 * 		Initialize an adcs_t structure to do IO with an __LPC17XX__ ADCS board
 * 		mock up.
 *
 * @details
 * 		Initialize an adcs_t structure to do IO with an __LPC17XX__ ADCS board
 * 		mock up. This mock up exists on the same board as the executing
 * 		software - there is no physical IO.
 *
 * @param adcs
 * 		A pointer to the adcs_t structure to initialize.
 *
 * @memberof adsc_t
 */
void initialize_adcs_lpc_local( adcs_t *adcs );

/**
 * @brief
 * 		Initialize an adcs_t structure to do IO with surry adcs.
 *
 * @details
 * 		Initialize an adcs_t structure to do IO with surry adcs.
 *
 * @param adcs
 * 		A pointer to the adcs_t structure to initialize.
 *
 * @memberof adsc_t
 */
void initialize_adcs_nanomind( adcs_t *adcs );

/**
 * @memberof adcs_t
 * @brief
 * 		Destroy and adcs_t structure when no longer needed.
 * @details
 * 		Destroy and adcs_t structure when no longer needed.
 * @param adcs[in]
 * 		The adcs structure to destroy.
 */
void destroy_adcs( adcs_t *adcs );

int adcs_get(uint8_t cmd, uint8_t *input, uint32_t size);
int adcs_set(uint8_t *output, uint32_t size);


struct __attribute__((packed)) adcs_location_vectors_t
{
	adcs_satellite_position_llh_t 	llh_position;
	adcs_nadir_vector_t				nadir_vector;
};
void adcs_get_location_vectors( struct adcs_location_vectors_t* vecs );
void vTaskOrbitalProp(void *pvParameters);

#endif /* ADCS_H_ */
