/*
 * adcs_locale_logging.h
 *
 *  Created on: Jun 16, 2016
 *      Author: bbruner
 */

#ifndef LIB_LIBCORE_INCLUDE_ADCS_ADCS_LOCALE_LOGGER_H_
#define LIB_LIBCORE_INCLUDE_ADCS_ADCS_LOCALE_LOGGER_H_

#include <portable_types.h>

bool_t init_adcs_locale_logger( );
uint32_t adcs_locale_logger_set_cadence( uint32_t );
bool_t adcs_locale_logger_enable( );
bool_t adcs_locale_logger_disable( );
bool_t adcs_locale_logger_is_enabled( );
bool_t adcs_locale_logger_commission( );
bool_t adcs_locale_logger_decommission( );
bool_t adcs_locale_logger_is_commissioned( );

#endif /* LIB_LIBCORE_INCLUDE_ADCS_ADCS_LOCALE_LOGGER_H_ */
