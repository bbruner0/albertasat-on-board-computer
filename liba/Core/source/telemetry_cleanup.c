/*
 * telemetry_cleanup.c
 *
 *  Created on: Jun 29, 2016
 *      Author: bbruner
 */

#include <telemetry_cleanup.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <inttypes.h>
#include <mnlp.h>
#include <unistd.h>

#define TELEMETRY_CLEANUP_COMMAND_QUEUE_DEPTH 5
#define TELEMETRY_CLEANUP_COMMAND_QUEUE_SIZE sizeof(uint32_t)

#define TELEMETRY_CLEANUP_TASK_NAME (signed char*) "telemetry cleanup"
#define TELEMETRY_CLEANUP_TASK_STACK (4*1024)
#define TELEMETRY_CLEANUP_TASK_PRIO (BASE_PRIORITY)
#define TELEMETRY_CLEANUP_TASK_DELAY_MS 100

struct telemetry_cleanup_t
{
	queue_t command_queue;
	logger_t* dfgm_firA_log;
	logger_t* dfgm_firB_log;
	logger_t* dfgm_raw_log;
	logger_t* wod_log;
	logger_t* dfgm_hk_log;
	logger_t* athena_log;
	filesystem_t* fs;
};

extern driver_toolkit_nanomind_t drivers;
static struct telemetry_cleanup_t self;

static void telemetry_cleanup_daemon( void* );

bool_t telemetry_cleanup_all( uint32_t epoch )
{
	if( xQueueSendToBack(self.command_queue, &epoch, USE_POLLING) != pdTRUE ) {
		return false;
	}
	return true;
}

bool_t telemetry_cleanup_init( )
{
	new_queue(self.command_queue, TELEMETRY_CLEANUP_COMMAND_QUEUE_DEPTH, TELEMETRY_CLEANUP_COMMAND_QUEUE_SIZE);
	if( self.command_queue == NULL ) {
		return false;
	}

	if( xTaskCreate(telemetry_cleanup_daemon, TELEMETRY_CLEANUP_TASK_NAME, TELEMETRY_CLEANUP_TASK_STACK, NULL, TELEMETRY_CLEANUP_TASK_PRIO, NULL) != pdPASS ) {
		return false;
	}

	self.dfgm_firA_log = drivers.s_.dfgm_filt1_logger;
	self.dfgm_firB_log = drivers.s_.dfgm_filt2_logger;
	self.dfgm_raw_log = drivers.s_.dfgm_raw_logger;
	self.dfgm_hk_log = drivers.s_.dfgm_hk_logger;
	self.wod_log = drivers.s_.wod_logger;
	self.athena_log = drivers.s_.athena_logger;
	self.fs = drivers.s_.fs;

	return true;
}

static void telemetry_cleanup_dfgm_raw( logger_t* logger, uint32_t time_origin )
{
	logger_error_t 	lerr;
	size_t			i;
	char			name[FILESYSTEM_MAX_NAME_LENGTH+1];

	for( i = 0; i < LOGGER_MAX_CAPACITY; ++i ) {
		/* Get name of file to remove. */
		lerr = logger_pop(logger, name);
		if( lerr == LOGGER_OK ) {
			/* Popped correctly, remove the file. */
			self.fs->delete(self.fs, name);
		}
		else if( lerr == LOGGER_EMPTY ) {
			/* No more files to remove. */
			break;
		}
	}
}


/* These are functions attri used to generate date for file names.
 * So, reuse for sake of consistency I guess.
 */
extern void mnlp_getdate(int*,int*,int*,uint32_t);
static void telemetry_cleanup_mnlp( uint32_t time_origin )
{
	uint32_t	qb50_time_origin;
	char		file_name[FILESYSTEM_MAX_NAME_LENGTH+1];
	char		sd_file_path[2*(FILESYSTEM_MAX_NAME_LENGTH+1)];
	char		boot_file_path[2*(FILESYSTEM_MAX_NAME_LENGTH+1)];
	int 		end_year, end_month, end_day;
	int			year, month, day;

	/* First, convert to QB50 epoch. */
	qb50_time_origin = time_origin - QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH;

	/* Now convert that to a date. */
	mnlp_getdate(&end_year, &end_month, &end_day, qb50_time_origin);
	if( end_year > 2020 ) {
		/* Satellite will be dead by 2020. */
		end_year = 2020;
	}

	/* Now, loop over all possible files. */
	csp_printf("telemetry cleanup: diag: removing mnlp data from before\n\tyear: %d\n\tmonth: %d\n\tday: %d", end_year, end_month, end_day);
	for( year = 2000; year <= end_year; ++year ) {
		for( month = 0; month <= end_month; ++month ) {
			for( day = 0; day <= end_day; ++day ) {
				/* HK files. */
				snprintf(file_name, FILESYSTEM_MAX_NAME_LENGTH+1, "M%02d%02d%02dH.bin", year % 100, month, day);
				snprintf(sd_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/sd/%s", file_name);
				snprintf(boot_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/boot/%s", file_name);
				if( access(sd_file_path, F_OK) != -1 ) {
					remove(sd_file_path);
				}
				if( access(boot_file_path, F_OK) != -1 ) {
					remove(boot_file_path);
				}

				/* Science files. */
				snprintf(file_name, FILESYSTEM_MAX_NAME_LENGTH+1, "M%02d%02d%02dS.bin", year % 100, month, day);
				snprintf(sd_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/sd/%s", file_name);
				snprintf(boot_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/boot/%s", file_name);
				if( access(sd_file_path, F_OK) != -1 ) {
					remove(sd_file_path);
				}
				if( access(boot_file_path, F_OK) != -1 ) {
					remove(boot_file_path);
				}

				/* Entertaining files. */
				snprintf(file_name, FILESYSTEM_MAX_NAME_LENGTH+1, "M%02d%02d%02dE.bin", year % 100, month, day);
				snprintf(sd_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/sd/%s", file_name);
				snprintf(boot_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/boot/%s", file_name);
				if( access(sd_file_path, F_OK) != -1 ) {
					remove(sd_file_path);
				}
				if( access(boot_file_path, F_OK) != -1 ) {
					remove(boot_file_path);
				}

				/* Derp files. */
				snprintf(file_name, FILESYSTEM_MAX_NAME_LENGTH+1, "M%02d%02d%02dD.bin", year % 100, month, day);
				snprintf(sd_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/sd/%s", file_name);
				snprintf(boot_file_path, 2*(FILESYSTEM_MAX_NAME_LENGTH+1), "/boot/%s", file_name);
				if( access(sd_file_path, F_OK) != -1 ) {
					remove(sd_file_path);
				}
				if( access(boot_file_path, F_OK) != -1 ) {
					remove(boot_file_path);
				}
			}
		}
		csp_printf("telemetry cleanup: diag: mnlp cleanup for year %d done", year);
	}
	csp_printf("telemetry cleanup: diag: mnlp cleanup done all");
}

static void telemetry_cleanup_log( logger_t* logger, uint32_t time_origin )
{
	file_t* 		file;
	logger_error_t 	lerr;
	fs_error_t 		ferr;
	uint32_t		bytes_read;
	size_t			i;
	char			name[FILESYSTEM_MAX_NAME_LENGTH+1];
	uint32_t		time_stamp;

	for( i = 0; i < LOGGER_MAX_CAPACITY; ++i ) {
		/* Get file at tail. */
		file = logger_peek_tail(logger, &lerr);

		/* only proceed if peek was successful. */
		if( lerr == LOGGER_OK ) {

			/* Get time stamp on telemetry. */
			ferr = file->read(file, (uint8_t*) &time_stamp, sizeof(time_stamp), &bytes_read);
			file->close(file);

			/* Check if telemetry exceeds time origin. */
			csp_printf("\nread time stamp:\t%" PRIu32 "\ncomparing to:\t\t%" PRIu32 "\nduring iteration %zu\nfile name: %s", time_stamp, time_origin, i, fs_handle_get_name((fs_handle_t*) file));
			if( ferr == FS_OK && time_stamp <= time_origin ) {
				/* File needs to be removed. */
				lerr = logger_pop(logger, name);
				csp_printf("Popped with err: %d", lerr);
				if( lerr == LOGGER_OK ) {
					/* Popped correctly, remove the file. */
					self.fs->delete(self.fs, name);
				}
				else if( lerr == LOGGER_EMPTY ) {
					csp_printf("cleanup exit at iteration: %zu", i);
					break;
				}
			}
			else {
				/* Do not need to remove. */
				csp_printf("cleanup exit at iteration: %zu", i);
				break;
			}
		}
		else if( lerr == LOGGER_EMPTY ) {
			/* No more files in logger, end loop. */
			csp_printf("cleanup exit at iteration: %zu", i);
			break;
		}
	}
}

static void telemetry_cleanup_daemon( void* args )
{
	uint32_t time_origin;
	for( ;; ) {

		if( xQueueReceive(self.command_queue, &time_origin, portMAX_DELAY) == pdTRUE ) {
			/* Got a command to cleanup telemetry. */
			telemetry_cleanup_log(self.dfgm_firA_log, time_origin);
			telemetry_cleanup_log(self.dfgm_firB_log, time_origin);
			telemetry_cleanup_dfgm_raw(self.dfgm_raw_log, time_origin);
			telemetry_cleanup_log(self.dfgm_hk_log, time_origin);
			telemetry_cleanup_log(self.wod_log, time_origin);
			telemetry_cleanup_log(self.athena_log, time_origin);
			telemetry_cleanup_mnlp(time_origin);
		}
		task_delay(TELEMETRY_CLEANUP_TASK_DELAY_MS);
	}
}
