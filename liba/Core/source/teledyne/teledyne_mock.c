/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <teledyne/teledyne.h>
#include <eps/power_lines.h>
#include <driver_toolkit/driver_toolkit.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <dev/arm/ds1302.h>
#include <core_defines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/

extern spi_chip_t spi_udos_chip;
extern driver_toolkit_nanomind_t drivers;

/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @brief
 * 		Turn the teledyne's power line on/off
 * @details
 * 		Turn the teledyne's power line on/off. This will invoke a hard power down / power on.
 * @param eps[in]
 * 		The eps which controls the teledyne's power line.
 * @param state
 * 		<b>true</b> to power on, <b>false</b> to power off.
 * @returns
 * 		<b>true</b> if successful, <b>false</b> otherwise.
 */
static bool_t power( teledyne_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	return true; // TODO add nanohub api calls
}

static void read( teledyne_t* self )
{
	DEV_ASSERT( self );
	static uint16_t data[6] = {0};

	int i;
	for( i = 0; i < 6; ++i ) {
		data[i]++;
	}
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_teledyne_( teledyne_t* self )
{
	DEV_ASSERT( self );

	self->power = power;
	self->read = read;

	new_semaphore(self->power_ctrl, 1, 1);
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/

void dosimeter_start(void)
{
	dosimeter_suspend(((driver_toolkit_t*) &drivers)->teledyne);
	xTaskCreate(udos_task, DOSIMETER_TASK_NAME, DOSIMETER_TASK_STACK, NULL, DOSIMETER_TASK_PRIO, NULL);
}

void dosimeter_resume( teledyne_t* self )
{
	give_semaphore(self->power_ctrl);
}

bool_t dosimeter_suspend( teledyne_t* self )
{
	if( take_semaphore(self->power_ctrl, DOSIMETER_RESUME_TIMEOUT) == SEMAPHORE_BUSY ) {
		return false;
	}
	return true;
}

void udos_task(void* param)
{
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	uint16_t old_low;
	FILE* fid;
	time_t seconds;
	rtc_t *rtc;
	rtc = kit->rtc;

	for(;;)
	{
		if( take_semaphore(kit->teledyne->power_ctrl, BLOCK_FOREVER) == SEMAPHORE_ACQUIRED ) {
			old_low = kit->teledyne->data[3];
			kit->teledyne->read(kit->teledyne);
			if(old_low < kit->teledyne->data[3])
			{
				seconds = rtc->get_ds1302_time();
				fid = fopen("/sd/udos.bin","a");
				if( fid == NULL ) {
					csp_printf("Failed to open file to log dosimeter data");
				}
				else {
					fwrite(&seconds,4,1,fid);
					if( ferror(fid) != 0 ) {
						csp_printf("DOSIMETER: failed to write time stamp");
					}
					fwrite(&kit->teledyne->data,1,12,fid);
					if( ferror(fid) != 0 ) {
						csp_printf("DOSIMETER: failed to log data");
					}
					fclose(fid);
				}
			}
			give_semaphore(kit->teledyne->power_ctrl);
		}
		vTaskDelay(10000); // TODO Make this configurable?
	}
}
