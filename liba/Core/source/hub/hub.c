/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file hub.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <hub/hub.h>
#include <io/nanohub.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
/**
 * @memberof hub_t
 * @brief
 * 		Set the state of power lines controlled by the hub.
 * @details
 * 		Set the state of power lines controlled by the hub.
 * @param line
 * 		The power line to set the state of.
 * @param state
 * 		The state to set the power line to.
 * @returns
 * 		<b>true</b> when state is successfully set, <b>false</b> otherwise.
 */
static bool_t power_line( hub_t* self, hub_power_lines_t line, hub_power_line_state_t state )
{;
	DEV_ASSERT( self );
	return false;
}

/**
 * @memberof hub_t
 * @brief
 * 		Invokes deployment.
 * @details
 * 		Invokes deployment of an external artifact such as an antenna.
 * @param line
 * 		The deployment line to deploy.
 */
static bool_t deploy( hub_t* self, hub_deployment_line_t line )
{
	DEV_ASSERT( self );
	return false;
}

/**
 * @memberof hub_t
 * @brief
 * 		Turn the hub on/off.
 * @details
 * 		Turn the hub on/off.
 * @param eps[in]
 * 		The EPS controlling the hub's power.
 * @param state
 * 		The new power. <b>true</b> to turn the hub on, <b>false</b> to turn it off.
 * @returns
 * 		<b>true</b> when successful, <b>false</b> otherwise.
 */
static bool_t power( hub_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );
	return true; // Nanohub can only ever be on when the mind is on.
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof hub_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_hub_( hub_t* self )
{
	self->deploy = deploy;
	self->power_line = power_line;
	self->power = power;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


