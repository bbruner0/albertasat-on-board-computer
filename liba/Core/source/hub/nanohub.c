/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file nanohub.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <hub/nanohub.h>
#include <io/nanohub.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t power_line( hub_t* self, hub_power_lines_t line, hub_power_line_state_t state )
{
	DEV_ASSERT( self );
	nanohub_switchdata_t data;
	uint8_t fivezero;
	uint8_t threethree;

	hub_set_single_output(line, state, 0, &data);

	fivezero = data.switchstatus & 0x01;
	threethree = data.switchstatus & 0x02;
	csp_printf("3v3: %x, 5V0: %x", threethree, fivezero);

	if(line == 0)
	{
		if(fivezero == state)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if(line == 1)
	{
		if(threethree == state)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

static bool_t deploy( hub_t* self, hub_deployment_line_t line )
{
	DEV_ASSERT( self );
	uint8_t ants_deployed;
	uint8_t boom_deployed;
	nanohub_knivesdata_t knives;

	for(int i=0;i<3;i++)
	{
		hub_knife(99, 0, 0, 0, &knives); // This is how Gomspace gets the struct >_>
		ants_deployed = knives.sense_status & 0b00111100; // 0 if all are stowed. 0x3C if all deployed.
		boom_deployed = knives.sense_status & 0b00000001; // 0x01 if deployed.
		if((line == 0) && (boom_deployed != 0x01))
		{
			hub_knife(0, 2, 0, 200, &knives); // Try 20 second burn on boom.
			csp_printf("Deploying boom.");
			vTaskDelay(10000); // Wait 30 seconds for knives to stop.
		}
		else if((line == 1) && (ants_deployed != 0x3C))
		{
			// TODO make sure that only 4 of 8 resistors burn at one time!
			hub_knife(1, 1, 0, 200, &knives); // Try 20 second burn on all 4 antennas.
			csp_printf("Deploying antennas.");
			vTaskDelay(10000); // Wait 30 seconds for knives to stop.
			hub_knife(1, 0, 0, 200, &knives); // Try 20 second burn on all 4 probes.
			csp_printf("Deploying probes.");
			vTaskDelay(10000); // Wait 30 seconds for knives to stop.
		}
		else
		{
			break;
		}
	}
	if((line == 0) && (boom_deployed == 0x01))
	{
		return true;
	}
	else if((line == 1) && (ants_deployed == 0x3C))
	{
		return true;
	}
	else
	{
		return false;
	}

}

static bool_t power( hub_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );
	return true; // Hub is powered with Mind, so this can only be true.
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_nanohub( nanohub_t* self )
{
	DEV_ASSERT( self );

	/* Initialize super class. */
	extern void initialize_hub_( hub_t* );
	initialize_hub_( (hub_t*) self );

	/* Override inherited methods. */
	((hub_t*) self)->power = power;
	((hub_t*) self)->power_line = power_line;
	((hub_t*) self)->deploy = deploy;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


