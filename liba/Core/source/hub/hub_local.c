/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file hub_local.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <hub/hub_local.h>
#include <eps/eps.h>
#include <eps/power_lines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t power_line( hub_t* self_, hub_power_lines_t line, hub_power_line_state_t state )
{
	hub_local_t* self = (hub_local_t*) self_;
	DEV_ASSERT( self );

	gpio_t* gpio;

	/* If the hub is in the off state, cannot do anything, return. */
	if( self->_.hub_powered_on == false )
	{
		return false;
	}

	/* If not using GPIOs to simulate power lines, return. */
	if( self->_.using_power_line_gpios == false )
	{
		return true;
	}

	gpio = self->_.power_line_gpios[line];
	if( state == HUB_POWER_LINE_ACTIVE )
	{
		gpio->set_state( gpio, GPIO_HIGH );
	}
	else
	{
		gpio->set_state( gpio, GPIO_LOW );
	}

	return true;
}

static bool_t deploy( hub_t* self_, hub_deployment_line_t line )
{
	hub_local_t* self = (hub_local_t*) self_;
	DEV_ASSERT( self );

	gpio_t* gpio;

	/* If hub is in off state, cannot deploy. */
	if( self->_.hub_powered_on == false )
	{
		return false;
	}

	/* If not using GPIOs to display deployment, return. */
	if( self->_.using_deployment_gpios == false )
	{
		return true;
	}

	gpio = self->_.deployment_gpios[line];
	gpio->set_state( gpio, GPIO_HIGH );

	return true;
}

static bool_t power( hub_t* self_, eps_t* eps, bool_t state )
{
	hub_local_t* self = (hub_local_t*) self_;
	DEV_ASSERT( self );

	/* Set new state. */
	self->_.hub_powered_on = state;
	return true;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_hub_local
(
	hub_local_t* self,
	gpio_t** power_line_gpios,
	gpio_t** deployment_gpios
)
{
	DEV_ASSERT( self );

	uint32_t iter;

	/* Initialize super class. */
	extern void initialize_hub_( hub_t* );
	initialize_hub_( (hub_t*) self );

	/* Override super class' methods. */
	((hub_t*) self)->power = power;
	((hub_t*) self)->power_line = power_line;
	((hub_t*) self)->deploy = deploy;

	/* Set hub off at initialization. */
	self->_.hub_powered_on = false;

	/* Set up power line GPIOs. */
	if( power_line_gpios == NULL )
	{
		self->_.using_power_line_gpios = false;
	}
	else
	{
		self->_.using_power_line_gpios = true;
		for( iter = 0; iter < HUB_TOTAL_POWER_LINES; ++iter )
		{
			power_line_gpios[iter]->set_direction( power_line_gpios[iter], GPIO_OUTPUT );
			power_line_gpios[iter]->set_state( power_line_gpios[iter], GPIO_LOW );
			self->_.power_line_gpios[iter] = power_line_gpios[iter];
		}
	}

	/* Set up deployment line GPIOs. */
	if( deployment_gpios == NULL )
	{
		self->_.using_deployment_gpios = false;
	}
	else
	{
		self->_.using_deployment_gpios = true;
		for( iter = 0; iter < HUB_TOTAL_DEPLOYMENT_LINES; ++iter )
		{
			deployment_gpios[iter]->set_direction( deployment_gpios[iter], GPIO_OUTPUT );
			deployment_gpios[iter]->set_state( deployment_gpios[iter], GPIO_LOW );
			self->_.deployment_gpios[iter] = deployment_gpios[iter];
		}
	}
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


