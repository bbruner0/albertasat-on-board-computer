/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <dfgm/dfgm.h>
#include <eps/power_lines.h>
#include <hub/deployment_lines.h>
#include <core_defines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
/**
 * @memberof dfgm_t
 * @brief
 * 		Turn the dfgm's power line on/off
 * @details
 * 		Turn the dfgm's power line on/off. This will invoke a hard power down / power on.
 * @param eps[in]
 * 		The eps which controls the dfgm's power line.
 * @param state
 * 		<b>true</b> to power on, <b>false</b> to power off.
 * @returns
 * 		<b>true</b> if successful, <b>false</b> otherwise.
 */
static bool_t power( dfgm_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	bool_t status;

	if( state == true )
	{
		status = eps->power_line( eps, DFGM_5V0_POWER_CHANNEL, EPS_POWER_LINE_ACTIVE );
	}
	else
	{
		status = eps->power_line( eps, DFGM_5V0_POWER_CHANNEL, EPS_POWER_LINE_INACTIVE );
	}

	return status;
}

/**
 * @memberof dfgm_t
 * @brief
 * 		Deploy the dfgm's boom.
 * @details
 * 		Deploy the dfgm's boom.
 * @param deployer[in]
 * 		The hub which controls the dfgm's boom.
 * @returns
 * 		<b>true</b> on success, <b>false</b> otherwise.
 */
static bool_t deploy( dfgm_t* self, hub_t* deployer )
{
	DEV_ASSERT( self );
	DEV_ASSERT( deployer );

	return deployer->deploy( deployer, DFGM_BOOM_DEPLOYMENT_LINE );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof dfgm_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_dfgm_( dfgm_t* self )
{
	DEV_ASSERT( self );

	init_dfgm_config(&self->dfgm_config);
	self->power = power;
	self->deploy = deploy;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


