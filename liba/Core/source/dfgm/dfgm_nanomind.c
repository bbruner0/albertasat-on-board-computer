/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.c
 * @author Alex Hamilton
 * @author Brendan Bruner
 * @date 2015-08-20
 */


#include "dfgm.h"
#include <stdint.h>
#include "FIR.h"
#include <jsmn.h>
#include <string.h>
#include <portable_types.h>
#include <printing.h>
#include <logger/logger.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#if defined( NANOMIND )
#include <csp_internal.h>
#include <dev/usart.h>
#include <io/nanopower2.h>
#endif
#include <core_defines.h>
#include <dfgm/dfgm_config.h>
#include <dev/arm/at91sam7.h>

/*
 * Structure for data sent from data logging task to SD card
 * writing task.
 */
struct dfgm_gateway_item_t
{
	char data[DFGM_PACKET_SIZE];
	uint32_t time_stamp;
};

/*
 * These functions read data off of the uart.
 */
int8_t dfgm_get8(void){
#if defined( NANOMIND )
	return (int8_t) usart_getc(DFGM_USART_HANDLE);
#else
	return 0;
#endif
}
int16_t dfgm_get16(void){
	return (int16_t)((uint16_t) dfgm_get8()<<8)+((uint16_t) dfgm_get8());
}
int32_t dfgm_get24(void){
	return ((int8_t)dfgm_get8())*0x10000+dfgm_get8()*0x100+dfgm_get8();
}
int32_t dfgm_get32(void){
	return (int32_t) (((uint32_t) dfgm_get8()<<24)+((uint32_t) dfgm_get8()<<16)+((uint32_t) dfgm_get8()<<8)+((uint32_t) dfgm_get8()));
}

/* DFGM task blocks on this to run */
xSemaphoreHandle dfgm_smphr;

/* External applications peek at this to tell if DFGM is running. */
xSemaphoreHandle dfgm_power;

/* Provides a supervisory lock on DFGM power */
xSemaphoreHandle dfgm_state_lock;

/* Gateway queue between data acquisition and data logging. */
xQueueHandle dfgm_gateway_queue;

/* Used to read FIR configuration file into memory. */
static char FIR_config_stream[CONFIG_STREAM_MAX_LENGTH];

/* FIR configurations. */
static struct FIR_config_t stream1_config;
static struct FIR_config_t stream2_config;

/* Driver objects. */
extern driver_toolkit_nanomind_t drivers;

/* Conversion values for raw mag data. */
#define CALIB_SCALE 10000
const int32_t DAC_s[3] = {17574,20318,19343};
const int32_t ADC_s[3] = {-353,-267,-302};
const int32_t offset[3] = {0,0,0};

/* Forward declartion of functions. */
static void vTaskDFGMGateway( void* );
static void cache_FIR_config( void );
static void dfgm_capture_packet( void );
static void dfgm_log_raw_packet( uint8_t* data, uint32_t time_stamp, size_t length );
static void dfgm_log_filtered_packet( uint8_t* raw_dfgm, size_t length, uint32_t time_stamp );
static void dfgm_log_hk( uint8_t* raw_dfgm, size_t length, uint32_t time_stamp );

/**
 * @memberof dfgm_t
 * Initialize DFGM data logging code.
 * @returns
 * 		true: initialization successful
 * 		false: otherwise
 */
bool_t DFGM_init( )
{
	/* Set up USART struct with correct memory location */
	AT91PS_USART usart_mem_loc;
	usart_mem_loc = AT91C_BASE_USART2;

	/* Set UART break command bit to stop power parasites from breeding. */
	usart_mem_loc->CR = STTBRK;

	/* Default, DFGM is turned off. */
	eps_output_set_single(1, 0, 0);

	/* Read in FIR configuration. */
	cache_FIR_config( );

	/* Create binary semaphores. */
	vSemaphoreCreateBinary(dfgm_smphr);
	vSemaphoreCreateBinary(dfgm_power);
	vSemaphoreCreateBinary(dfgm_state_lock);

	/* Create gateway queue. */
	dfgm_gateway_queue = xQueueCreate(DFGM_NUM_GATEWAY_ITEMS, sizeof(struct dfgm_gateway_item_t));

	/* Error check RTOS objects were created. */
	if( dfgm_smphr == NULL || dfgm_power == NULL || dfgm_state_lock == NULL || dfgm_gateway_queue == NULL ) {
		return false;
	}

	/* Make sure the semaphores are initially locked. */
	take_semaphore(dfgm_smphr, USE_POLLING);
	take_semaphore(dfgm_power, USE_POLLING);
	take_semaphore(dfgm_state_lock, USE_POLLING);

	/* Create DFGM task, this must be done after taking semaphores. */
	if( xTaskCreate(vTaskDFGM, DFGM_TASK_NAME, DFGM_TASK_STACK,NULL, DFGM_TASK_PRIO, NULL) != pdPASS ) {
			return false;
	}

	/* Create gateway task. */
	if( xTaskCreate(vTaskDFGMGateway, DFGM_GATEWAY_TASK_NAME, DFGM_GATEWAY_TASK_STACK, NULL, DFGM_GATEWAY_TASK_PRIO, NULL) != pdPASS ) {
			return false;
	}

	csp_printf_crit("DFGM init done.");
	return true;
}

/**
 * @memberof dfgm_t
 * Provides supervisory unlock to DFGM. Any calls to DFGM_Stop and DFGM_Start will
 * only work if the supervisory locked is unlocked.
 *
 * This should only be called within the system state machine.
 */
void DFGM_unlock(void)
{
	give_semaphore(dfgm_state_lock);
}

/**
 * @memberof dfgm_t
 * Provides a supervisory lock to the DFGM. See DFGM_unlock( ).
 * @sa DFGM_unlock
 */
void DFGM_lock(void)
{
	// lock semaphore power_lock_semaphore
	// Instruct EPS to turn off power line going to DFGM
	take_semaphore( dfgm_state_lock, BLOCK_FOREVER );
	DFGM_stop();
}

/**
 * @memberof dfgm_t
 * 		Used to poll if the DFGM is powered on and currently running.
 * @returns
 *  	true: DFGM is on and running.
 *  	false: DFGM is off and not running.
 */
bool_t DFGM_is_running( )
{
	if( peek_semaphore(dfgm_power, USE_POLLING) == SEMAPHORE_AVAILABLE ) {
		return true;
	}
	else {
		return false;
	}
}

/**
 * @memberof dfgm_t
 * 		Power on the DFGM and start data logging. This only works when the
 * 		supervisory lock is unlocked, use DFGM_unlock( ) to do this.
 * @return
 * 		true: DFGM was powered on and started.
 * 		false: Supervisory lock is locked, DFGM was not powered on and not started.
 */
bool_t DFGM_start( )
{
	int i;

	/* Peek at the supervisory lock, only continue if its unlocked. */
	if(peek_semaphore(dfgm_state_lock, USE_POLLING) == SEMAPHORE_AVAILABLE) {

		/* Drain garbage from usart buffer. */
		for( i = 0; i < USART_RX_QUEUE_LENGTH; ++i ) {
			if( usart_messages_waiting(DFGM_USART_HANDLE) == 0 ) {
				break;
			}
			dfgm_get8( );
		}

		/* Giving this allows the data logging task to run. */
		give_semaphore(dfgm_smphr);

		/* Giving this enables DFGM_is_running( ) to return true. */
		give_semaphore(dfgm_power);

		/* Turn on the power line to the DFGM. */
		eps_output_set_single(1, 1, 0);

		return true;
	}
	else {
		return false;
	}
}

/**
 * @memberof dfgm_t
 * 		Stop the data logging task and power off the DFGM. This has to potential to block for
 * 		at most DFGM_STOP_BLOCK_TIME seconds.
 */
void DFGM_stop( )
{
	if( DFGM_is_running( ) == true ) {
		/* Taking this will make DFGM_is_running( ) return false. */
		take_semaphore(dfgm_power, 0);

		/* Taking this stops the data logging task from running. */
		/* Cannot block forever on this, there is a slim chance two tasks will enter */
		/* the if statement and one will be deadlocked if a block forever is used. */
		take_semaphore(dfgm_smphr, DFGM_STOP_BLOCK_TIME);
	}

	/* Turn off power line to DFGM */
	eps_output_set_single(1, 0, 0);
}

/**
 * @memberof dfgm_t
 * 		Parse a packet from usart and attempt to send it to the gateway task. This method assumes usart
 * 		buffers are filled with a valid packet based on the STX and DLE at packet start.
 */
static void dfgm_capture_packet( )
{
	int i;
	portBASE_TYPE status;
	uint32_t time_stamp;
	driver_toolkit_t*	kit;
	struct dfgm_gateway_item_t item;

	kit = (driver_toolkit_t*) &drivers;

	/* Get time stamp first, more processing will delay the value of the time stamp. */
	time_stamp = kit->rtc->get_ds1302_time( );

	/* Capture entire packet as raw data. */
	item.data[DFGM_DLE] = DFGM_DLE_VAL;
	item.data[DFGM_STX] = DFGM_STX_VAL;
	for( i=DFGM_STX+1; i<DFGM_PACKET_SIZE; i++) {
		item.data[i] = usart_getc_blocking(DFGM_USART_HANDLE, DFGM_INTER_PACKET_TIME_OUT, &status);
		if( status == pdFALSE ) {
			/* If there was a timeout, we were reading in the end of a packet and about to read */
			/* in the beginning of a new packet. Return since this is now meaningless. */
			return;
		}
	}

	item.time_stamp = time_stamp;

	/* Post data to gateway queue. If queue is full then data is lost. */
	if( xQueueSendToBack(dfgm_gateway_queue, &item, USE_POLLING) != pdTRUE ) {
		csp_printf("DFGM: diag:\n\tbuffer overflow at line: %d", __LINE__);
	}
}

/**
 * memberof dfgm_t
 * 		Gateway task. Take raw data off gateway queue and attempt to log it to SD.
 */
static void vTaskDFGMGateway( void* params )
{
	struct dfgm_gateway_item_t item;
	driver_toolkit_t*	kit;
	static uint32_t time_origin = 0;

	kit = (driver_toolkit_t*) &drivers;

	for( ;; ) {
		/* Wait for an item from the queue. */
		if( xQueueReceive(dfgm_gateway_queue, &item, portMAX_DELAY) == pdTRUE ) {

			 dfgm_config_status_e dfgm_stream = dfgm_config_get_stream(&kit->dfgm->dfgm_config);

			 if( dfgm_stream == RAW_ENABLE ) {
				 /* log captured packet. */
				 dfgm_log_raw_packet(item.data, item.time_stamp, DFGM_PACKET_SIZE);
			 }
			 else {
				/* Log filtered data to aid raw data selection */
				dfgm_log_filtered_packet(item.data, DFGM_PACKET_SIZE, item.time_stamp);

				/* Log HK every DFGM_HK_LOG_CADENCE_MS milliseconds. */
				if( (task_time_elapsed( ) - time_origin) >= DFGM_HK_LOG_CADENCE_MS ) {
					time_origin = task_time_elapsed( );
					dfgm_log_hk(item.data, DFGM_PACKET_SIZE, item.time_stamp);
				}
			 }
		}
	}
}

/**
 * @memberof dfgm_t
 *		Data logging task for DFGM.
 */
void vTaskDFGM(void * pvParameters)
{
	uint32_t i=0;

	for( ;; ) {
		/* Give up CPU to other tasks.. */
		task_delay(DFGM_INTER_PACKET_DELAY);

		/* Wait until DFGM is turned on - if not already on. */
		take_semaphore(dfgm_smphr,BLOCK_FOREVER);

		/* Look for start of packet identifier. Do this by combing an entire packet's length */
		/* for start identifier. If one is not found, give up CPU */
		/* to other tasks then try again. */
		for( i = 0; i < DFGM_PACKET_SIZE; ++i ) {
			if( usart_getc(DFGM_USART_HANDLE) == DFGM_DLE_VAL ) {
				if( usart_getc(DFGM_USART_HANDLE) == DFGM_STX_VAL ) {
					dfgm_capture_packet( );
					break;
				}
			}
		}

		/* Give other tasks an opportunity to turn of the dfgm. */
		give_semaphore(dfgm_smphr);
	}
}

/**
 * @memberof dfgm_t
 * 		Log housekeeping data.
 * @param raw_dfgm
 * 		The raw data packet read from the DFGM.
 * @param length
 * 		The length of the raw data packet.
 * @param time_stamp
 * 		Time in seconds since unix epoch. This will be prepended to the housekeeping data before being
 * 		logged.
 */
static void dfgm_log_hk( uint8_t* raw_dfgm, size_t length, uint32_t time_stamp )
{
	driver_toolkit_t*	kit;
	logger_t* 			hk_logger;
	logger_error_t 		lerr;
	uint32_t 			written;
	fs_error_t			ferr;
	(void) length;

	kit = (driver_toolkit_t*) &drivers;
	hk_logger = kit->dfgm_hk_logger;

	file_t* log_file;
	log_file = logger_peek_head(hk_logger, &lerr);
	if( lerr == LOGGER_EMPTY ) {
		/* Nothing in logger, insert a blank file to work off of. */
		log_file = logger_insert(hk_logger, &lerr, NULL);
	}
	if( lerr == LOGGER_OK ) {
		uint32_t size = log_file->size(log_file, &ferr);
		if( size >= MAX_DFGM_HK_FILE_SIZE ) {
			/* file contains maximum amount of data. Insert a new blank file into the logger to work with. */
			log_file->close(log_file);
			log_file = logger_insert(hk_logger, &lerr, NULL);
		}
	}
	if( lerr == LOGGER_OK ) {
		/* Write to log_file. */
		log_file->write(log_file, &time_stamp, sizeof(time_stamp), &written);
		log_file->write(log_file, raw_dfgm+DFGM_HK_START, DFGM_HK_SIZE, &written);
		log_file->close(log_file);
	}
}


/**
 * @memberof dfgm_t
 * 		Log raw data.
 * @param data
 * 		The raw dfgm packet to log.
 * @param length
 * 		The length of the raw dfgm packet.
 */
static void dfgm_log_raw_packet( uint8_t* data, uint32_t time_stamp, size_t length )
{
	driver_toolkit_t*	kit;
	logger_t* 			raw_logger;
	logger_error_t 		lerr;
	uint32_t 			written;
	fs_error_t			ferr;
	file_t* raw_log_file;

	kit = (driver_toolkit_t*) &drivers;
	raw_logger = kit->dfgm_raw_logger;

	/* Get file at head of ring buffer .*/
	raw_log_file = logger_peek_head(raw_logger, &lerr);
	if( lerr == LOGGER_EMPTY ) {
		/* If ring buffer is empty, insert a file. */
		raw_log_file = logger_insert(raw_logger, &lerr, NULL);
		if( lerr == LOGGER_OK ) {
			raw_log_file->write(raw_log_file, (uint8_t*) &time_stamp, sizeof(time_stamp), &written);
		}
	}
	if( lerr == LOGGER_OK ) {
		/* Check if file full, if it is, insert a new file. */
		uint32_t size = raw_log_file->size(raw_log_file, &ferr);
		if( ferr == FS_OK && size >= MAX_DFGM_RAW_FILE_SIZE ) {
			raw_log_file->close(raw_log_file);
			raw_log_file = logger_insert(raw_logger, &lerr, NULL);
			if( lerr != LOGGER_OK ) {
				csp_printf("DFGM: failed to increment and open raw log file");
			}
			else {
				raw_log_file->write(raw_log_file, (uint8_t*) &time_stamp, sizeof(time_stamp), &written);
			}
		}
	}
	else {
		csp_printf("DFGM: failed to open raw log file");
	}

	if( lerr == LOGGER_OK ) {
		/* Write to log_file. */
		if( raw_log_file->write(raw_log_file, data, length, &written) != FS_OK ) {
			csp_printf("DFGM: failed to write raw data");
		}
		raw_log_file->close(raw_log_file);
	}
}

/**
 * @memberof dfgm_t
 * 		Filter raw data and log it to memory.
 * @param raw_dfgm
 * 		The raw dfgm packet which contains the data to be filtered and logged.
 * @param length
 * 		The length of the raw dfgm packet.
 * @param time_stamp
 * 		Time since unix epoch in seconds. This will be prepended to the filtered data then logged.
 */
static void dfgm_log_filtered_packet( uint8_t* raw_dfgm, size_t length, uint32_t time_stamp )
{
	static int filt1_packet_count = 0;
	static int filt2_packet_count = 0;
	static int first_run = 1;

	driver_toolkit_t*	kit;
	logger_t* 			filt_logger1;
	logger_t* 			filt_logger2;
	logger_error_t 		lerr1, lerr2;
	uint32_t 			written;
	int					i;
	fs_error_t			ferr;
	int32_t 			filt1_sample[MAG_DIM];
	int32_t 			filt2_sample[MAG_DIM];
	(void) length;
	int32_t				raw_sample[MAG_DIM];
	file_t* log_file1;
	file_t* log_file2;

	kit = (driver_toolkit_t*) &drivers;
	filt_logger1 = kit->dfgm_filt1_logger;
	filt_logger2 = kit->dfgm_filt2_logger;

	/* Open up a log file to put down sampled DFGM data into. */
	if( first_run == 1 ) {

		/* This will keep time stamps apart between nanomind reboots. */
		/* Insert a new file into the ring buffer. */
		log_file1 = logger_insert(filt_logger1, &lerr1, NULL);
		log_file2 = logger_insert(filt_logger2, &lerr2, NULL);
		first_run = 0;
	}
	else {

		/* Not the first run since power up, peek at the head packet. */
		log_file1 = logger_peek_head(filt_logger1, &lerr1);
		log_file2 = logger_peek_head(filt_logger2, &lerr2);

		/* Error check filter 1 logger. */
		if( lerr1 == LOGGER_EMPTY ) {

			/* Nothing in logger, insert a blank file to work off of. */
			log_file1 = logger_insert(filt_logger1, &lerr1, NULL);
			filt1_packet_count = 0;
		}
		else if( lerr1 == LOGGER_OK ) {

			/* Make sure the file isn't full and insert a new file if it is. */
			uint32_t size = log_file1->size(log_file1, &ferr);
			if( size >= MAX_DFGM_FILT_FILE_SIZE ) {

				/* file contains maximum amount of data. Insert a new blank file into the logger to work with. */
				log_file1->close(log_file1);
				log_file1 = logger_insert(filt_logger1, &lerr1, NULL);
				filt1_packet_count = 0;
				if( lerr1 != LOGGER_OK ) {
					csp_printf("DFGM: failed to increment and open filt 1 file");
				}
			}
		}
		else {
			csp_printf("DFGM: failed to open filt 1 file");
		}

		/* Error check filter 2 logger. */
		if( lerr2 == LOGGER_EMPTY ) {

			/* Nothing in logger, insert a blank file to work off of. */
			log_file2 = logger_insert(filt_logger2, &lerr2, NULL);
			filt2_packet_count = 0;
		}
		else if( lerr2 == LOGGER_OK ) {

			/* Make sure the file isn't full and insert a new file if it is. */
			uint32_t size = log_file2->size(log_file2, &ferr);
			if( size >= MAX_DFGM_FILT_FILE_SIZE ) {

				/* file contains maximum amount of data. Insert a new blank file into the logger to work with. */
				log_file2->close(log_file2);
				log_file2 = logger_insert(filt_logger2, &lerr2, NULL);
				filt2_packet_count = 0;
				if( lerr2 != LOGGER_OK ) {
					csp_printf("DFGM: failed to increment and open filt 2 file");
				}
			}
		}
		else {
			csp_printf("DFGM: failed to open filt 2 file");
		}
	}

	/* Parse mag data. */
	/* Iterate over every sample in raw packet. */
	for( i=0; i < DFGM_FS; i++ ){
		/* Convert from serial to int32. */
		int32_t low_word, high_word;

		high_word = ((raw_dfgm[39+(12*i)]) + (raw_dfgm[38+(12*i)]<<8))*CALIB_SCALE;
		low_word = ((raw_dfgm[37+(12*i)]) + (raw_dfgm[36+(12*i)]<<8))*CALIB_SCALE;
		raw_sample[X_DIM] = (high_word*ADC_s[0]) + (low_word*DAC_s[0]) + (offset[0]);

		high_word = ((raw_dfgm[43+(12*i)]) + (raw_dfgm[42+(12*i)]<<8))*CALIB_SCALE;
		low_word = ((raw_dfgm[41+(12*i)]) + (raw_dfgm[40+(12*i)]<<8))*CALIB_SCALE;
		raw_sample[Y_DIM] = (high_word*ADC_s[1]) + (low_word*DAC_s[1]) + (offset[1]);

		high_word = ((raw_dfgm[47+(12*i)]) + (raw_dfgm[46+(12*i)]<<8))*CALIB_SCALE;
		low_word = ((raw_dfgm[45+(12*i)]) + (raw_dfgm[44+(12*i)]<<8))*CALIB_SCALE;
		raw_sample[Z_DIM] = (high_word*ADC_s[2]) + (low_word*DAC_s[2]) + (offset[2]);

//		raw_sample[X_DIM] = ((DAC_s[0]*((raw_dfgm[37+(12*i)]) + (raw_dfgm[36+(12*i)]<<8))) << 16) + ADC_s[0]*((raw_dfgm[39+(12*i)]) + (raw_dfgm[38+(12*i)]<<8)) + offset[0];
//		raw_sample[Y_DIM] = ((DAC_s[1]*((raw_dfgm[41+(12*i)]) + (raw_dfgm[40+(12*i)]<<8))) << 16) + ADC_s[1]*((raw_dfgm[43+(12*i)]) + (raw_dfgm[42+(12*i)]<<8)) + offset[1];
//		raw_sample[Z_DIM] = ((DAC_s[2]*((raw_dfgm[45+(12*i)]) + (raw_dfgm[44+(12*i)]<<8))) << 16) + ADC_s[2]*((raw_dfgm[47+(12*i)]) + (raw_dfgm[46+(12*i)]<<8)) + offset[2];

		/* Reset sample status to busy. */
		filt_result_e filt1_sample_status = SAMPLE_BUSY;
		filt_result_e filt2_sample_status = SAMPLE_BUSY;

		/* Skip filter if there is no down sampling. */
		if( stream1_config.sample_period == 1 ) {
			filt1_sample_status = SAMPLE_READY;
			filt1_sample[X_DIM] = raw_sample[X_DIM];
			filt1_sample[Y_DIM] = raw_sample[Y_DIM];
			filt1_sample[Z_DIM] = raw_sample[Z_DIM];
		}
		else {
			filt1_sample_status = filter_data(raw_sample[X_DIM], raw_sample[Y_DIM], raw_sample[Z_DIM], filt1_sample, &stream1_config);
		}

		/* Skip filter if there is no down sampling. */
		if( stream2_config.sample_period == 1 ) {
			filt2_sample_status = SAMPLE_READY;
			filt2_sample[X_DIM] = raw_sample[X_DIM];
			filt2_sample[Y_DIM] = raw_sample[Y_DIM];
			filt2_sample[Z_DIM] = raw_sample[Z_DIM];
		}
		else {
			filt2_sample_status = filter_data(raw_sample[X_DIM], raw_sample[Y_DIM], raw_sample[Z_DIM], filt2_sample, &stream2_config);
		}

		/* If both streams were disabled, sample_status will still be SAMPLE_BUSY. */
		if( filt1_sample_status == SAMPLE_READY ) {

			/* Add time stamp if samples have been processed. */
			if( filt1_packet_count == 0 && lerr1 == LOGGER_OK ) {
				/* Log time stamp. */
				if( log_file1->write(log_file1, &time_stamp, sizeof(time_stamp), &written) != FS_OK ) {
					csp_printf("DFGM: failed to time stamp filter 1 data");

					/* Insert new file to head of ring buffer. */
					/* This will keep time stamp locations within the file synchronized. */
					log_file1->close(log_file1);
					log_file1 = logger_insert(filt_logger1, &lerr1, NULL);
					break;
				}
			}

			/* Increment packet_count */
			filt1_packet_count = (filt1_packet_count + 1) % DFGM_TIME_STAMP_CADENCE;

			/* Log sample. If logger is in a failed state, data will be lost. */
			if( lerr1 == LOGGER_OK ) {
				if( log_file1->write(log_file1, (uint8_t*) filt1_sample, MAG_DIM*sizeof(*filt1_sample), &written) != FS_OK ) {
					csp_printf("DFGM: failed to log filter 1 data");

					/* Insert new file to head of ring buffer. */
					/* This will keep time stamp locations within the file synchronized. */
					filt1_packet_count = 0;
					log_file1->close(log_file1);
					log_file1 = logger_insert(filt_logger1, &lerr1, NULL);
					break;
				}
			}
		}

		if( filt2_sample_status == SAMPLE_READY ) {

			/* Add time stamp if samples have been processed. */
			if( filt2_packet_count == 0 && lerr2 == LOGGER_OK ) {
				/* Log time stamp. */
				if( log_file2->write(log_file2, &time_stamp, sizeof(time_stamp), &written) != FS_OK ) {
					csp_printf("DFGM: failed to time stamp filter 2 data");

					/* Insert new file to head of ring buffer. */
					/* This will keep time stamp locations within the file synchronized. */
					log_file2->close(log_file2);
					log_file2 = logger_insert(filt_logger2, &lerr2, NULL);
					break;
				}
			}

			/* Increment packet_count */
			filt2_packet_count = (filt2_packet_count + 1) % DFGM_TIME_STAMP_CADENCE;

			/* Log sample. If logger is in a failed state, data will be lost. */
			if( lerr2 == LOGGER_OK ) {
				if( log_file2->write(log_file2, (uint8_t*) filt2_sample, MAG_DIM*sizeof(*filt2_sample), &written) != FS_OK ) {
					csp_printf("DFGM: failed to log filter 2 data");

					/* Insert new file to head of ring buffer. */
					/* This will keep time stamp locations within the file synchronized. */
					filt2_packet_count = 0;
					log_file2->close(log_file2);
					log_file2 = logger_insert(filt_logger2, &lerr2, NULL);
					break;
				}
			}
		}
	}

	/* Close logger if it was opened in first place. */
	if( lerr1 == LOGGER_OK ) {
		log_file1->close(log_file1);
	}
	if( lerr2 == LOGGER_OK ) {
		log_file2->close(log_file2);
	}

}

/**
 * Convert ascii to signed 64 bit int.
 * Trims leading white space and terminates when valid numbers are no longer encountered.
 */
static int64_t atos64( char const* str )
{
	char const* pos = str;

	/* Trim white space. */
	while( *pos == ' ' ) {
		++pos;
	}

	/* Check for sign. */
	int sign = 1;
	if( *pos == '-' ) {
		sign = -1;
		++pos;
	}
	else if( *pos == '+' ) {
		sign = 1;
		++pos;
	}

	/* Convert while char is between '0' and '9'. */
	int64_t res;
	for( res = 0; (*pos <= '9' && *pos >= '0'); ++pos ) {
		res = res*10 + *pos - '0';
	}

	return res*sign;
}

void DFGM_printConfig( )
{
	csp_printf_crit(	"DFGM: diag:\n\tstream one:\n\t\tshift: %" PRId64 "\n\t\tperiod: %zu\n\t\ttaps: %zu\n\t"
				"stream two:\n\t\tshift: %" PRId64 "\n\t\tperiod: %zu\n\t\ttaps: %zu",
				stream1_config.shift, stream1_config.sample_period, stream1_config.total_taps,
				stream2_config.shift, stream2_config.sample_period, stream2_config.total_taps );
}

static int parse_FIR_config( char const* configStream, size_t configStreamSize, struct FIR_config_t* stream1, struct FIR_config_t* stream2, bool_t* raw_enable );
static void cache_FIR_config( )
{
	/* Open configuration file. */
	FILE* config_file;
	size_t config_stream_size;
	config_file = fopen(DFGM_CONFIG_FILE_PATH, "r");

	/* Init FIR filters. */
	init_FIR_config(&stream1_config);
	init_FIR_config(&stream2_config);

	if( config_file == NULL ) {
		/* Failed to open, abort and use defaults. */
		stream2_config.sample_period = 1;
		stream2_config.total_taps = 1;
		stream2_config.taps[0] = 1;
		stream2_config.shift = 1;
		return;
	}
	else {
		config_stream_size = fread(FIR_config_stream, sizeof(char), CONFIG_STREAM_MAX_LENGTH, config_file);
		fclose(config_file);
		FIR_config_stream[config_stream_size] = '\0';

		bool_t raw_enable;
		if( parse_FIR_config(FIR_config_stream, config_stream_size, &stream1_config, &stream2_config, &raw_enable ) == 1 ) {
			/* Failed to parse, reset to default. */
			init_FIR_config(&stream1_config);
			init_FIR_config(&stream2_config);
		}
		else {
			/* Correctly parsed. Make sure shift is not zero and period is within bounds. */
			if( stream1_config.shift == 0 ) {
				stream1_config.shift = 1;
			}
			if( stream1_config.sample_period < FIR_MIN_DOWN_SAMPLE ) {
				stream1_config.sample_period = FIR_MIN_DOWN_SAMPLE;
			}
			if( stream2_config.shift == 0 ) {
				stream2_config.shift = 1;
			}
			if( stream2_config.sample_period < FIR_MIN_DOWN_SAMPLE ) {
				stream2_config.sample_period = FIR_MIN_DOWN_SAMPLE;
			}
		}
	}
}

/**
 * Parse configuration file, returns 1 on failure, returns 0 on success.
 * @params stream*
 * 		FIR configuration structures to fill with data based on configuration file.
 * @param raw_enable
 * 		Will be set to 1 or 0 based on configuration file. Any ambiguity in configuration file will make this default to zero (disabled).
 */
#define DFGM_JSMN_TOKEN_COUNT 2000
static jsmntok_t t[DFGM_JSMN_TOKEN_COUNT]; /* We expect no more than 128 tokens */
static int parse_FIR_config( char const* configStream, size_t configStreamSize, struct FIR_config_t* stream1, struct FIR_config_t* stream2, bool_t* raw_enable )
{
	int i;
	int tokens;
	jsmn_parser p;

	if( stream1 == NULL || stream2 == NULL || raw_enable == NULL ) {
		return 1;
	}

	stream1->total_taps = 0;
	stream2->total_taps = 0;

	jsmn_init(&p);
	tokens = jsmn_parse(&p, configStream, configStreamSize, t, DFGM_JSMN_TOKEN_COUNT);

	/* Assume the top-level element is an object */
	if (tokens < 1 || t[0].type != JSMN_OBJECT) {
		return 1;
	}
	if (tokens < -1) {
		return 1;
	}
	if( tokens < 0 ) {
		return 1;
	}

  	/* Loop over all keys of the root object */
	for (i = 1; i < tokens; i++) {
		if( jsmn_isobject(configStream, &t[i], "stream1") == 0 ) {
			/* Parse stream 1 */
			for( i += 2; i < tokens; ++i ) {
				if( jsmn_iskey(configStream, &t[i], "period") == 0 ) {
					stream1->sample_period = atoi(configStream + t[i+1].start);
					i++;
				} else if( jsmn_iskey(configStream, &t[i], "shift") == 0 ) {
					stream1->shift = atos64(configStream + t[i+1].start);
					i++;
				} else if (jsmn_iskey(configStream, &t[i], "taps") == 0) {
					int j;
					if (t[i+1].type == JSMN_ARRAY) {
						for (j = 0; j < t[i+1].size; j++) {
							jsmntok_t *g = &t[i+j+2];
							stream1->taps[j] = atos64(configStream + g->start);
						}
						stream1->total_taps = t[i+1].size;
						i += t[i+1].size + 1;
					}
					else {
						return 1;
					}
				} else {
					i = i - 1;
					break;
				}
			}
		}
		else if( jsmn_isobject(configStream, &t[i], "stream2") == 0 ) {
			/* Parse stream 2 */
			for( i += 2; i < tokens; ++i ) {
				if( jsmn_iskey(configStream, &t[i], "period") == 0 ) {
					stream2->sample_period = atoi(configStream + t[i+1].start);
					i++;
				} else if( jsmn_iskey(configStream, &t[i], "shift") == 0 ) {
					stream2->shift = atoi(configStream + t[i+1].start);
					i++;
				} else if (jsmn_iskey(configStream, &t[i], "taps") == 0) {
					int j;
					if (t[i+1].type == JSMN_ARRAY) {
						for (j = 0; j < t[i+1].size; j++) {
							jsmntok_t *g = &t[i+j+2];
							stream2->taps[j] = atoi(configStream + g->start);
						}
						stream2->total_taps = t[i+1].size;
						i += t[i+1].size + 1;
					}
					else {
					}
				} else {
					i = i - 1;
					break;
				}
			}
		}
		else if( jsmn_iskey(configStream, &t[i], "raw") == 0 ) {
			if( strncmp( configStream + t[i+1].start, "on", t[i+1].end - t[i+1].start) == 0 ) {
				*raw_enable = 1;
			}
			else {
				*raw_enable = 0;
			}
			++i;
		}
		else {
			return 1;
		}
	}

	if( stream1->total_taps == 0 || stream2->total_taps == 0 ) {
		return 1;
	}
	return 0;
}


