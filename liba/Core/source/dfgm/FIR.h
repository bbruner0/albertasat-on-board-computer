/*
 * FIR.h
 *
 *  Created on: May 12, 2016
 *      Author: bandren
 */

#ifndef DFGM_FIR_VERIFICATION_FIR_H_
#define DFGM_FIR_VERIFICATION_FIR_H_

#include <inttypes.h>
#include <stdlib.h>
#include <core_defines.h>

#define MAG_DIM 3
#define X_DIM 0
#define Y_DIM 1
#define Z_DIM 2

/* Number of accumulators required to facilitate min down sample period. */
#if FIR_MIN_DOWN_SAMPLE <= 0
#error "FIR_MIN_DOWN_SAMPLE must be defined as greater than zero."
#endif
#define CONFIG_MAX_ACCUMULATORS ((MAX_TAPS/FIR_MIN_DOWN_SAMPLE)+1) /* C standard required (a/b)*b + a%b = a, therefore, this is floor of division plus one. */


/**
 * @struct FIR_acc_t
 * @var FIR_acc_t::acc
 * 		An array of length three to hold the accumulation of x, y, and z mag data.
 * @var FIR_acc_t::step
 * 		Propagation point within the FIR filter. For example:
 * 			step = 0 => No accumulation has been done
 * 			step = 1 => product of first sample/coef pair
 * 			step = 2 => product of second sample/coef pair
 * 			step = n => product of n'th sample/coef pair
 */
struct FIR_acc_t {
	int64_t acc[MAG_DIM];
	size_t step;
};

/**
 * @struct FIR_config_t
 * @var FIR_config_t::taps
 * 		Coefficients where the product is taken to be:
 * 			sample zero * taps[0]
 * 			sample one	* taps[1]
 * 			sample two	* taps[2]
 * 			...
 * @var FIR_config_t::total_taps
 * 		Total number of coefficients in FIR_acc_t::taps
 * @var FIR_config_t::shift
 * 		A decimal value to divide accumulation results by. Set this as '1' to ignore it.
 * @var FIR_config_t::accumulators
 *		Array of accumulators for each sample progressing through filter. If down sampling was 1 for 1 (ie, no down sampling
 *		just filter) then this array is the same length as MAX_TAPS. If down sampling was 1 every 100 samples and there were
 *		101 coefficients, then this array would be length 2.
 * @var FIR_config_t::running_acc
 * 		The running accumulator channel. Accumulators in this array are currently being used in the filter.
 * @var FIR_config_t::idle_acc
 * 		Idle accumulator channel. Accumulators in this array are not being used by the filter and can be taken
 * 		for use.
 * @var FIR_config_t::lead_acc
 * 		Pointer to accumulator which is the most progressed through the filter (ie, the next sample output by the filter will
 * 		be from this accumulator).
 * @var FIR_config_t::tail_acc
 * 		Pointer to accumulator which is the least progressed through the filter (ie, this accumulator contains the least amount
 * 		of product sum pairs out of all running accumulators).
 */
struct FIR_config_t {
	int64_t taps[MAX_TAPS];
	size_t 	total_taps;
	int64_t	shift;
  	size_t 	sample_period;
	struct FIR_acc_t 	accumulators[CONFIG_MAX_ACCUMULATORS];
  	struct FIR_acc_t*	running_acc[CONFIG_MAX_ACCUMULATORS];
  	struct FIR_acc_t* 	idle_acc[CONFIG_MAX_ACCUMULATORS];
	struct FIR_acc_t* 	lead_acc;
  	struct FIR_acc_t* 	tail_acc;
};

/* Results of filter_data method. */
typedef enum {
	SAMPLE_READY = 0,
	SAMPLE_BUSY = 1
} filt_result_e;

filt_result_e filter_data( int32_t, int32_t, int32_t, int32_t*, struct FIR_config_t* );
void init_FIR_config( struct FIR_config_t* );

#endif /* DFGM_FIR_VERIFICATION_FIR_H_ */
