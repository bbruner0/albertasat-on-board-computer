/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file fatfs_result_map.c
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */

#include <filesystems/fatfs/filesystem_fatfs.h>
#ifdef __LPC17XX__
#include <ff.h>
#else
#include <fat_sd/ff.h>
#endif


/* Maps a fatfs error code to an fs_error_t code. */
fs_error_t _fatfs_result_map[] = { 	FS_OK,
									FS_DISK_ERR,
									FS_INT_ERR,
									FS_NOT_READY,
									FS_NO_FILE,
									FS_NO_PATH,
									FS_INVALID_NAME,
									FS_DENIED,
									FS_EXIST,
									FS_INVALID_OBJECT,
									FS_WRITE_PROTECTED,
									FS_INVALID_DRIVE,
									FS_NOT_ENABLED,
									FS_NO_FILESYSTEM,
									FS_MKFS_ABORTED,
									FS_TIMEOUT,
									FS_LOCKED,
									FS_NOT_ENOUGH_CORE,
									FS_TOO_MANY_OPEN_FILES,
									FS_INVALID_PARAMETER
								};

/* Map for opening attribute. */
BYTE attribute_map[] = { FA_OPEN_EXISTING, FA_OPEN_ALWAYS, FA_CREATE_NEW, FA_CREATE_ALWAYS };
