/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_fatfs.c
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */

#include <filesystems/fatfs/filesystem_fatfs.h>
#include <filesystems/fatfs/fatfs_map.h>
#include <portable_types.h>
#ifdef __LPC17XX__
#include <ff.h>
#else
#include <fat_sd/ff.h>
#endif


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define NO_FILE 				(char const *) 0

/* nanomind changed the API for f_mkfs... */
#ifdef NANOMIND
#define DEFAULT_DRIVE			0
#else
#define DEFAULT_DRIVE			""
#endif
#define MOUNT_LATER				0
#define DEFAULT_ALLOCATION_SIZE 0
#define FDISK					0

#define FILESYSTEM_FATFS_IS_INIT	1
#define FILESYSTEM_FATFS_NOT_INIT	0

#define FILESYSTEM_FATFS_MAX_FILES	20
#define FILESYSTEM_FATFS_MAX_DIRS	1


/********************************************************************************/
/* Private Singleton Variables													*/
/********************************************************************************/
/* Used to prevent duplicate initialization of singletons. */
static uint8_t _filesystem_fatfs_is_init_ = FILESYSTEM_FATFS_NOT_INIT;

static mutex_t		_filesystem_fatfs_access_controller_;

static file_fatfs_t 	_filesystem_fatfs_files_[FILESYSTEM_FATFS_MAX_FILE_HANDLES];
static dir_fatfs_t 		_filesystem_fatfs_dirs_[FILESYSTEM_FATFS_MAX_DIR_HANDLES];

/********************************************************************************/
/* Protected Singleton Variables												*/
/********************************************************************************/
/* Declare fatfs_result_map. */
extern fs_error_t fatfs_result_map[];
extern BYTE attribute_map[];


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Destroy all constructed file handles and directory handles.
 * @details
 * 		Destroy all constructed file handles and directory handles.
 */
static void _filesystem_fatfs_destroy_files_( filesystem_fatfs_t *self )
{
	DEV_ASSERT( self );

	int16_t 		iter;
	file_fatfs_t 	*file;
	dir_fatfs_t		*dir;

	/* Destroy file handles. */
	for( iter = 0; iter < FILESYSTEM_FATFS_MAX_FILE_HANDLES; ++iter )
	{
		file = &(self->_files_[iter]);
		((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
	}

	/* Destroy dir handles. */
	for( iter = 0; iter < FILESYSTEM_FATFS_MAX_DIR_HANDLES; ++iter )
	{
		dir = &(self->_dirs_[iter]);
		((fs_handle_t *) dir)->destroy( (fs_handle_t *) dir);
	}
}
/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Initializes the singleton semaphores and mutexs used.
 * @details
 * 		Initializes the singleton semaphores and mutexs used.
 * @attention
 * 		Only to be called in constructor. Duplicate calls after
 * 		<b>FILESYSTEM_SUCCESS</b> has been returned once will result
 * 		in undefined behaviour. However, it is safe to call successively
 * 		if <b>FILESYSTEM_FAILURE</b> has been returned.
 * @returns
 * 		<b>FILESYSTEM_SUCCESS</b>: If initialized successfully.
 * 		<b>FILESYSTEM_FAILURE</b>: If failed to initialized handles. Handles
 * 		are not safe to use. Note, all semaphores and mutexs are destroyed when there is
 * 		a failure.
 */
static uint8_t _filesystem_fatfs_initialize_locks_( filesystem_fatfs_t *self )
{
	DEV_ASSERT( self );

	/* Create access controll mutex . */
	new_mutex( _filesystem_fatfs_access_controller_ );
	if(  _filesystem_fatfs_access_controller_ == NULL )
	{
		return FILESYSTEM_FAILURE;
	}

	/* Success if here. */
	return FILESYSTEM_SUCCESS;
}

/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Initializes the array of file handles and directory handles.
 * @details
 * 		Initializes the array of file handles and directory handles.
 * @attention
 * 		Only to be called in constructor. Duplicate calls after
 * 		<b>FILESYSTEM_SUCCESS</b> has been returned once will result
 * 		in undefined behaviour. However, it is safe to call successively
 * 		if <b>FILESYSTEM_FAILURE</b> has been returned.
 * @returns
 * 		<b>FILESYSTEM_SUCCESS</b>: If initialized successfully.
 * 		<b>FILESYSTEM_FAILURE</b>: If failed to initialized handles. Handles
 * 		are not safe to use. Note, all handles are destroyed when there is
 * 		a failure.
 */
static uint8_t _filesystem_fatfs_initialize_files_( filesystem_fatfs_t *self )
{
	/* This function needs to be refactored into several smaller functions. */

	DEV_ASSERT( self );

	uint8_t 		err;
	int16_t 		iter_out, iter_in;
	file_fatfs_t 	*file;
	dir_fatfs_t		*dir;

	/* Initialize file handles. */
	for( iter_out = 0; iter_out < FILESYSTEM_FATFS_MAX_FILE_HANDLES; ++iter_out )
	{
		file = &(self->_files_[iter_out]);
		err = initialize_file_fatfs( file, (filesystem_t *) self );

		/* Error check for successful construction. */
		if( err != FS_HANDLE_SUCCESS)
		{
			/* Failure, destroy all successfully created handles before returning. */
			for( iter_in = 0; iter_in < iter_out+1; ++iter_in )
			{
				file = &(self->_files_[iter_in]);
				((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
			}
			return FILESYSTEM_FAILURE;
		}
	}

	/* Initialize dir handles. */
	for( iter_out = 0; iter_out < FILESYSTEM_FATFS_MAX_DIR_HANDLES; ++iter_out )
	{
		dir = &(self->_dirs_[iter_out]);
		err = initialize_dir_fatfs( dir, (filesystem_t *) self );

		/* Error check for successful construction. */
		if( err != FS_HANDLE_SUCCESS)
		{
			/* Failure, destroy all successfully created dir handles before returning. */
			for( iter_in = 0; iter_in < (iter_out + 1); --iter_in )
			{
				dir = &(self->_dirs_[iter_in]);
				((fs_handle_t *) dir)->destroy( (fs_handle_t *) dir );
			}

			/* Destroy all successfully created file handles too. */
			for( iter_in = 0; iter_in < FILESYSTEM_FATFS_MAX_FILE_HANDLES; --iter_in )
			{
				file = &(self->_files_[iter_in+1]);
				((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
			}
			return FILESYSTEM_FAILURE;
		}
	}

	/* Success if we are here. */
	return FILESYSTEM_SUCCESS;
}

/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Use the native FATFS functions to open a file.
 * @details
 * 		Use the native FATFS functions to open a file.
 * @param handle[in]
 * 		The handle which will operate on the opened file.
 * @param name[in]
 * 		The name of the file to open.
 * @param attribute[in]
 * 		File attributes to open the file with.
 * @returns
 * 		Resulting error code.
 */
static fs_error_t _filesystem_fatfs_open_file_
(
	filesystem_fatfs_t *self,
	file_t *handle,
	char const *name,
	fs_open_attribute_t attribute
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	DEV_ASSERT( name );
	ASSERT_FATFS_HANDLE_IS_FATFS( handle );

	uint8_t retry;
	FRESULT native_err;
	FIL		*native_handle;
	BYTE 	open_attribute;

	native_handle = (FIL *) fs_handle_get_native( (fs_handle_t *) handle );
	open_attribute = FA_READ | FA_WRITE | attribute_map[attribute];

	/* Attempt to open the file. */
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		native_err = f_open( native_handle, name, open_attribute );
		if( native_err == FR_OK )
		{
			/* File opened, break from loop. */
			break;
		}
		/* Give a recovery delay before retrying to open the file. */
		_DISK_ERR_RECOVERY_DELAY( );
	}

	/* Set the name of opened file in the handle. */
	fs_handle_set_name( (fs_handle_t *) handle, name );

	/* Attempt to flush the buffer. */
	//f_sync( native_handle );

	return _fatfs_result_map[native_err];
}

/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Use the native FATFS functions to open a directory.
 * @details
 * 		Use the native FATFS functions to open a directory.
 * @param dir[in]
 * 		The handle which will operate on the opened directory.
 * @param name[in]
 * 		The name of the directory to open.
 * @returns
 * 		Resulting error code.
 */
static fs_error_t _filesystem_fatfs_open_dir_
(
	filesystem_fatfs_t * self,
	dir_fatfs_t *dir,
	char const *dir_name
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( dir );
	DEV_ASSERT( dir_name );
	ASSERT_FATFS_HANDLE_IS_FATFS( dir );

	FRESULT 	err;
	fat_dir_t	*native_dir;
	uint8_t		retry;

	native_dir = (fat_dir_t *) fs_handle_get_native( (fs_handle_t *) dir );

	/* Attempt to open the directory. */
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		err = f_opendir( native_dir, dir_name );
		if( err == FR_OK )
		{
			/* directory opened, break from loop. */
			break;
		}
		/* Give a recovery delay before retrying to open the directory. */
		_DISK_ERR_RECOVERY_DELAY( );
	}

	fs_handle_set_name( (fs_handle_t *) dir, dir_name );

	return _fatfs_result_map[err];
}

/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Use the native FATFS functions to close a file.
 * @details
 * 		Use the native FATFS functions to close a file.
 * @param handle[in]
 * 		The handle which has an open file that needs to be closed.
 * @returns
 * 		Resulting error code
 */
static fs_error_t _filesystem_fatfs_close_file_( filesystem_fatfs_t *self, file_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	ASSERT_FATFS_HANDLE_IS_FATFS( handle );

	uint8_t retry;
	FRESULT native_err;
	FIL		*native_handle;

	native_handle = (FIL *) fs_handle_get_native( (fs_handle_t *) handle );

	/* Try to close the file. */
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		native_err = f_close( native_handle );
		if( native_err == FR_OK )
		{
			break;
		}
		/* Give a recovery delay before attempting to close again. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[native_err];
}

/**
 * @memberof filesystem_fatfs_t
 * @private
 * @brief
 * 		Use the native FATFS functions to close a directory.
 * @details
 * 		Use the native FATFS functions to close a directory.
 * @param handle[in]
 * 		The handle which has an open directory that needs to be closed.
 * @returns
 * 		Resulting error code
 */
static fs_error_t _filesystem_fatfs_close_dir_( filesystem_fatfs_t *self, dir_t *dir )
{
	DEV_ASSERT( self );
	DEV_ASSERT( dir );
	ASSERT_FATFS_HANDLE_IS_FATFS( dir );

	return _fatfs_result_map[FR_OK];
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static file_t *open
(
	filesystem_t *self,
	fs_error_t *err,
	char const *name,
	fs_open_attribute_t attribute,
	block_time_t block_time
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	DEV_ASSERT( name );

	file_fatfs_t 		*handle;
	fs_error_t			temp_err;

	/* Try to get a free handle. */
	handle = _filesystem_fatfs_get_file( (filesystem_fatfs_t *) self, block_time );
	if( handle == (file_fatfs_t *) NULL )
	{
		/* No free handles. */
		*err = FS_TOO_MANY_OPEN_FILES;
		return _filesystem_get_null_file( self );
	}

	/* Open the request file */
	temp_err = _filesystem_fatfs_open_file_( (filesystem_fatfs_t *) self, (file_t *) handle, name, attribute );
	*err = temp_err;

	if( temp_err == FS_OK )
	{
		/* No error opening the file. return the new handle. */
		return (file_t *) handle;
	}

	/* Error opening file, set the handle free and return the null handle. */
	_filesystem_fatfs_free_handle( (filesystem_fatfs_t *) self, (fs_handle_t *) handle );
	return _filesystem_get_null_file( self );
}

static fs_error_t close( filesystem_t *self, file_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	ASSERT_FATFS_HANDLE_IS_FATFS( handle );

	fs_error_t		temp_err;

	/* If handle is null, return with error. */
	if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_NULL )
	{
		return FS_NULL_HANDLE;
	}

	/* Close the file */
	temp_err = _filesystem_fatfs_close_file_( (filesystem_fatfs_t *) self, handle );

	/* If the file closed successfully, free the handle. */
	if( temp_err == FS_OK )
	{
		_filesystem_fatfs_free_handle( (filesystem_fatfs_t *) self, (fs_handle_t *) handle );
	}

	return temp_err;
}

static fs_error_t close_all( filesystem_t *self )
{
	DEV_ASSERT( self );

	uint32_t 		iter;
	file_fatfs_t 	*handles_array;
	file_fatfs_t	*handle;
	fs_error_t 		temp_err;

	handles_array = ((filesystem_fatfs_t *) self)->_files_;

	/* Loop through all handles and close any open ones. */
	for( iter = 0; iter < FILESYSTEM_FATFS_MAX_FILE_HANDLES; ++iter )
	{
		/* Get the handle in the handles_array at the current iter position */
		handle = &handles_array[iter];

		/* Check if handle is busy. */
		if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_BUSY )
		{
			/* Handle is busy, so close it */
			temp_err = self->close( self, (file_t *) handle );

			/* If close failed, do not continue. */
			if( temp_err != FS_OK )
			{
				return temp_err;
			}
		}
	}
	return FS_OK;
}

static fs_error_t delete( filesystem_t *self, char const *file_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( file_name );

	FRESULT	native_err;
	uint8_t retry;

	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		/* Attempt to delete the file. */
		native_err = f_unlink( file_name );
		if( native_err == FR_OK || native_err == FR_NO_FILE )
		{
			/* File was deleted, break from loop */
			break;
		}
		/* File was not deleted, allow for a recovery delay before retrying. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[native_err];
}

static fs_error_t delete_handle( filesystem_t *self, file_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );

	fs_error_t temp_err;

	/* Check if the handle is null. */
	if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_NULL )
	{
		return FS_NULL_HANDLE;
	}

	/* Close the file, then delete it. */
	temp_err = _filesystem_fatfs_close_file_( (filesystem_fatfs_t *) self, handle );
	if( temp_err != FS_OK )
	{
		/* Failure closing, return error. */
		return temp_err;
	}

	/* If the file closed successfully, try to delete it, free the handle */
	temp_err = self->delete( self, fs_handle_get_name( (fs_handle_t *) handle ) );
	_filesystem_fatfs_free_handle( (filesystem_fatfs_t *) self, (fs_handle_t *) handle );

	return temp_err;
}

static fs_error_t reformat( filesystem_t *self )
{
	DEV_ASSERT( self );

	FRESULT native_err;

	/* Close all files before reformatting. */
	native_err = self->close_all( self );
	if( native_err != FR_OK )
	{
		/* Failed to close all files. Do not reformat. */
		return native_err;
	}

	/* Reformat the drive. */
	/* TODO: change allocation size to meet filesystem requirements in 2. of wiki. */
	native_err = FR_MKFS_ABORTED;//f_mkfs( DEFAULT_DRIVE, FDISK, DEFAULT_ALLOCATION_SIZE );

	return _fatfs_result_map[native_err];
}

static fs_error_t rename_file( filesystem_t *self, char const *old_name, char const *new_name )
{
	DEV_ASSERT( self);
	DEV_ASSERT( old_name );
	DEV_ASSERT( new_name );

	FRESULT native_err;
	uint8_t retry;

	/* Attempt to rename the file. */
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		native_err = f_rename( old_name, new_name );
		if( native_err == FR_OK )
		{
			/* Rename successful, break loop. */
			break;
		}
		/* Allow for a recovery delay before trying to rename again. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[native_err];
}
static fs_error_t rename_handle( filesystem_t *self, file_t *handle, char const *new_name )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	DEV_ASSERT( new_name );
	ASSERT_FATFS_HANDLE_IS_FATFS( handle );

	fs_error_t temp_err;

	/* Check the handle is not null. */
	if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_NULL )
	{
		return FS_NULL_HANDLE;
	}

	/* Close the file, then rename it. */
	temp_err = _filesystem_fatfs_close_file_( (filesystem_fatfs_t *) self, handle );
	if( temp_err != FS_OK )
	{
		/* Error closing, do not continue. */
		return temp_err;
	}

	/* If the file closed successfully, rename it and reopen it. */
	temp_err = self->rename( self, fs_handle_get_name( (fs_handle_t *) handle ), new_name );
	if( temp_err != FS_OK )
	{
		/* Could not rename handle, return error. */
		return temp_err;
	}

	/* Open newly named file. */
	temp_err = _filesystem_fatfs_open_file_( (filesystem_fatfs_t *) self, handle, new_name, FS_OPEN_ALWAYS );
	return temp_err;
}

static fs_error_t remap( filesystem_t *self, file_t *handle, char const *name, fs_open_attribute_t type )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );
	DEV_ASSERT( name );

	fs_error_t err;

	/* Check the handle is not null. */
	if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_NULL )
	{
		return FS_NULL_HANDLE;
	}

	/* Check the handle is not free. */
	if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_FREE )
	{
		return FS_INVALID_PARAMETER;
	}

	/* Close the currently open file, then open the requested file. */
	err = _filesystem_fatfs_close_file_( (filesystem_fatfs_t *) self, handle );
	if( err != FS_OK )
	{
		return err;
	}
	err = _filesystem_fatfs_open_file_( (filesystem_fatfs_t *) self, handle, name, type );
	if( err != FS_OK )
	{
		self->close( self, handle );
	}
	return err;
}

static fs_error_t close_dir( filesystem_t *self, dir_t *dir )
{
	DEV_ASSERT( self );
	DEV_ASSERT( dir );

	fs_error_t		temp_err;

	/* If handle is null, return with error. */
	if( fs_handle_get_busy_status( (fs_handle_t *) dir ) == HANDLE_NULL )
	{
		return FS_NULL_HANDLE;
	}

	/* Close the directory */
	temp_err = _filesystem_fatfs_close_dir_( (filesystem_fatfs_t *) self, dir );

	/* If the file closed successfully, free the handle. */
	if( temp_err == FS_OK )
	{
		_filesystem_fatfs_free_dir( (filesystem_fatfs_t *) self, (fs_handle_t *) dir );
	}

	return temp_err;
}

static dir_t *open_dir( filesystem_t *self, fs_error_t *err, char const *dir_name, block_time_t block_time )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	DEV_ASSERT( dir_name );

	dir_fatfs_t	*dir;
	fs_error_t	temp_err;

	/* Try to get a free handle. */
	dir = _filesystem_fatfs_get_dir( (filesystem_fatfs_t *) self, block_time );
	if( dir == (dir_fatfs_t *) NULL )
	{
		/* No free directories. */
		*err = FS_TOO_MANY_OPEN_DIRS;
		return _filesystem_get_null_dir( self );
	}

	/* Open the request directory */
	temp_err = _filesystem_fatfs_open_dir_( (filesystem_fatfs_t *) self, dir, dir_name );
	*err = temp_err;

	if( temp_err == FS_OK )
	{
		/* No error opening the dir. return the new dir. */
		return ( dir_t *) dir;
	}

	/* Otherwise, error opening file, set the dir free and return the null dir. */
	_filesystem_fatfs_free_dir( (filesystem_fatfs_t *) self, (fs_handle_t *)dir );
	return _filesystem_get_null_dir( self );
}

static fs_error_t file_exists( filesystem_t *fs, char const *name )
{
	DEV_ASSERT( fs );
	DEV_ASSERT( name );

	FILINFO info;
	return f_stat( name, &info );
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t _initialize_filesystem_fatfs( filesystem_fatfs_t *self )
{
	DEV_ASSERT( self );

	uint8_t 	init_err;

	/* Initialize super data. */
	init_err = _initialize_filesystem( (filesystem_t *) self );
	if( init_err == FILESYSTEM_FAILURE )
	{
		/* Failure to initialize super object, do not continue. */
		return init_err;
	}

	/* Initial number of open files/dirs. */
	self->_num_files_ = 0;
	self->_num_dirs_ = 0;

	self->_files_ = _filesystem_fatfs_files_;
	self->_dirs_ = _filesystem_fatfs_dirs_;

	/* Override method pointers. */
	((filesystem_t *) self)->open = open;
	((filesystem_t *) self)->close = close;
	((filesystem_t *) self)->close_all = close_all;
	((filesystem_t *) self)->delete = delete;
	((filesystem_t *) self)->delete_handle = delete_handle;
	((filesystem_t *) self)->reformat = reformat;
	((filesystem_t *) self)->rename = rename_file;
	((filesystem_t *) self)->rename_handle = rename_handle;
	((filesystem_t *) self)->remap = remap;
	((filesystem_t *) self)->open_dir = open_dir;
	((filesystem_t *) self)->close_dir = close_dir;
	((filesystem_t *) self)->file_exists = file_exists;

	/* Check if handles, dirs, semaphores, and mutex singletons have been */
	/* initialized. */
	if( _filesystem_fatfs_is_init_== FILESYSTEM_FATFS_IS_INIT )
	{
		/* Yes, they have been initialized. Safe to return successfully. */
		return FILESYSTEM_SUCCESS;
	}

	/* Construct all file handles and dirs. */
	init_err = _filesystem_fatfs_initialize_files_( self );
	if( init_err != FILESYSTEM_SUCCESS )
	{
		return init_err;
	}

	/* Create all semaphores and mutexs. */
	init_err = _filesystem_fatfs_initialize_locks_( self );
	if( init_err != FILESYSTEM_SUCCESS )
	{
		_filesystem_fatfs_destroy_files_( self );
		return init_err;
	}

	/* Finally, mark singleton data as initialized. */
	_filesystem_fatfs_is_init_ = FILESYSTEM_FATFS_IS_INIT;

	return FILESYSTEM_SUCCESS;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Protected Method Defines														*/
/********************************************************************************/
file_fatfs_t *_filesystem_fatfs_get_file( filesystem_fatfs_t *self, block_time_t block_time )
{
	DEV_ASSERT( self );

	file_fatfs_t	*handle;
	uint32_t 		iter;
	file_fatfs_t 	*handles_array;

	handles_array = ((filesystem_fatfs_t *) self)->_files_;

	/* lock to prevent duplicate handle access. */
	lock_mutex( _filesystem_fatfs_access_controller_, BLOCK_FOREVER );

	if( self->_num_files_ >= FILESYSTEM_FATFS_MAX_FILE_HANDLES )
	{
		/* No file handles available. */
		unlock_mutex( _filesystem_fatfs_access_controller_ );
		return (file_fatfs_t *) NULL;
	}

	/* Loop through all handles to find a free one. */
	for( iter = 0; iter < FILESYSTEM_FATFS_MAX_FILE_HANDLES; ++iter )
	{
		handle = &handles_array[iter];

		/* Check if the handle is busy. */
		if( fs_handle_get_busy_status( (fs_handle_t *) handle ) == HANDLE_BUSY )
		{
			/* Keep looking until a free handle is found. */
			continue;
		}
		/* Found a free handle, break. */
		++self->_num_files_;
		break;
	}

	/* Fail safe. Should never occur. */
	if( iter == FILESYSTEM_FATFS_MAX_FILE_HANDLES )
	{
		unlock_mutex( _filesystem_fatfs_access_controller_ );
		return (file_fatfs_t *) NULL;
	}

	fs_handle_set_busy( (fs_handle_t *) handle );
	unlock_mutex( _filesystem_fatfs_access_controller_ );
	return handle;
}

dir_fatfs_t *_filesystem_fatfs_get_dir( filesystem_fatfs_t *self, block_time_t block_time )
{
	DEV_ASSERT( self );

	dir_fatfs_t 	*dir;
	int16_t 		iter;
	dir_fatfs_t 	*dirs_array;

	dirs_array = ((filesystem_fatfs_t *) self)->_dirs_;

	/* Lock it to prevent duplicate access. */
	lock_mutex( _filesystem_fatfs_access_controller_, BLOCK_FOREVER );

	if( self->_num_dirs_ >= FILESYSTEM_FATFS_MAX_DIR_HANDLES )
	{
		/* No file handles available. */
		unlock_mutex( _filesystem_fatfs_access_controller_ );
		return (dir_fatfs_t *) NULL;
	}

	/* Loop through all dirs to find a free one. */
	for( iter = 0; iter < FILESYSTEM_FATFS_MAX_DIR_HANDLES; ++iter )
	{
		dir = &dirs_array[iter];

		/* Check if the handle is busy. */
		if( fs_handle_get_busy_status( (fs_handle_t *) dir ) == HANDLE_BUSY )
		{
			/* Keep looking until a free handle is found. */
			continue;
		}
		/* Found a free handle, break. */
		break;
		++self->_num_dirs_;
	}

	/* Fail safe. Should never occur. */
	if( iter == FILESYSTEM_FATFS_MAX_DIR_HANDLES )
	{
		unlock_mutex( _filesystem_fatfs_access_controller_ );
		return (dir_fatfs_t *) NULL;
	}

	fs_handle_set_busy( (fs_handle_t *) dir );
	unlock_mutex( _filesystem_fatfs_access_controller_ );
	return dir;
}

void _filesystem_fatfs_free_handle( filesystem_fatfs_t *self, fs_handle_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );

	lock_mutex( _filesystem_fatfs_access_controller_, BLOCK_FOREVER );
	fs_handle_set_free( handle );
	--self->_num_files_;
	unlock_mutex( _filesystem_fatfs_access_controller_ );
}

void _filesystem_fatfs_free_dir( filesystem_fatfs_t *self, fs_handle_t *handle )
{
	DEV_ASSERT( self );
	DEV_ASSERT( handle );

	lock_mutex( _filesystem_fatfs_access_controller_, BLOCK_FOREVER );
	fs_handle_set_free( handle );
	--self->_num_dirs_;
	unlock_mutex( _filesystem_fatfs_access_controller_ );
}
