/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dir.c
 * @author Brendan Bruner
 * @date Jul 22, 2015
 */
#include <filesystems/dir.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static char const *list( dir_t *self, fs_error_t *err)
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	*err = FS_NULL_HANDLE;
	_dir_set_file_name( self, "\0" );
	return _dir_get_file_name( self );
}

static fs_error_t reset_list( dir_t *self )
{
	DEV_ASSERT( self );

	return FS_NULL_HANDLE;
}


/********************************************************************************/
/* Constructor 																	*/
/********************************************************************************/
uint8_t _initialize_dir( dir_t *self, filesystem_t *fs, fs_handle_type_t type )
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );

	uint8_t init_err;

	/* Initialize super struct data. */
	init_err = _initialize_fs_handle( (fs_handle_t *) self, fs, type );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		return init_err;
	}

	/* Assign virtual function pointers. */
	self->list = list;
	self->reset_list = reset_list;

	return FS_HANDLE_SUCCESS;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
char const *_dir_get_file_name( dir_t *self )
{
	DEV_ASSERT( self );

	return (char const *) self->_file_name_;
}

void _dir_set_file_name( dir_t *self, char const *file_name )
{
	uint32_t iter;

	for( iter = 0; iter < FILESYSTEM_MAX_NAME_LENGTH; ++iter )
	{
		self->_file_name_[iter] = file_name[iter];

		/* Break if end of string. */
		if( self->_file_name_[iter] == '\0' )
		{
			break;
		}
	}
	/* Silently truncate string in event the above for loop exceeded */
	/* FILESYSTEM_MAX_NAME_LENGTH iterations. */
	self->_file_name_[iter] = '\0';
}
