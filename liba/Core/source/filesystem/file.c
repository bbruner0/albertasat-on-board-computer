/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file file.c
 * @author Brendan Bruner
 * @date May 12, 2015
 */

#include <filesystems/file.h>
#include <filesystems/filesystem.h>


/********************************************************************************/
/* Virtual Method Implementation												*/
/********************************************************************************/
static fs_error_t write( file_t *self, uint8_t *msg, uint32_t size, uint32_t *written )
{
	DEV_ASSERT( self );
	UNUSED( msg );
	UNUSED( size );

	/* No data is written. */
	*written = 0;

	return FS_NULL_HANDLE;
}

static fs_error_t read( file_t *self, uint8_t *msg, uint32_t read_size, uint32_t *read_count )
{
	DEV_ASSERT( self );
	UNUSED( msg );
	UNUSED( read_size );

	/* No data is read. */
	*read_count = 0;

	return FS_NULL_HANDLE;
}

static fs_error_t seek( file_t *self, uint32_t location )
{
	DEV_ASSERT( self );
	UNUSED( location );

	return FS_NULL_HANDLE;
}

static uint32_t size( file_t *self, fs_error_t *err )
{
	DEV_ASSERT( self );

	*err = FS_NULL_HANDLE;

	/* File is zero bytes. */
	return 0;
}

static uint32_t position( file_t *self, fs_error_t *err )
{
	DEV_ASSERT( self );

	*err = FS_NULL_HANDLE;

	/* At position zero. */
	return 0;
}

static fs_error_t truncate( file_t *self )
{
	DEV_ASSERT( self );

	return FS_NULL_HANDLE;
}

static fs_error_t kill( file_t *self, char const *buffer, block_time_t time_out, uint32_t begin, uint32_t end )
{
	DEV_ASSERT( self );
	DEV_ASSERT( buffer );
	UNUSED( time_out );
	UNUSED( begin );
	UNUSED( end );

	return FS_NULL_HANDLE;
}

static void close( file_t* self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Initialization Implementation												*/
/********************************************************************************/
uint8_t _initialize_file( file_t *self, filesystem_t *fs, fs_handle_type_t type )
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );

	uint8_t init_err;

	/* Initialize super struct data. */
	init_err = _initialize_fs_handle( (fs_handle_t *) self, fs, type );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		return init_err;
	}

	/* Assign virtual function pointers. */
	self->position = position;
	self->read = read;
	self->seek = seek;
	self->size = size;
	self->truncate = truncate;
	self->write = write;
	self->kill = kill;
	self->close = close;

	return FS_HANDLE_SUCCESS;
}
