/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_generic
 * @author Brendan Bruner
 * @date Jan 29, 2015
 */

#include <states/state_relay.h>
#include <states/state.h>
#include <telecommands/telecommand.h>
#include "portable_types.h"
#include <core_defines.h>
#include <printing.h>

#define STATE_NOT_INIT 0
#define STATE_IS_INIT 1

#define ASSERT_FS_OP( state, err, file ) 								\
	do {																\
		if( (err) != FS_OK ) {											\
			(state)->_fs_->close( (state)->_fs_, (file) );				\
			return STATE_FAILURE;										\
		}																\
	} while( 0 )

#define TIMER_NAME (signed char*) "config timer"
#define TIMER_PERIOD CONFIG_LOCK_DELAY
#define TIMER_ID (void *) 0

/************************************************************************/
/* Global Defines														*/
/************************************************************************/
static semaphore_t _state_global_config_lock_;
static software_timer_t _state_config_time_out_;

static uint8_t _state_is_init_ = STATE_NOT_INIT;

static mutex_t _state_communal_mutex_;		/* Provides mutual exclusion for all state object. */

char* state_to_name_map[] = { 		"Undefined State",
									"Operations Bring Up",
									"Operations Low Power",
									"Operations Alignment",
									"Operations Science",
									"LEOP Black Out",
									"LEOP Alignment",
									"LEOP Low Power",
									"LEOP Boom Deployment",
									"LEOP Antenna Deployment",
									"LEOP Entry Evaluation" };


/************************************************************************/
/* Private Method Defines												*/
/************************************************************************/
static void _state_timer_call_back_( software_timer_t xTimer )
{
	UNUSED( xTimer );
	unlock_mutex( _state_global_config_lock_ );
}

/**
 * @memberof state_t
 * @private
 * @brief
 * 		Loads configuration file from memory.
 * @details
 * 		Loads configuration file from memory. If no config file
 * 		is in memory it creates a default config file. In addition,
 * 		if it detects corruption in the existing config file it will
 * 		reset it to the default one.
 * @param state[in]
 * 		A pointer to the state_t structure to load the config file of.
 * @returns
 * 		<b>STATE_SUCCESS</b>: If it loads a config file. If no config file exists, it
 * 		calls state_t::reset_config.
 * 		<br><b>STATE_FAILURE</b>: If a file system error occured loading the config file.
 * 		<br><b>STATE_DEFAULTED</b>: No configuration file was found. A default configuration
 * 		was used, but no configuration file was generated.
 */
static uint8_t state_load_config( state_t *state )
{
	DEV_ASSERT( state );

	state_config_t config;
	filesystem_t *fs;
	char const *log;
	fs_error_t err;
	uint8_t reset_err;
	file_t *file;
	uint32_t read;

	fs = state->_fs_;
	log = state->_config_name_;

	/* Open log file. */
	file = fs->open( fs, &err, log, FS_OPEN_EXISTING, BLOCK_FOREVER );
	if( err == FS_NO_FILE )
	{
		fs->close( fs, file );

		/* No config file exists yet. Set state up with default one. */
		reset_err = state_reset_config( state );
		if( reset_err == STATE_SUCCESS )
		{
			/* Successful reset. */
			return STATE_DEFAULTED;
		}
		else
		{
			return STATE_FAILURE;
		}
	}
	else
	{
		ASSERT_FS_OP( state, err, file );
	}

	/* Read configuration. */
	err = file->read( file, (uint8_t *) &config, sizeof(state_config_t), &read );
	ASSERT_FS_OP( state, err, file );
	fs->close( fs, file );
	if( read != sizeof( state_config_t ) || state_config_detect_corruption( &config ) == STATE_CONFIG_CORRUPT )
	{
		/* File is not the expected size - it is corrupt. */
		fs->delete( fs, log );
		if( state_reset_config( state ) == STATE_FAILURE)
		{
			return STATE_FAILURE;
		}
		else
		{
			return STATE_DEFAULTED;
		}
	}

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	state->_config_ = config;
	unlock_mutex( _state_communal_mutex_ );

	return STATE_SUCCESS;
}


/************************************************************************/
/* virtual methods														*/
/************************************************************************/
static void state_no_entry_no_exit( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	UNUSED( kit );
	return;
}

static state_t *never_exit_state(	state_t *state,	state_relay_t *relay )
{
	DEV_ASSERT( state );
	UNUSED( relay );
	return NULL;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy_state( state_t *state )
{
	DEV_ASSERT( state );
	return;
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state( state_t *state, char const *log, filesystem_t *fs, state_config_t *default_config )
{
	DEV_ASSERT( state );
	DEV_ASSERT( log );
	DEV_ASSERT( fs );
	DEV_ASSERT( default_config );

	/* Initialize singleton lock for configuration changes. */
	if( _state_is_init_ == STATE_NOT_INIT )
	{
		new_semaphore( _state_global_config_lock_, 1, 1 ); /* Posted binary semaphore. */
		if( _state_global_config_lock_ == NULL )
		{
			return STATE_FAILURE;
		}

		/* Initialize mutex. */
		new_mutex( _state_communal_mutex_ );
		if( _state_communal_mutex_ == NULL )
		{
			delete_semaphore( _state_global_config_lock_ );
			return STATE_FAILURE;
		}

		new_timer( _state_config_time_out_, TIMER_NAME, TIMER_PERIOD, MANUAL_RESTART, TIMER_ID, _state_timer_call_back_ );
		if( _state_config_time_out_ == NULL )
		{
			delete_semaphore( _state_global_config_lock_ );
			delete_mutex( _state_communal_mutex_ );
			return STATE_FAILURE;
		}

		_state_is_init_ = STATE_IS_INIT;
	}

	/* Assign variable members. */
	state->_fs_ = fs;
	state->_default_config_ = *default_config;
	state->_config_name_ = log;
	state->_mutex_ = &_state_communal_mutex_;

	/* Assign virtual functions. */
	state->enter_state = state_no_entry_no_exit;
	state->exit_state = state_no_entry_no_exit;
	state->next_state = never_exit_state;
	state->destroy = destroy_state;

	/* Attempt to load a pre existing configuration. */
	/* If none exist, use the default configuration. */
	uint8_t err;
	err = state_load_config( state );
	if( err == STATE_FAILURE )
	{
		err = state_reset_config( state );
		if( err == STATE_FAILURE )
		{
			return STATE_FAILURE;
		}
		return STATE_DEFAULTED;
	}

	return err;
}


/********************************************************************************/
/* Public Non Virtual Methods													*/
/********************************************************************************/
void state_execution_hk( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );

	uint8_t uses;

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	uses = state->_config_._uses_hk;
	unlock_mutex( _state_communal_mutex_ );

	if( command == NULL || uses == STATE_DISABLES_EXECUTION )
	{
		return;
	}
	telecommand_execute( command );
}

void state_execution_transmit( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );

	uint8_t uses;

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	uses = state->_config_._uses_transmit;
	unlock_mutex( _state_communal_mutex_ );

	if( command == NULL || uses == STATE_DISABLES_EXECUTION )
	{
		return;
	}
	telecommand_execute( command );
}

void state_execution_response( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );

	uint8_t uses;

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	uses = state->_config_._uses_response;
	unlock_mutex( _state_communal_mutex_ );

	if( command == NULL || uses == STATE_DISABLES_EXECUTION )
	{
		return;
	}
	telecommand_execute( command );
}

void state_execution_mnlp( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );

	uint8_t uses;

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	uses = state->_config_._uses_mnlp;
	unlock_mutex( _state_communal_mutex_ );

	if( command == NULL || uses == STATE_DISABLES_EXECUTION )
	{
		return;
	}
	telecommand_execute( command );
}

void state_execution_dfgm( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );

	uint8_t uses;

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	uses = state->_config_._uses_dfgm;
	unlock_mutex( _state_communal_mutex_ );

	if( command == NULL || uses == STATE_DISABLES_EXECUTION )
	{
		return;
	}
	telecommand_execute( command );
}

void state_execution_diagnostics( state_t *state, telecommand_t *command )
{
	DEV_ASSERT( state );
	if( command == NULL )
	{
		return;
	}
	telecommand_execute( command );
}

uint8_t state_reset_config( state_t *state )
{
	DEV_ASSERT( state );

	/* Set active config as default config. */
	return state_set_config( state, &state->_default_config_ );
}

uint8_t state_set_config( state_t *state, state_config_t *config )
{
	DEV_ASSERT( state );
	DEV_ASSERT( config );

	filesystem_t *fs;
	char const *log;
	fs_error_t err;
	file_t *file;
	uint32_t written;

	fs = state->_fs_;
	log = state->_config_name_;

	/* Open log file. */
	file = fs->open( fs, &err, log, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( state, err, file );

	/* Write new configuration to log file. */
	err = file->write( file, (uint8_t *) config, sizeof( state_config_t ), &written );
	ASSERT_FS_OP( state, err, file );
	if( written != sizeof( state_config_t) )
	{
		/* Out of memory to write... */
		fs->close( fs, file );
		return STATE_FAILURE;
	}

	fs->close( fs, file );

	/* Success, update changes in ram. */
	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	state->_config_ = *config;
	unlock_mutex( _state_communal_mutex_ );

	return STATE_SUCCESS;
}

void state_get_config( state_t *state, state_config_t *config )
{
	DEV_ASSERT( state );
	DEV_ASSERT( config );

	lock_mutex( _state_communal_mutex_, BLOCK_FOREVER );
	*config = state->_config_;
	unlock_mutex( _state_communal_mutex_ );
}

uint8_t query_state_id( state_t *state )
{
	if( state == NULL )
	{
		return STATE_NULL;
	}
	return state->_id_;
}

base_t state_lock_config_globally( block_time_t block_time )
{
	base_t success;
	success = take_semaphore( _state_global_config_lock_, block_time );
	if( success == SEMAPHORE_ACQUIRED )
	{
		restart_timer( _state_config_time_out_, BLOCK_FOREVER );
	}
	return success;
}

void state_unlock_config_globally( )
{
	post_semaphore( _state_global_config_lock_ );
	stop_timer( _state_config_time_out_, BLOCK_FOREVER );
}

void state_refresh( state_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	/* Refresh the config file with whats in non volatile. */
	state_load_config( self );

}


void state_enter( state_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	state_refresh( self, kit );
	self->enter_state( self, kit );
}


void state_exit( state_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	state_refresh( self, kit );
	self->exit_state( self, kit );
}
