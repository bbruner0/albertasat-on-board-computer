/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_black_out.c
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#include <states/state.h>
#include <states/leop/state_black_out.h>
#include <states/state_relay.h>
#include <core_defines.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_DISABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_DISABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_DISABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static char const * const config_log = STATE_BLACK_OUT_LOG;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_enter_blackout( state_t *state, driver_toolkit_t *kit )
{
	state_black_out_t* self = (state_black_out_t*) state;
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	/* Start the timer. */
	self->_.timer.start( &self->_.timer );
}

static void state_exit_blackout( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	state_black_out_t* self = (state_black_out_t*) state;
	DEV_ASSERT( self );
	DEV_ASSERT( relay );

	eps_t* eps;

	eps = relay->drivers->eps;

	if( self->_.timer.is_expired( &self->_.timer, USE_POLLING ) )
	{
		/* Return next state. */
		if( eps->mode( eps ) ==  EPS_MODE_OPTIMAL )
		{
			return (state_t*) &relay->boom_deploy;
		}
		else
		{
			return (state_t*) &relay->power_charge;
		}
	}
	else
	{
		/* Black out has not finished yet. */
		return NULL;
	}

}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( state_t* state )
{
	state_black_out_t* self = (state_black_out_t*) state;
	DEV_ASSERT( self );

	self->_.timer.destroy( &self->_.timer );
	/* Call super's destructor. */
	self->_.supers_destroy( (state_t*) self );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_black_out( state_black_out_t *state, filesystem_t *fs, uint32_t time_out, char const* log_file )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	bool_t			timer_err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, config_log, fs, &config );
	timer_err = initialize_ptimer( &state->_.timer, time_out, BLACK_OUT_RESOLUTION, fs, log_file );
	if( !timer_err )
	{
		/* Error initializing timer. */
		return STATE_FAILURE;
	}

	((state_t *) state)->_id_ = 		STATE_BLACK_OUT_ID;
	((state_t *) state)->enter_state = 	state_enter_blackout;
	((state_t *) state)->exit_state = 	state_exit_blackout;
	((state_t *) state)->next_state = 	next_state;

	/* Override super's destructor, but maintain a reference to it. */
	state->_.supers_destroy = ((state_t*) state)->destroy;
	((state_t*) state)->destroy = destroy;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
