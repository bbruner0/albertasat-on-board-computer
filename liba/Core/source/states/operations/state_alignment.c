/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_alignment.c
 * @author Brendan Bruner
 * @date Aug 11, 2015
 */
#include "states/operations/state_alignment.h"
#include "states/state_relay.h"
#include <core_defines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_ENABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_ENABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_entry( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static void state_exit_alignment( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	eps_t *eps;

	eps = relay->drivers->eps;

	/* If battery is at power safe, go to power safe state. */
	if( eps->mode( eps ) == EPS_MODE_POWER_SAFE || eps->mode( eps ) == EPS_MODE_CRITICAL )
	{
		return (state_t *) &relay->low_power;
	}
	else
	{
		return (state_t *) &relay->science;
	}

	return NULL;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_alignment( state_alignment_t *state, filesystem_t *fs )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, STATE_ALIGNMENT_LOG, fs, &config );

	((state_t *) state)->_id_ = 		STATE_ALIGNMENT_ID;
	((state_t *) state)->enter_state = 	state_entry;
	((state_t *) state)->exit_state = 	state_exit_alignment;
	((state_t *) state)->next_state = 	next_state;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
