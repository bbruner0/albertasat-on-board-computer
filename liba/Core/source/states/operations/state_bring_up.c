/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file bring_up.c
 * @author Brendan Bruner
 * @date Aug 10, 2015
 */
#include <states/operations/state_bring_up.h>
#include <states/state_relay.h>
#include <core_defines.h>
#include <script_time_server.h>
#include <adcs/adcs_locale_logger.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_DISABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_DISABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_DISABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
char const * const operations_bring_up_config_log = STATE_BRING_UP_LOG;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_entry( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	/* Start the flight scheduler */
	initialize_script_time_server(&kit->time_server, kit->gs);

	/* Attempt to enable locale logger */
	csp_printf("state: diag: attempting to enable locale logger");
	adcs_locale_logger_enable( );

	//kit->comm->power( kit->comm, kit->eps, true );
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	return (state_t *) &relay->low_power;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_bring_up( state_bring_up_t *state, filesystem_t *fs )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, operations_bring_up_config_log, fs, &config );

	((state_t *) state)->_id_ = 		STATE_BRING_UP_ID;
	((state_t *) state)->enter_state = 	state_entry;
	((state_t *) state)->next_state = 	next_state;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


