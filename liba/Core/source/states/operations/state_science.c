/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_science.c
 * @author Brendan Bruner
 * @date Aug 11, 2015
 */
#include "states/operations/state_science.h"
#include <states/state_relay.h>
#include <core_defines.h>
#include <dfgm/dfgm.h>
#include <athena_gateway/athena.h>
#include <mnlp.h>
#include <dfgm.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_ENABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_ENABLES_EXECUTION
#define DEFAULT_DFGM		STATE_ENABLES_EXECUTION
#define DEFAULT_MNLP		STATE_ENABLES_EXECUTION
// TODO Not used anymore. Add to state_entry and exit.


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static char const * const config_log = STATE_SCIENCE_LOG;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_entry( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	kit->mnlp->start_stop(kit->mnlp, true);
	DFGM_unlock();
}

static void state_exit_science( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	size_t i;
	int err;

	kit->mnlp->start_stop(kit->mnlp, false);
	DFGM_lock();
	athena_suspend( );
	script_time_server_kill(&kit->time_server);

	/* Turn power off to ADCS. Try at most twice.
	 */
	for( i = 0; i < POWER_CHANNEL_RETRY; i++ ) {
		err = eps_output_set_single(ADCS_5V0_POWER_CHANNEL, POWER_CHANNEL_OFF, 0);
		if( err > 0 ) {
			break;
		}
	}
	for( i = 0; i < POWER_CHANNEL_RETRY; i++ ) {
		err = eps_output_set_single(ADCS_3V3_POWER_CHANNEL, POWER_CHANNEL_OFF, 0);
		if( err > 0 ) {
			break;
		}
	}
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	eps_t *eps;

	eps = relay->drivers->eps;

	/* if battery is in power safe or critical, go to low power. */
	if( eps->mode( eps ) == EPS_MODE_POWER_SAFE || eps->mode( eps ) == EPS_MODE_CRITICAL )
	{
		return (state_t *) &relay->low_power;
	}

	/* Otherwise, stay in this state. */
	return (state_t *) NULL;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_science( state_science_t *state, filesystem_t *fs )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, config_log, fs, &config );

	((state_t *) state)->_id_ = 		STATE_SCIENCE_ID;
	((state_t *) state)->enter_state = 	state_entry;
	((state_t *) state)->exit_state = 	state_exit_science;
	((state_t *) state)->next_state = 	next_state;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
