/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_dfgm_power.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_dfgm_power.h>
#include <string.h>
#include <athena_gateway/athena.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	dfgm_t* dfgm;
	dfgm = self->_kit->dfgm;

	if( strncmp(self->_argument, TELECOMMAND_DFGM_OFF_SYM0, TELECOMMAND_DFGM_OFF_SYM0_LEN) == 0 ||
		strncmp(self->_argument, TELECOMMAND_DFGM_OFF_SYM1, TELECOMMAND_DFGM_OFF_SYM1_LEN) == 0 )
	{
		dfgm->power(dfgm, self->_kit->eps, false);

		/* Don't let athena run if power to dfgm is off. */
		athena_suspend( );
	}
	else if( strncmp(self->_argument, TELECOMMAND_DFGM_ON_SYM0, TELECOMMAND_DFGM_ON_SYM0_LEN) == 0 ||
			 strncmp(self->_argument, TELECOMMAND_DFGM_ON_SYM1, TELECOMMAND_DFGM_ON_SYM1_LEN) == 0 )
	{
		dfgm->power(dfgm, self->_kit->eps, true);
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_dfgm_power_t* clone;
	bool_t err;

	clone = (telecommand_dfgm_power_t*) OBCMalloc( sizeof(telecommand_dfgm_power_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_dfgm_power( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_dfgm_power( telecommand_dfgm_power_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DFGM_POWER;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
