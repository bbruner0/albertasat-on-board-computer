/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_locale_logger.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_locale_logger.h>
#include <string.h>
#include <adcs/adcs_locale_logger.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	if( self->_argument_length == 0 ) {
		csp_printf(	"locale logger: diag:"
					"\n\tEnabled: %s"
					"\n\tCommissioned: %s"
					"\n\tOptional Arguments:"
					"\n\t\t\"commission\" to commision locale logger"
					"\n\t\t\"decommission\" to decommission locale logger",
					(adcs_locale_logger_is_enabled( ) == true ? "yes" : "no"),
					(adcs_locale_logger_is_commissioned( ) == true ? "yes" : "no"));
		return;
	}

	if( strncmp(self->_argument, TELECOMMAND_LOCALE_LOGGER_COMMISSION_SYM, self->_argument_length) == 0 ) {
		/* Commanded to commission locale logger. */
		bool_t status = adcs_locale_logger_commission( );
		if( status == true ) {
			csp_printf("locale logger: diag:\n\tsuccessfully commissioned");
		}
		else {
			csp_printf("locale logger: diag:\n\tfailed to commission, try rebooting nanomind first");
		}
	}
	else if( strncmp(self->_argument, TELECOMMAND_LOCALE_LOGGER_DECOMMISSION_SYM, TELECOMMAND_LOCALE_LOGGER_DECOMMISSION_SYM_LEN) == 0 ) {
		/* Commanded to decommission locale logger. */
		bool_t status = adcs_locale_logger_decommission( );
		if( status == true ) {
			csp_printf("locale logger: diag:\n\tsuccessfully decommissioned");
		}
		else {
			csp_printf("locale logger: diag:\n\tfailed to decommission, try rebooting nanomind first");
		}
	}
	else {
		csp_printf(	"locale logger: diag:"
					"\n\tEnabled: %s"
					"\n\tCommissioned: %s"
					"\n\tOptional Arguments:"
					"\n\t\t\"commission\" to commision locale logger"
					"\n\t\t\"decommission\" to decommission locale logger",
					(adcs_locale_logger_is_enabled( ) == true ? "yes" : "no"),
					(adcs_locale_logger_is_commissioned( ) == true ? "yes" : "no"));

	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_locale_logger_t* clone;
	bool_t err;

	clone = (telecommand_locale_logger_t*) OBCMalloc( sizeof(telecommand_locale_logger_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_locale_logger( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_locale_logger( telecommand_locale_logger_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_LOCALE_LOGGER;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
