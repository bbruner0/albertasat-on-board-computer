/*
 * Copyright (C) 2018  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_rtos_status.c
 * @author Brendan Bruner
 * @date April 2018
 */
#include <telecommands/nanomind/telecommand_rtos_status.h>
#include <string.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define HEX_BASE_NUMBER 16


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static int findnl( char const* str )
{
	int c = 0;
	for( ;; ) {
		if( str[c] == '\n' || str[c] == '\0' || c > 4000 ) {
			return c;
		}
		++c;
	}
	return c;
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self_ )
{
	DEV_ASSERT( self_ );
	struct telecommand_rtos_status_t* self = (void*) self_;
	char const* tlptr;
	int nl;

	csp_printf_crit("task list");
	vTaskList((signed char*) self->task_list);

	tlptr = self->task_list;
	for( ;; ) {
		nl = findnl(tlptr);
		csp_printf_crit("\n%.*s", nl+1, tlptr);
		if( tlptr[nl] == '\0' ) {
			break;
		}
		tlptr += nl + 1;
		task_delay(50);
	}

	csp_printf_crit("run time stats");
	vTaskGetRunTimeStats((signed char*) self->task_list);

	tlptr = self->task_list;
	for( ;; ) {
		nl = findnl(tlptr);
		csp_printf_crit("\n%.*s", nl+1, tlptr);
		if( tlptr[nl] == '\0' ) {
			break;
		}
		tlptr += nl + 1;
		task_delay(50);
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_rtos_status_t* clone;
	bool_t err;

	clone = (telecommand_rtos_status_t*) OBCMalloc( sizeof(telecommand_rtos_status_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_rtos_status( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_rtos_status( telecommand_rtos_status_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_RTOS_STATUS;

	return err;
}
