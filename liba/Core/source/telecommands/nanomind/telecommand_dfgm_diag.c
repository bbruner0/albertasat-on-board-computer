/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_dfgm_diag.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_dfgm_diag.h>
#include <string.h>
#include <dfgm.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	dfgm_t* dfgm;
	dfgm = self->_kit->dfgm;

	csp_printf("DFGM Diag:\n\tstream: %d\n\tpower: %d", dfgm_config_get_stream(&dfgm->dfgm_config), DFGM_is_running());
	DFGM_printConfig( );
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_dfgm_diag_t* clone;
	bool_t err;

	clone = (telecommand_dfgm_diag_t*) OBCMalloc( sizeof(telecommand_dfgm_diag_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_dfgm_diag( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_dfgm_diag( telecommand_dfgm_diag_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DFGM_DIAG;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
