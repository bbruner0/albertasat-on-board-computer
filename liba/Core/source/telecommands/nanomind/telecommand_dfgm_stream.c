/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_dfgm_stream.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_dfgm_stream.h>
#include <string.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	dfgm_t* dfgm;
	dfgm = self->_kit->dfgm;

	if( strncmp(self->_argument, TELECOMMAND_DFGM_STREAM1_SYM0, TELECOMMAND_DFGM_STREAM1_SYM0_LEN) == 0 ||
		strncmp(self->_argument, TELECOMMAND_DFGM_STREAM1_SYM1, TELECOMMAND_DFGM_STREAM1_SYM1_LEN) == 0 )
	{
		if( dfgm_config_set_stream(&dfgm->dfgm_config, RAW_ENABLE) == true ) {
			csp_printf_crit("DFGM stream: raw enabled, filtered disabled");
		}
		else {
			csp_printf_crit("DFGM stream: failure to enable raw");
		}
	}
	else if( strncmp(self->_argument, TELECOMMAND_DFGM_STREAM2_SYM0, TELECOMMAND_DFGM_STREAM2_SYM0_LEN) == 0 ||
			 strncmp(self->_argument, TELECOMMAND_DFGM_STREAM2_SYM1, TELECOMMAND_DFGM_STREAM2_SYM1_LEN) == 0 )
	{
		if( dfgm_config_set_stream(&dfgm->dfgm_config, FILTER_ENABLE) == true ) {
			csp_printf_crit("DFGM stream: filter enabled, raw disabled");
		}
		else {
			csp_printf_crit("DFGM stream: failure to enable filter");
		}
	}
//	else if( strncmp(self->_argument, TELECOMMAND_DFGM_STREAM_DISABLE_SYM0, TELECOMMAND_DFGM_STREAM_DISABLE_SYM0_LEN) == 0 ||
//			 strncmp(self->_argument, TELECOMMAND_DFGM_STREAM_DISABLE_SYM1, TELECOMMAND_DFGM_STREAM_DISABLE_SYM1_LEN) == 0 ||
//			 strncmp(self->_argument, TELECOMMAND_DFGM_STREAM_DISABLE_SYM2, TELECOMMAND_DFGM_STREAM_DISABLE_SYM2_LEN) == 0 )
//	{
//		if( dfgm_config_set_stream(&dfgm->dfgm_config, STREAM_DISABLE) == true ) {
//			csp_printf("DFGM stream: stream disabled");
//		}
//		else {
//			csp_printf("DFGM stream: failure to disable");
//		}
//	}
//	else if( strncmp(self->_argument, TELECOMMAND_DFGM_RAW_ON_SYM0, TELECOMMAND_DFGM_RAW_ON_SYM0_LEN) == 0 ||
//		strncmp(self->_argument, TELECOMMAND_DFGM_RAW_ON_SYM1, TELECOMMAND_DFGM_RAW_ON_SYM1_LEN) == 0 )
//	{
//		if( dfgm_config_set_stream(&dfgm->dfgm_config, RAW_ENABLE) == true ) {
//			csp_printf("DFGM stream: raw set");
//		}
//		else {
//			csp_printf("DFGM stream: failure to set raw");
//		}
//	}
	else {
		csp_printf_crit("DFGM stream: unknown parameter \"%.*s\"", self->_argument_length, self->_argument);
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_dfgm_stream_t* clone;
	bool_t err;

	clone = (telecommand_dfgm_stream_t*) OBCMalloc( sizeof(telecommand_dfgm_stream_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_dfgm_stream( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_dfgm_stream( telecommand_dfgm_stream_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DFGM_STREAM;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
