/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_debug_set.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/self_alteration/telecommand_debug_set.h>
#include <string.h>
#include <athena_gateway/athena.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	char num[TELECOMMAND_MAX_ARGUMENT_LENGTH+1];
	int i;

	strncpy(num, self->_argument, self->_argument_length);
	num[self->_argument_length] = '\0';
	i = atoi(num);

	if( i < CSP_DEBUG_MIN || i > CSP_DEBUG_MAX ) {
		csp_printf_crit("Debug level out of range: %d", i);
		_telecommand_set_status(self, CMND_SYNTX_ERR);
		return;
	}

	csp_printf_crit("Debug level: %d", i);
	csp_printf_set(i);
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_debug_set_t* clone;
	bool_t err;

	clone = (telecommand_debug_set_t*) OBCMalloc( sizeof(telecommand_debug_set_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_debug_set( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_debug_set( telecommand_debug_set_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DEBUG_SET;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
