/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_demo.c
 * @author Brendan Bruner
 * @date Oct 9, 2015
 */
#include <telecommands/self_alteration/telecommand_demo.h>
#include <printing.h>
#include <string.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *command )
{
	telecommand_demo_t* self = (telecommand_demo_t*) command;
	DEV_ASSERT( self );

	DEMO_PRINTF( self->_.msg );
}

static telecommand_t* clone( telecommand_t* self_ )
{
	telecommand_demo_t* self = (telecommand_demo_t*) self_;
	DEV_ASSERT( self );

	telecommand_demo_t* clone_command;
	bool_t err;

	clone_command = (telecommand_demo_t*) OBCMalloc( sizeof( telecommand_demo_t ) );
	if( clone_command == NULL ){ return NULL; }

	err = initialize_telecommand_demo( clone_command, ((telecommand_t*) self)->_kit, self->_.msg );
	if( !err )
	{
		OBCFree( (void*) clone_command );
		return NULL;
	}

	return (telecommand_t*) clone_command;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_demo( telecommand_demo_t *self, driver_toolkit_t *kit, char const* msg )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DEFAULT;
	((telecommand_t *) self)->clone = clone;
	strncpy( self->_.msg, msg, TELECOMMAND_DEMO_MSG_LENGTH );
	self->_.msg[TELECOMMAND_DEMO_MSG_LENGTH-1] = '\0';

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
