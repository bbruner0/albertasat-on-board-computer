/*
 * Copyright (C) 2018  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_server.c
 * @author Brendan Bruner
 * @date April 6, 2018
 */

#include <telecommands/telecommand_server.h>
#include <parser/interpreter/statement_expression.h>
#include <parser/parser.h>
#include <script_daemon.h>
#include <printing.h>
#include <parser/interpreter/telecommand_expression.h>
#include <telecommands/telecommand_prototype_manager.h>
#include <telecommands/telecommand.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h>
#include <telecommands/nanomind/mnlp_off.h>
#include <telecommands/nanomind/mnlp_reinit.h>
#include <telecommands/nanomind/telecommand_dfgm_power.h>
#include <telecommands/nanomind/telecommand_dfgm_stream.h>
#include <telecommands/nanomind/telecommand_dfgm_diag.h>
#include <telecommands/nanomind/telecommand_scv_config.h>
#include <telecommands/nanomind/telecommand_dosimeter.h>
#include <telecommands/nanomind/telecommand_athena.h>
#include <telecommands/nanomind/telecommand_locale_logger.h>
#include <telecommands/nanomind/telecommand_telemetry_cleanup.h>
#include <telecommands/nanomind/telecommand_downlink.h>
#include <telecommands/nanomind/telecommand_adcs.h>
#include <telecommands/nanomind/telecommand_uart_flush.h>
#include <telecommands/subsystem_alteration/telecommand_configure_eps.h>
#include <telecommands/self_alteration/telecommand_debug_set.h>
#include <telecommands/nanomind/telecommand_rtos_status.h>
#include "../../include/telecommands/nanomind/telecommand_scv_config.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/

/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static telecommand_mnlp_off_t			mnlp_off;
static telecommand_mnlp_reinit_t		mnlp_reinit;
static telecommand_dfgm_power_t			dfgm_power_command;
static telecommand_dfgm_stream_t		dfgm_stream_command;
static telecommand_dfgm_diag_t			dfgm_diag_command;
static telecommand_dosimeter_t			dosimeter_command;
static telecommand_athena_t				athena_command;
static telecommand_locale_logger_t		locale_logger_command;
static telecommand_telemetry_cleanup_t	tcleanup_command;
static telecommand_downlink_t 			downlink_command;
static telecommand_adcs_t 				adcs_command;
static telecommand_scv_config_t			scv_conf_command;
static telecommand_uart_flush_t			uart_flush_command;
static telecommand_configure_eps_t		eps_command;
static telecommand_debug_set_t			debug_command;
static telecommand_rtos_status_t		rtos_status_command;

static script_daemon_t script_daemon;

/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static void register_telecommands( struct driver_toolkit_t* kit )
{
	telecommand_prototype_manager_t*	prototypes;

	// Get the prototype manager.
	prototypes = get_telecommand_prototype_manager( );
	if( prototypes == NULL )
	{
		csp_printf_crit( "Failed to initialize telecommanding backend" );
		return;
	}

	initialize_telecommand_mnlp_off( &mnlp_off, kit );
	initialize_telecommand_mnlp_reinit( &mnlp_reinit, kit );
	initialize_telecommand_dfgm_power(&dfgm_power_command, kit);
	initialize_telecommand_dfgm_stream(&dfgm_stream_command, kit);
	initialize_telecommand_dfgm_diag(&dfgm_diag_command, kit);
	initialize_telecommand_dosimeter(&dosimeter_command, kit);
	initialize_telecommand_athena(&athena_command, kit);
	initialize_telecommand_locale_logger(&locale_logger_command, kit);
	initialize_telecommand_telemetry_cleanup(&tcleanup_command, kit);
	initialize_telecommand_downlink(&downlink_command, kit);
	initialize_telecommand_adcs(&adcs_command, kit);
	initialize_telecommand_scv_config(&scv_conf_command, kit);
	initialize_telecommand_uart_flush(&uart_flush_command, kit);
	initialize_telecommand_configure_eps(&eps_command, kit);
	initialize_telecommand_debug_set(&debug_command, kit);
	initialize_telecommand_rtos_status(&rtos_status_command, kit);

	prototypes->register_prototype( prototypes, "mnlp stop", (telecommand_t*) &mnlp_off );
	prototypes->register_prototype( prototypes, "mnlp start", (telecommand_t*) &mnlp_reinit );
	prototypes->register_prototype(prototypes, "dfgm power", (telecommand_t*) &dfgm_power_command);
	prototypes->register_prototype(prototypes, "dfgm stream", (telecommand_t*) &dfgm_stream_command);
	prototypes->register_prototype(prototypes, "dfgm diag", (telecommand_t*) &dfgm_diag_command);
	prototypes->register_prototype(prototypes, "udos", (telecommand_t*) &dosimeter_command);
	prototypes->register_prototype(prototypes, "athena", (telecommand_t*) &athena_command);
	prototypes->register_prototype(prototypes, "locale logger", (telecommand_t*) &locale_logger_command);
	prototypes->register_prototype(prototypes, "cleanup", (telecommand_t*) &tcleanup_command);
	prototypes->register_prototype(prototypes, "downlink", (telecommand_t*) &downlink_command);
	prototypes->register_prototype(prototypes, "adcs", (telecommand_t*) &adcs_command);
	prototypes->register_prototype(prototypes, "RB", (telecommand_t*) &scv_conf_command);
	prototypes->register_prototype(prototypes, "flush", (telecommand_t*) &uart_flush_command);
	prototypes->register_prototype(prototypes, "eps", (telecommand_t*) &eps_command);
	prototypes->register_prototype(prototypes, "debug", (telecommand_t*) &debug_command);
	prototypes->register_prototype(prototypes, "rtos", (telecommand_t*) &rtos_status_command);
}

/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/

/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void start_telecommand_server( struct driver_toolkit_t* kit )
{
	bool_t err;
	static bool_t is_init = false;

	if( is_init ) {
		return;
	}

	/* Register all possible telecommands and start handler. */
	register_telecommands(kit);
	/* Start the script daemon. */
	err = initialize_script_daemon( &script_daemon, kit->gs );
	if( !err )
		{csp_printf_crit( "Failed to initialzie telecommanding services" );}
	else
		{csp_printf( "Script Daemon up and running" );}
	is_init = true;

}
/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
