/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_beacon_wod.c
 * @author Brendan Bruner
 * @date Oct 22, 2015
 */
#include <telecommands/telemetry_alteration/telecommand_beacon_wod.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h> /* For WOD size. */

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	file_t*				packet;
	fs_error_t			file_err;
	logger_error_t		logger_err;
	uint8_t				wod[PACKET_RAW_WOD_SIZE];
	uint32_t			file_size;

	/* Get the packet to read most recent log from. */
	packet = logger_peek_head( self->_kit->wod_logger, &logger_err );
	if( logger_err != LOGGER_OK )
	{
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		packet->close( packet );
		return;
	}

	/* Get file size. */
	file_size = packet->size( packet, &file_err );
	if( file_err != FS_OK || file_size < PACKET_RAW_WOD_SIZE )
	{
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		packet->close( packet );
		return;
	}

	/* Seek to location where read will start. */
	file_err = packet->seek( packet, file_size - PACKET_RAW_WOD_SIZE );
	if( file_err != FS_OK )
	{
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		packet->close( packet );
		return;
	}

	/* Read out the data and recycle file_size variable. */
	file_err = packet->read( packet, wod, PACKET_RAW_WOD_SIZE, &file_size );
	packet->close( packet );
	if( file_err != FS_OK )
	{
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		return;
	}

	/* Beacon the data. */
	self->_kit->gs->write( self->_kit->gs, wod, PACKET_RAW_WOD_SIZE, BEACON_PORT, USE_POLLING );
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_beacon_wod( telecommand_beacon_wod_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_BEACON_WOD;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
