/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ptimer_controller.c
 * @author Brendan Bruner
 * @date Oct 1, 2015
 */

#include <ptimer/ptimer_controller.h>
#include <core_defines.h>
#include <printing.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static bool_t ptimer_controller_is_init = false;
static ptimer_controller_t ptimer_contoller_singleton;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static void _ptimer_controller_task_( void* unused_input )
{
	UNUSED( unused_input );
	ptimer_controller_t* self = &ptimer_contoller_singleton;

	uint32_t 	iter;
	ptimer_t* 	ptimer;
	uint32_t	lag_time;

	for( ;; )
	{
		/* Loop through each timer observing the controller. */
		lock_mutex( self->_.register_mutex, BLOCK_FOREVER );
		for( iter = 0; iter < self->_.stack_size; ++iter )
		{
			/* Invoke the timers update operation. */
			ptimer = self->_.observers_stack[iter].ptimer;
			lag_time = self->_.observers_stack[iter].time_elapsed;

			if( ptimer == NULL ){ continue; }
			ptimer_update( ptimer, task_time_elapsed( ) - lag_time );
			self->_.observers_stack[iter].time_elapsed = task_time_elapsed( );
		}
		unlock_mutex( self->_.register_mutex );

		task_delay( PTIMER_CONTROLLER_UPDATE_CADENCE_MS );
	}
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
bool_t ptimer_controller_register( ptimer_controller_t* self, ptimer_t* observer )
{
	DEV_ASSERT( self );
	DEV_ASSERT( observer );

	/* Do not let the timer observe the controller if the controller's stack is full. */
	if( self->_.stack_size >= PTIMER_CONTROLLER_MAX_OBSERVERS )
	{
		return false;
	}

	/* Controller's stack is not full, add the timer to the observer stack. */
	lock_mutex( self->_.register_mutex, BLOCK_FOREVER );
	self->_.observers_stack[self->_.stack_size].ptimer = observer;
	self->_.observers_stack[self->_.stack_size].time_elapsed = task_time_elapsed( );
	++self->_.stack_size;
	unlock_mutex( self->_.register_mutex );

	return true;
}

void ptimer_controller_deregister( ptimer_controller_t* self, ptimer_t* observer )
{
	DEV_ASSERT( self );
	DEV_ASSERT( observer );

	uint32_t iter;

	for( iter = 0; iter < PTIMER_CONTROLLER_MAX_OBSERVERS; ++iter )
	{
		if( self->_.observers_stack[iter].ptimer == observer )
		{
			self->_.observers_stack[iter].ptimer = NULL;
			break;
		}
	}
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
ptimer_controller_t* initialize_ptimer_controller( )
{
	ptimer_controller_t* self = &ptimer_contoller_singleton;

	if( ptimer_controller_is_init == false )
	{
		uint32_t iter;

		new_mutex( self->_.register_mutex );
		if( self->_.register_mutex == NULL )
		{
			/* Failed to create semaphore. */
			return NULL;
		}
		unlock_mutex( self->_.register_mutex );

		/* persistent timer controller hasn't been initialized. */
		/* create the controller task. */
		base_t task_created;
		task_created = create_task( _ptimer_controller_task_, PTIMER_MANAGER_NAME, PTIMER_MANAGER_STACK_DEPTH, NO_PARAMETERS, PTIMER_MANAGER_PRIORITY, &self->_.controller_task );
		if( task_created != TASK_CREATED )
		{
			/* Failed to create task. */
			delete_mutex( self->_.register_mutex );
			return NULL;
		}

		/* Initialize timer stack. */
		for( iter = 0; iter < PTIMER_CONTROLLER_MAX_OBSERVERS; ++iter )
		{
			self->_.observers_stack[iter].ptimer = NULL;
		}

		ptimer_controller_is_init = true;

		/* Initialize member data. */
		self->_.stack_size = 0;

	}
	return self;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


