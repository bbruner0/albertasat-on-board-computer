/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file file_variable.c
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */

#include <non_volatile/file_variable.h>
#include <string.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define FILE_VARIABLE_INIT_ARRAY_LENGTH 8
#define FILE_VARIABLE_FIRST_INIT_VALUE 0


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static mutex_t file_variable_mutex;
static bool_t file_variable_mutex_is_init = false;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static bool_t file_variable_first_initialize( file_variable_t* self, file_t* variable )
{
	DEV_ASSERT( self );

	fs_error_t	file_err;
	uint8_t		init_value = FILE_VARIABLE_FIRST_INIT_VALUE;
	uint32_t	bytes_used;
	size_t i;

	file_err = variable->seek( variable, BEGINNING_OF_FILE );
	if( file_err != FS_OK )
	{
		return false;
	}

	for( i = 0; i < self->_.size; ++i ) {
		file_err = variable->write( variable, &init_value, sizeof(uint8_t), &bytes_used );
		if( bytes_used != sizeof(uint8_t) || file_err != FS_OK ) {
			/* Out of memory or file IO failure.
			 */
			variable->close( variable );
			return false;
		}
	}

	variable->close( variable );
	return true;
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t nvv_set( non_volatile_variable_t* self_, void* value )
{
	file_variable_t* self = (file_variable_t*) self_;
	DEV_ASSERT( self );
	DEV_ASSERT( value );

	fs_error_t	file_err;
	file_t*		variable;
	uint32_t	bytes_used;

	lock_mutex( *self->_.mutex, BLOCK_FOREVER );

	variable = self->_.fs->open( self->_.fs, &file_err, self->_.variable_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	if( file_err != FS_OK )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}

	file_err = variable->write( variable, (uint8_t*) value, self->_.size, &bytes_used );
	variable->close( variable );
	if( file_err != FS_OK )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}
	if( bytes_used < self->_.size )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}
	unlock_mutex( *self->_.mutex );
	return true;
}

static bool_t nvv_get( non_volatile_variable_t* self_, void* value )
{
	file_variable_t* self = (file_variable_t*) self_;
	DEV_ASSERT( self );
	DEV_ASSERT( value );

	fs_error_t	file_err;
	file_t*		variable;
	uint32_t	bytes_used;

	lock_mutex( *self->_.mutex, BLOCK_FOREVER );

	variable = self->_.fs->open( self->_.fs, &file_err, self->_.variable_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	if( file_err != FS_OK )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}

	file_err = variable->read( variable, (uint8_t*) value, self->_.size, &bytes_used );
	variable->close( variable );
	if( file_err != FS_OK )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}
	if( bytes_used < self->_.size )
	{
		unlock_mutex( *self->_.mutex );
		return false;
	}
	unlock_mutex( *self->_.mutex );
	return true;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_file_variable
(
	file_variable_t* self,
	filesystem_t* fs,
	char const* name,
	uint32_t size
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );
	DEV_ASSERT( name );

	fs_error_t	file_err;
	file_t*		variable;

	/* Initialize super class. */
	extern void initialize_non_volatile_variable( non_volatile_variable_t* );
	initialize_non_volatile_variable( (non_volatile_variable_t*) self );

	/* Member functions. */
	((non_volatile_variable_t*) self)->set = nvv_set;
	((non_volatile_variable_t*) self)->get = nvv_get;

	/* Initialize singleton mutex if not yet initialized. */
	if( file_variable_mutex_is_init == false )
	{
		new_mutex( file_variable_mutex );
		if( file_variable_mutex == NULL )
		{
			/* initialization failure. */
			return false;
		}
		file_variable_mutex_is_init = true;
	}

	/* Member data. */
	self->_.fs = fs;
	self->_.size = size;
	strncpy( self->_.variable_name, name, FILE_VARIABLE_MAX_NAME_LENGTH );
	self->_.variable_name[FILE_VARIABLE_MAX_NAME_LENGTH] = '\0';
	self->_.mutex = &file_variable_mutex;

	/* Check if the variable already exists. If it doesn't, allocate space for it. */
	file_err = fs->file_exists( fs, self->_.variable_name );;
	if( file_err == FS_NO_FILE )
	{
		/* The variable doesn't exist, create it. */
		variable = fs->open( fs, &file_err, self->_.variable_name, FS_CREATE_ALWAYS, BLOCK_FOREVER );
		if( file_err != FS_OK )
		{
			return false;
		}

		/* Allocate space for the variable. */
		file_err = variable->seek( variable, size );
		(void) file_err;

		file_variable_first_initialize( self, variable );

		variable->close( variable );
	}
	else if( file_err != FS_OK )
	{
		/* Error checking the file. */
		return false;
	}

	/* else, file_err == FS_OK */
	/* The file exists, nothing to do. */
	return true;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


