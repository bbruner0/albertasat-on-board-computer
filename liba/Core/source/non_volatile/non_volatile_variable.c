/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file non_volatile_variable.c
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */

#include <non_volatile/non_volatile_variable.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
/**
 * @memberof non_volatile_variable_t
 * @brief
 * 		Set the value of the variable.
 * @details
 * 		Set the value of the variable. This erases the old value.
 * @param value[in]
 * 		A pointer to the value which will be set.
 * @returns
 * 		<b>true</b> on success. <b>false</b> on failure.
 */
static bool_t nvv_set( non_volatile_variable_t* self, void* value )
{
	DEV_ASSERT( self );
	DEV_ASSERT( value );
	return false;
}

/**
 * @memberof non_volatile_variable_t
 * @brief
 * 		Get the value of the variable.
 * @details
 * 		Get the value of the variable
 * @param value[out]
 * 		A pointer to the location where the variable's value will be written.
 * @returns
 * 		<b>true</b> on success. <b>false</b> on failure.
 */
static bool_t nvv_get( non_volatile_variable_t* self, void* value )
{
	DEV_ASSERT( self );
	DEV_ASSERT( value );
	return false;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( non_volatile_variable_t* self )
{
	DEV_ASSERT( self );
	return;
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof non_volatile_variable_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_non_volatile_variable( non_volatile_variable_t* self )
{
	DEV_ASSERT( self );

	self->destroy = destroy;
	self->set = nvv_set;
	self->get = nvv_get;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
/**
 * @memberof non_volatile_variable_t
 * @details
 * 		Interface for setting a variable in non volatile memory.
 * @param value
 * 		Pointer to location of data to write to non volatile memory.
 * @return
 * 		<b>true</b>: on successful set.
 * 		<b>false</b>: otherwise.
 */
bool_t non_volatile_variable_set( non_volatile_variable_t* self, void* value )
{
	DEV_ASSERT(self);
	return self->set(self, value);
}

/**
 * @memberof non_volatile_variable_t
 * @details
 * 		Interface for getting a variable in non volatile memory.
 * @param value
 * 		Pointer to location of where data from non volatile memoruy
 * 		will be written.
 * @return
 * 		<b>true</b>: on successful set.
 * 		<b>false</b>: otherwise.
 */
bool_t non_volatile_variable_get( non_volatile_variable_t* self, void* value )
{
	DEV_ASSERT(self);
	return self->get(self, value);
}

