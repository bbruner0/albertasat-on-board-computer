/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file allocator.c
 * @author Brendan Bruner
 * @date Jun 8, 2015
 */

#include <allocator/allocator.h>
#include <portable_types.h>


/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
void initialize_allocator(  allocator_t *alloc,
							void *memory,
							allocator_node_t *nodes,
							uint32_t size_of_element,
							uint32_t size_of_memory )
{
	DEV_ASSERT( alloc );
	DEV_ASSERT( memory );
	DEV_ASSERT( nodes );

	uint32_t iter;

	alloc->_tail = nodes;
	alloc->_head = nodes + size_of_memory - 1;
	alloc->_remaining = size_of_memory;

	for( iter = 0; iter < size_of_memory; ++iter )
	{
		nodes[iter]._memory = (void *) (((uint8_t *) memory) + (iter * size_of_element));
		if( iter == size_of_memory - 1 )	nodes[iter]._next = &nodes[0];
		else 								nodes[iter]._next = &nodes[iter+1];
	}
}

void destroy_allocator( allocator_t *alloc )
{
	DEV_ASSERT( alloc );
}


/********************************************************************************/
/* Method Defines																*/
/********************************************************************************/
void *alloc_element( allocator_t *alloc )
{
	DEV_ASSERT( alloc );

	void *allocated_element;

	if( alloc->_tail == NULL ) return NULL;

	allocated_element = alloc->_tail->_memory;
	--(alloc->_remaining);

	/* if tail = head then this is last element, set tail to null. */
	if( alloc->_tail == alloc->_head )	alloc->_tail = NULL;
	/* otherwise, set tail to next element in linked list. */
	else 								alloc->_tail = alloc->_tail->_next;

	return allocated_element;
}

void free_element( allocator_t *alloc, void *element )
{
	DEV_ASSERT( alloc );
	DEV_ASSERT( element );

	if( alloc->_tail == NULL )	alloc->_tail = alloc->_head;
	else						alloc->_head = alloc->_head->_next;

	alloc->_head->_memory = element;
	++(alloc->_remaining);
}

uint32_t remaining_elements( allocator_t *alloc )
{
	return alloc->_remaining;
}
