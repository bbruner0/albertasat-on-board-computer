/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mnlp.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <mnlp/mnlp.h>
#include <mnlp.h>
#include <eps/power_lines.h>
#include <core_defines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
/**
 * @memberof mnlp_t
 * @brief
 * 		Turn the mnlp's power line on/off
 * @details
 * 		Turn the mnlp's power line on/off. This will invoke a hard power down / power on.
 * @param eps[in]
 * 		The eps which controls the mnlp's power line.
 * @param state
 * 		<b>true</b> to power on, <b>false</b> to power off.
 * @returns
 * 		<b>true</b> if successful, <b>false</b> otherwise.
 */
static bool_t power( mnlp_t* self, hub_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps ); //change this to hub, not eps.

	bool_t status;

	if( state == true )
	{
		csp_printf("MNLP Power on...");
		status = eps->power_line( eps, MNLP_3V3_POWER_LINE, HUB_POWER_LINE_ACTIVE );
		status = eps->power_line( eps, MNLP_5V0_POWER_LINE, HUB_POWER_LINE_ACTIVE );
	}
	else
	{
		csp_printf("MNLP Power off...");
		status = eps->power_line( eps, MNLP_5V0_POWER_LINE, HUB_POWER_LINE_INACTIVE );
		status = eps->power_line( eps, MNLP_3V3_POWER_LINE, HUB_POWER_LINE_INACTIVE );
	}

	return status;
}
static bool_t start_stop( mnlp_t* self, bool_t state)
{
	bool_t result;
	if( state == true )
			{
		#ifdef ENABLE_MNLP
				// Turn on mnlp task.
				result =  mnlp_start();
		#endif
			}
			else
			{
		#ifdef ENABLE_MNLP
				// Turn off mnlp task.
				result = mnlp_stop();
		#endif
			}
		return result;
}

/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof mnlp_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_mnlp_( mnlp_t* self )
{
	DEV_ASSERT( self );

	self->power = power;
	self->start_stop = start_stop;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


