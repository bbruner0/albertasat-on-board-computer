/*
 * mnlp_common.c
 *
 *  Created on: 15-Jul-2015
 *      Author: Atri Bhattacharyya
 */

#include "mnlp.h"
#include <string.h>
#include "rtc.h"
#include "driver_toolkit/driver_toolkit_nanomind.h"
#include <portable_types.h>
#include <printing.h>

#ifdef LPC1769
#include "mnlp_lpc.h"
#else
#include "mnlp_nanomind.h"
#include "dev/usart.h"
#include "dev/arm/ds1302.h"
#include "dev/arm/at91sam7.h"
#endif

/****************************************************************/
/*  Private Function prototypes									*/
/****************************************************************/

void mnlp_task( void *pvParameters);
uint32_t mnlp_gettime();
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time);

/****************************************************************/
/*  mNLP globals												*/
/****************************************************************/

mnlp_script_t script_slot[7];				//Several slots for scripts - QB50 specs
filesystem_t *filesys;						//File system var.
file_t *data_file;						//File for output of mNLP data
task_t cmd_tx_task, pkt_rx_task;			//Task handles for mnlpTask and mnlp_rx_data tasks
volatile uint8_t mnlp_error_detected = 0;	//When mNLP responds with SU_ERR packet, this variable is set
volatile uint8_t mnlp_is_on = 0;			//mNLP status - knows when mNLP is on or off based on mNLP power commands sent
uint8_t run_script = 0;						//Identified currently running script
uint16_t run_tt = 0;						//Identified currently running times table entry
static uint16_t mnlp_timeout = 0;

uint16_t c0_int;							//Variable used to checksum script
uint16_t c1_int;							//Variable used to checksum script
uint16_t xsum_w;							//Variable used to checksum script



/****************************************************************/
/*  Local defines												*/
/****************************************************************/
//Units for angular values for ADCS and MNLP data
#define ADCS_ANGLE_UNIT		0.01	//degrees
#define MNLP_ANGLE_UNIT 	2		//degrees
//Units for position values for ADCS and MNLP data
#define ADCS_POSITION_UNIT	0.25	//kilometers
#define MNLP_POSITION_UNIT	5		//kilometers

#define SCRIPT_ENTRY_IS_VALID(x) 	((x.script_index == SCPT_SEQ_FIVE || x.script_index == SCPT_SEQ_FOUR || x.script_index == SCPT_SEQ_THREE || x.script_index == SCPT_SEQ_TWO || x.script_index == SCPT_SEQ_ONE || x.script_index == END_OF_TABLE) && x.time_seconds < 60 && x.time_minutes < 60 && x.time_hours < 60)
#define CMD_HEADER_IS_VALID(x)		(x.delSec < 60 && x.delMin < 60 && 						\
(	x.CMD_ID == OBC_SU_ON 	|| x.CMD_ID == OBC_SU_OFF 	|| x.CMD_ID == SU_RESET 	|| x.CMD_ID == SU_LDP || x.CMD_ID == SU_HC		\
|| 	x.CMD_ID == SU_CAL 		|| x.CMD_ID == SU_SCI		|| x.CMD_ID == SU_HK 		|| x.CMD_ID == SU_STM							\
|| 	x.CMD_ID == SU_DUMP 	|| x.CMD_ID == SU_BIAS_ON 	|| x.CMD_ID == SU_BIAS_OFF 	|| x.CMD_ID == SU_MTEE_ON						\
|| 	x.CMD_ID == SU_MTEE_OFF	|| x.CMD_ID == SU_ERR 		|| x.CMD_ID == OBC_SU_ERR 	|| x.CMD_ID == OBC_EOT))

/****************************************************************/
/*  External variables											*/
/****************************************************************/
extern driver_toolkit_nanomind_t drivers;
static driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;

xSemaphoreHandle mnlp_go;
xSemaphoreHandle mnlp_nogo;


/****************************************************************/
/*  mNLP Public functions										*/
/****************************************************************/
/* Used as software enable. */
bool_t mnlp_start(void)
{
	if( mnlp_go != NULL ) {
		give_semaphore(mnlp_go);
		return true;
	}
	return false;
}

/* Used as software disable. */
bool_t mnlp_stop(void)
{
	if( mnlp_go != NULL ) {
		take_semaphore(mnlp_go, BLOCK_FOREVER);
		return true;
	}
	return false;
}

/* This is initialization function for MnLP driver. */
void mnlp_preinit(void)
{
	vSemaphoreCreateBinary(mnlp_go);
	if( mnlp_go == NULL ) {
		csp_printf("Failed to init MnLP driver");
		for(;;);
	}
	take_semaphore(mnlp_go, USE_POLLING);
	xTaskCreate(mnlp_task,(const signed char *) "mnlp task", 1024, NULL, 1, NULL);
}

/**
 * Spoof periodic telemetry from MnLP.
 */
void mnlp_task( void* param )
{
	FILE* fid2;
	mnlp_science_hdr_t data_hdr;					//Data packet header - to be attached before all packets
	mnlp_response_pkt_t *response_pkt;				//Packet containing received data
	char data_file_name[13];						//File name for data write to file
	char sd_file_name[17];
	char uffs_file_name[19];
	mnlp_timeout = 0;
	response_pkt = (mnlp_response_pkt_t *)pvPortMalloc(sizeof(mnlp_response_pkt_t));
#define MNLP_TOTAL_PACKET_TYPES 8
	uint8_t packet_types[] = {SU_LDP_PKT, SU_HC_PKT, SU_CAL_PKT, SU_SCI_PKT, SU_STM_PKT, SU_DUMP_PKT, SU_ERR_PKT, OBC_SU_ERR};
	uint8_t packet_index = 0;


	if( response_pkt == NULL ) {
		csp_printf("MnLP alloc failed, line %d", __LINE__);
		for(;;);
	}

	while(1)
	{
		peek_semaphore(mnlp_go, BLOCK_FOREVER);

		//Write to date-stamped data file in SD card
		create_output_filename(data_file_name, packet_types[(packet_index++) % MNLP_TOTAL_PACKET_TYPES], mnlp_gettime());

		snprintf(sd_file_name, 13, "%s", data_file_name);
		fs_error_t ferr;
		file_t* file = kit->fs->open(kit->fs, &ferr, sd_file_name, FS_OPEN_ALWAYS, 0);
		if(ferr == FS_OK)
		{
			uint32_t written;
			file->seek(file, file->size(file, &ferr));
			file->write(file, (uint8_t*) &data_hdr, sizeof(mnlp_science_hdr_t), &written);
			file->write(file, (uint8_t*) response_pkt, DATA_PACKET_SIZE, &written);
			file->close(file);
		}

		// Save to UFFS as well
		sprintf(uffs_file_name, "/boot/%s", data_file_name);
		fid2 = fopen(uffs_file_name, "a");
		if(fid2 != NULL)
		{
			fwrite(&data_hdr, sizeof(mnlp_science_hdr_t), 1, fid2);
			fwrite(response_pkt, DATA_PACKET_SIZE, 1, fid2);
		}
		fclose(fid2);

		vTaskDelay(60*60000);
	}

}

/**
 * @brief
 * 	Get current time
 *
 * @return
 * 	Seconds since QB50 epoch
 */
uint32_t mnlp_gettime()
{
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	time_t seconds;
	rtc_t *rtc;
	rtc = kit->rtc;
	seconds = rtc->get_ds1302_time();

	return seconds - QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH; // Converts to QB50 epoch.
}

/**
 * @brief
 * 	Checks if a year is a leap year
 *
 * @param year
 * 	Year to be checked
 */
inline int isleap(int year)
{
	return ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0));
}

void mnlp_getdate( int* yearp, int* monthp, int* dayp, uint32_t time )
{
	int year, month, days;
	int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int days_in_month_leap[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	days = time / 86400;		//Remove seconds

	//Extract year, month, date from time
	year = 2000;
	while(( days -= (isleap(year)?366:365)) > 0)
		year++;
	days += (isleap(year))?366:365;
	month = 1;
	if(isleap(year))
	{
		while((days -= days_in_month_leap[month - 1]) > 0)
			month++;
		days += days_in_month_leap[month - 1];
	}
	else
	{
		while((days -= days_in_month[month - 1]) > 0)
			month++;
		days += days_in_month[month - 1];
	}

	*yearp = year;
	*monthp = month;
	*dayp = days;
}

/**
 * @brief
 * 	Create the filename string for output file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param rsp_id
 * 	Response ID of packet to be written
 *
 * @param time
 * 	Current QB50 epoch time
 */
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time)
{
	int year, month, days;
	mnlp_getdate(&year, &month, &days, time);

	//Create file name
	switch(rsp_id)
	{
		case SU_LDP_PKT	:
		case SU_HC_PKT	:
		case SU_CAL_PKT	:
		case SU_HK_PKT	:
		case SU_STM_PKT	:	snprintf(name, 13, "M%02d%02d%02dH.bin", year % 100, month, days);	break;
		case SU_SCI_PKT	:
		case SU_DUMP_PKT:	snprintf(name, 13, "M%02d%02d%02dS.bin", year % 100, month, days);	break;
		case SU_ERR_PKT	:
		case OBC_SU_ERR	:	snprintf(name, 13, "M%02d%02d%02dE.bin", year % 100, month, days);	break;
		default			:	snprintf(name, 13, "M%02d%02d%02dD.bin", year % 100, month, days);	PRINT_DEBUG_STATEMENT("Illegal response ID");
	}
}
/****************************************************************/
/*  End of mNLP driver											*/
/****************************************************************/

