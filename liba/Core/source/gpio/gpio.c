/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file gpio.c
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */

#include <gpio/gpio.h>
#include <portable_types.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void set_state( gpio_t* self, gpio_state_t state )
{
	DEV_ASSERT( self );
}

static void set_direction( gpio_t* self, gpio_direction_t direction )
{
	DEV_ASSERT( self );
}

static gpio_state_t get_state( gpio_t* self )
{
	DEV_ASSERT( self );
	return GPIO_LOW;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( gpio_t* self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof gpio_t
 * @protected
 * @brief
 * 		Constructor for abstract gpio_t class.
 * @details
 * 		Constructor for abstract gpio_t class.
 */
void initialize_gpio_( gpio_t* self )
{
	DEV_ASSERT( self );
	self->get_state = get_state;
	self->set_state = set_state;
	self->set_direction = set_direction;
	self->destroy = destroy;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


