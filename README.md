## Application code
---

This repo contains three libraries used by Ex-Alta 1's OBC firmware

**liba/Core**

Almost all the OBC application code is in here

**liba/Subsystem**

Remainder of the OBC application code in here. There is no reason for this seperate library, it should be merged into liba/Core.

**liba/Testing**

Unit and integration tests the application code.

## Hardware Drivers, Network Stack, and File System
---

This code is contained in a private repository. 